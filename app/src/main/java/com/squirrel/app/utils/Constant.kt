package com.squirrel.app.utils

import com.squirrel.app.R


const val SPLASH_TIME_OUT = 1200

/*
* Places api key:
* */
const val BASE_URL = "https://maps.googleapis.com/"
const val PLACES_API_KEY = "AIzaSyBGcPyVrnH2Se2RpOy-TZHI9ewkX5oKV0s"
const val GOOGLE_PLACES_SEARCH = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
const val GOOGLE_PLACES_LAT_LONG = "https://maps.googleapis.com/maps/api/place/details/json?place_id="

/*
* Fragment Tags
* */
const val PARENT_HOME_TAG = "parent_home_tag"
const val PARENT_CHAT_TAG = "parent_save_tag"
const val PARENT_PLAYDATE_CHAT_TAG = "parent_playedate_chat_tag"
const val PARENT_BABYSITTER_CHAT_TAG = "parent_babysitter_chat_tag"



/*
* Links Constants
* */
const val LINK_TYPE = "link_type"
const val LINK_ABOUT = "link_about"
const val LINK_PP = "https://github.com/"
const val LINK_TERMS = "https://www.google.com/"
const val LINK_SUPPORT = "https://www.yahoo.com/"



/*
* Shared Preferences Keys
* */
const val IS_LOGIN = "is_login"
const val USERID = "userID"
const val USER_ROLE = "user_role"
const val USER_DOB = "user_dob"
const val NAME = "name"
const val USER_GENDER = "user_gender"
const val USER_GENDER_SPECIFY = "user_gender_specify"
const val PROFILEPIC = "profileImage"
const val USER_IC_EPIC = "user_id_Image"
const val USER_LATITUDE = "user_latitude"
const val USER_LONGITUDE = "user_longitude"
const val PLAYDATE_REMOVE_PREF = "play_date_pref"
const val BABYSITTER_REMOVE_PREF = "babysitter_remove_pref"



/*
* Data Transfer
* */
const val PHONE_NO = "phone_number"
const val MODEL = "model"
const val POS = "pos"
const val TITLE = "title"

/*
* View Holder
* */
const val LEFT_VIEW_HOLDER = 0
const val RIGHT_VIEW_HOLDER = 1
const val CENTER_VIEW_HOLDER = 2
const val LAST_MSG = "last_msg"
const val GOOGLE_REQUEST = 9893


/**
 * Firebase URLS/COLLECTIONS
 */
const val USER_IMAGES = "user_images/"
const val USER_IMAGES_ID = "user_image_ids/"
const val PARENTS_COLLECTION = "Parents"
const val BABYSITTERS_COLLECTION = "Babysitters"
const val CONVERSATIONS_COLLECTION = "Conversations"
const val MESSAGES = "messages"
const val EVENTS_COLLECTION = "Events"
const val KIDS_COLLECTION = "Kids"
const val REQUESTS_COLLECTION = "Requests"


/**
 * Pass Data/objects/models
 *
 */
const val PARENT_OBJECT = "parent_object"
const val KIDS_OBJECT = "kid_object"
const val PREVIOUS_DATA = "previous_data"
const val OTHER_SPECIFY = "other_specify"
const val FULL_ADDRESS = "other_specify"
const val LATITUDE = "latitude"
const val LONGITUDE = "longitude"
const val LOCATION_ROLE = "location_role"
const val FROM_PARENT = "form_parent"
const val FROM_BABYSITTER = "from_babysitter"
const val FROM_PLAYDATES = "form_playdates"



/*
* Arrays
* */
val SPECIFY_GENDER = arrayOf("Agender","Bigender","Cisgender","Gender Expression","Gender Fluid","Genderqueer","Intersex","Gemder Varamt","MX.","Third Gender","Transgender","Two-Spirit","Ze/Hir")
val GENDER = arrayOf("Male","Female","Other")
val RELATIONSHIP = arrayOf("Mother","Father","Other")
val mkidCountArray = arrayOf("First kid", "Second kid", "Third Kid", "Forth Kid", "Fifth kid")

val AVTAR_ARRAY = arrayOf(
    R.drawable.ab1,
    R.drawable.ag1,
    R.drawable.ab2,
    R.drawable.ag2,
    R.drawable.ab3,
    R.drawable.ag3,
    R.drawable.ab4,
    R.drawable.ag4,
    R.drawable.ab5,
    R.drawable.ag5,
    R.drawable.ab6,
    R.drawable.ag6,
    R.drawable.ab7,
    R.drawable.ag7,
    R.drawable.ab8,
    R.drawable.ag8,
    R.drawable.ab9,
    R.drawable.ag9,
    R.drawable.ab10,
    R.drawable.ag10,
    R.drawable.ab11,
    R.drawable.ag11,
    R.drawable.ab12,
    R.drawable.ag12,
    R.drawable.ab13,
    R.drawable.ag13,
    R.drawable.ab14,
    R.drawable.ag14,
    R.drawable.ab15,
    R.drawable.ag15,
    R.drawable.ab16,
    R.drawable.ag16,
    R.drawable.ab17,
    R.drawable.ag17,
    R.drawable.ag18,
    R.drawable.ag19,
    R.drawable.ag20,
    R.drawable.ag21
)


val FILTER_START_TIME = arrayOf("All","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00","00:00")
val FILTER_END_TIME = arrayOf("All","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00","00:00")
val WEEK_DAYS = arrayOf("All", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")

