package com.squirrel.app.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.net.ConnectivityManager
import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.location.Location
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.aigestudio.wheelpicker.WheelPicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.ktx.Firebase
import com.squirrel.app.R
import com.squirrel.app.activities.SelectionActivity
import com.squirrel.app.models.FlagModel
import org.json.JSONArray
import org.json.JSONException
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern






/*
* Initialize Other Classes Objects...
* */
var progressDialog: Dialog? = null


/*
* Is User Login:
* */
fun Context.isLogin(mActivity : Activity): Boolean {
    return AppPrefrences().readBoolean(mActivity, IS_LOGIN, false)
}


/*
* Getting User Role
* */
fun Context.getUserRole(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, USER_ROLE, "")!!
}

/*
* Getting User ID
* */
fun Context.getUserID(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, USERID, "")!!
}


/*
* Getting User DateOfBirth
* */
fun Context.getDateOfBirth(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, USER_DOB, "")!!
}

/*
* Getting User Gender
* */
fun Context.getUserGender(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, USER_GENDER, "")!!
}

/*
* Getting User Gender Specify Text
* */
fun Context.getUserGenderSpecify(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, USER_GENDER_SPECIFY, "")!!
}

/*
* Getting User Name
* */
fun Context.getName(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, NAME, "")!!
}



/*
* Getting User Profile Pic
* */
fun Context.getProfilePic(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, PROFILEPIC, "")!!
}

/*
* Getting User Id Pic
* */
fun Context.getIdImage(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, USER_IC_EPIC, "")!!
}

/*
* Getting User Latitude
* */
fun Context.getUserLatitude(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, USER_LATITUDE, "")!!
}

/*
* Getting User Longitude
* */
fun Context.getUserLongitude(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, USER_LONGITUDE, "")!!
}

/*
* PLAYDATE_REMOVE_PREF
* */
fun Context.getPlayDateRemoveIds(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, PLAYDATE_REMOVE_PREF, "")!!
}

/*
* BABYSITTER_REMOVE_PREF
* */
fun Context.getBabySitterRemoveIds(mActivity : Activity): String {
    return AppPrefrences().readString(mActivity, BABYSITTER_REMOVE_PREF, "")!!
}



/*
* Top Status Bar color
* */
fun Context.statusBarColor(mActivity : Activity){
    val window: Window = mActivity.getWindow()
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    window.statusBarColor = mActivity.getResources().getColor(R.color.colorWhite)
}

/*
* Top Status Bar color
* */
fun Context.statusRequestColor(mActivity : Activity,s: String) {
    val window: Window = mActivity.getWindow()
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    if(s==mActivity.getString(R.string.parentTag))
    {
        window.statusBarColor = mActivity.getResources().getColor(R.color.colorOrange)

    }
    else{
        window.statusBarColor = mActivity.getResources().getColor(R.color.colorGreen)

    }
}


/*
 * Show Progress Dialog
 * */
@RequiresApi(api = Build.VERSION_CODES.KITKAT)
fun Context.showProgressDialog(mActivity: Activity?) {
    progressDialog = Dialog(mActivity!!)
    progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
    progressDialog!!.setContentView(R.layout.dialog_progress)
    Objects.requireNonNull(progressDialog!!.window)!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    progressDialog!!.setCanceledOnTouchOutside(false)
    progressDialog!!.setCancelable(false)
    if (progressDialog != null) progressDialog!!.show()
}


/*
 * Hide Progress Dialog
 * */
fun Context.dismissProgressDialog() {
    if (progressDialog != null && progressDialog!!.isShowing) {
        progressDialog!!.dismiss()
    }
}


/*
 * Validate Email Address
 * */
fun Context.isValidEmaillId(email: String?): Boolean {
    return Pattern.compile(
        "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
    ).matcher(email).matches()
}


/*
 * Check Internet Connections
 * */
fun Context.isNetworkAvailable(mContext: Context): Boolean {
    val connectivityManager =
        mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}


/*
 * Toast Message
 * */
fun Context.showToast(mActivity: Activity?, strMessage: String?) {
    Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
}


/*
 *
 * Error Alert Dialog
 * */
fun Context.showAlertDialog(mActivity: Activity?, strMessage: String?) {
    val alertDialog = Dialog(mActivity!!)
    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    alertDialog.setContentView(R.layout.dialog_alert)
    alertDialog.setCanceledOnTouchOutside(false)
    alertDialog.setCancelable(false)
    alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    // set the custom dialog components - text, image and button
    val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
    val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
    txtMessageTV.text = strMessage
    btnDismiss.setOnClickListener { alertDialog.dismiss() }
    alertDialog.show()
}
/*
 *
 * Finish Error Alert Dialog
 * */
fun Context.showFinishAlertDialog(mActivity: Activity?, strMessage: String?) {
    val alertDialog = Dialog(mActivity!!)
    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    alertDialog.setContentView(R.layout.dialog_alert)
    alertDialog.setCanceledOnTouchOutside(false)
    alertDialog.setCancelable(false)
    alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    // set the custom dialog components - text, image and button
    val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
    val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
    txtMessageTV.text = strMessage
    btnDismiss.setOnClickListener {
        alertDialog.dismiss()
        mActivity.finish()
    }
    alertDialog.show()
}



/*
*
* Logout Alert Dialog
* */
fun Context.showLogoutAlertDialog(mActivity: Activity?) {
    val alertDialog = Dialog(mActivity!!)
    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    alertDialog.setContentView(R.layout.dialog_logout)
    alertDialog.setCanceledOnTouchOutside(false)
    alertDialog.setCancelable(false)
    alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    // set the custom dialog components - text, image and button
    val txtCancelTV = alertDialog.findViewById<TextView>(R.id.txtCancelTV)
    val txtLogoutTV = alertDialog.findViewById<TextView>(R.id.txtLogoutTV)

    txtCancelTV.setOnClickListener { alertDialog.dismiss() }
    txtLogoutTV.setOnClickListener {
        alertDialog.dismiss()
        val preferences: SharedPreferences = mActivity?.let { AppPrefrences().getPreferences(it) }!!
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
        Firebase.auth.signOut()
        val mIntent = Intent(mActivity, SelectionActivity::class.java)
        TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent).startActivities()
        mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        mActivity?.startActivity(mIntent)
        mActivity?.finish()
    }

    alertDialog.show()
}


fun Context.convertBitmapToByteArrayUncompressed(bitmap: Bitmap): ByteArray? {
    val stream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 40, stream)
    return stream.toByteArray()
}

fun Context.getAlphaNumericString(): String? {
    val n = 20

    // chose a Character random from this String
    val AlphaNumericString = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" /*+ "0123456789"*/
            + "abcdefghijklmnopqrstuvxyz")

    // create StringBuffer size of AlphaNumericString
    val sb = StringBuilder(n)
    for (i in 0 until n) {

        // generate a random number between
        // 0 to AlphaNumericString variable length
        val index = (AlphaNumericString.length
                * Math.random()).toInt()

        // add Character one by one in end of sb
        sb.append(
            AlphaNumericString[index]
        )
    }
    return sb.toString()
}


fun Context.convertTime(miliSec: Long): String {
    val simple: DateFormat = SimpleDateFormat("MMM dd at HH:mm")
    val result = Date(miliSec)
    System.out.println(simple.format(result))
    return simple.format(result)
}





/*
* Get Countries Details
* */
fun Context.getAllCountriesData(mActivity: Activity) : ArrayList<FlagModel> {
    var mCountriesArrayList: ArrayList<FlagModel> = ArrayList()
    try {
        val countryValue = readJSONFromAsset(mActivity)
        val jsonArray = JSONArray(countryValue)

        for (idx in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(idx)

            val firstValue = jsonObject.getString("nameImage").replace(" ", "_").toLowerCase()
            val secondValue = firstValue.replace("-", "_").toLowerCase()
            val resources: Resources = resources
            val resourceId: Int = resources.getIdentifier(secondValue, "drawable", packageName)

            val mModel : FlagModel = FlagModel(jsonObject.getString("name"), jsonObject.getString("code"),jsonObject.getString("dial_code"),resourceId,jsonObject.getString("nameImage"))
            mCountriesArrayList!!.add(mModel)
        }

    } catch (e: JSONException) {
        e.printStackTrace()
    }

    return mCountriesArrayList;
}


fun Context.hideKeyboard(activity: Activity) {
    val view =
        activity.findViewById<View>(android.R.id.content)
    if (view != null) {
        val imm =
            activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun Context.getCountryDialCode(mActivity: Activity,mCountryName : String) :  String{
    var mCountryCode : String = ""
    var mCountriesArrayList: ArrayList<FlagModel> = ArrayList()
    try {
        val countryValue = readJSONFromAsset(mActivity)
        val jsonArray = JSONArray(countryValue)

        for (idx in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(idx)

            val firstValue = jsonObject.getString("nameImage").replace(" ", "_").toLowerCase()
            val secondValue = firstValue.replace("-", "_").toLowerCase()
            val resources: Resources = resources
            val resourceId: Int = resources.getIdentifier(secondValue, "drawable", packageName)

            val mModel : FlagModel = FlagModel(jsonObject.getString("name"), jsonObject.getString("code"),jsonObject.getString("dial_code"),resourceId,jsonObject.getString("nameImage"))
            mCountriesArrayList!!.add(mModel)

            if (mCountryName.equals(jsonObject.getString("nameImage"))){
                mCountryCode = jsonObject.getString("dial_code")
                break
            }

        }

    } catch (e: JSONException) {
        e.printStackTrace()
    }

    return mCountryCode
}



/*
* Read Json from assests folder
* */
fun Context.readJSONFromAsset(mActivity: Activity): String? {
    val jsonString: String
    try {
        jsonString =
            mActivity.assets.open("countriesList.json").bufferedReader().use { it.readText() }
    } catch (ioException: IOException) {
        ioException.printStackTrace()
        return null
    }
    return jsonString
}




// - - DatePicker
fun Context.showDatePicker(mTextView: TextView) {
    // - - Initialize calender object
    val c = Calendar.getInstance()
    val mYear = c[Calendar.YEAR]
    val mMonth = c[Calendar.MONTH]
    val mDay = c[Calendar.DAY_OF_MONTH]
    val datePickerDialog = DatePickerDialog(
        this,
        { view, year, monthOfYear, dayOfMonth ->
            var monthValue = ""
            monthValue = if (monthOfYear + 1 >= 10) {
                (monthOfYear + 1).toString()
            } else {
                "0" + (monthOfYear + 1).toString()
            }
            var dateValue = ""
            dateValue = if (dayOfMonth >= 10) {
                dayOfMonth.toString()
            } else {
                "0$dayOfMonth"
            }
            //dd-mm-yyyy
            val mActualDate: String = "$dateValue-$monthValue-$year"
            mTextView.text = mActualDate
        }, mYear, mMonth, mDay
    )
    val currentDate = System.currentTimeMillis()
    datePickerDialog.datePicker.maxDate = currentDate
    datePickerDialog.show()
}



// - - DatePicker
fun Context.showFutureDatePicker(mTextView: TextView) {
    // - - Initialize calender object
    val c = Calendar.getInstance()
    val mYear = c[Calendar.YEAR]
    val mMonth = c[Calendar.MONTH]
    val mDay = c[Calendar.DAY_OF_MONTH]
    val datePickerDialog = DatePickerDialog(
        this,
        { view, year, monthOfYear, dayOfMonth ->
            var monthValue = ""
            monthValue = if (monthOfYear + 1 >= 10) {
                (monthOfYear + 1).toString()
            } else {
                "0" + (monthOfYear + 1).toString()
            }
            var dateValue = ""
            dateValue = if (dayOfMonth >= 10) {
                dayOfMonth.toString()
            } else {
                "0$dayOfMonth"
            }
            //dd-mm-yyyy
            val mActualDate: String = "$dateValue-$monthValue-$year"
            mTextView.text = mActualDate
        }, mYear, mMonth, mDay
    )
    val currentDate = System.currentTimeMillis()
    datePickerDialog.datePicker.minDate = currentDate
    datePickerDialog.show()
}


val HOURS = arrayOf("01","02","03","04","05","06","07","08","09","10","11","12")
val MINUTES = arrayOf("00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","19","20",
    "21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41",
    "42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59")
val AMPM = arrayOf("AM","PM")


fun Context.createFromToTimeDialog(mActivity: Activity, mTextView: TextView) {
    var mDataHoursList : ArrayList<String> = ArrayList<String>()
    var mDataMinutesList : ArrayList<String> = ArrayList<String>()
    var mDataAmPmList : ArrayList<String> = ArrayList<String>()
    for (i in 0..HOURS?.size!! - 1){
        mDataHoursList.add(HOURS?.get(i)!!)
    }
    for (i in 0..MINUTES?.size!! - 1){
        mDataMinutesList.add(MINUTES?.get(i)!!)
    }
    for (i in 0..AMPM?.size!! - 1){
        mDataAmPmList.add(AMPM?.get(i)!!)
    }

    val mBottomSheetDialog = BottomSheetDialog(this, R.style.AppBottomSheetDialogTheme)
    @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_from_to_time, null)
    mBottomSheetDialog.setContentView(sheetView)
    mBottomSheetDialog.show()
    val mTypeface = Typeface.createFromAsset(assets, "poppins_medium.ttf")
    val hoursWP: WheelPicker = sheetView.findViewById(R.id.hoursWP)
    val minutesWP: WheelPicker = sheetView.findViewById(R.id.minutesWP)
    val ampmWP: WheelPicker = sheetView.findViewById(R.id.ampmWP)
    hoursWP.typeface = mTypeface
    minutesWP.typeface = mTypeface
    ampmWP.typeface = mTypeface

    val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
    val btnConfirmB = sheetView.findViewById<androidx.appcompat.widget.AppCompatButton>(R.id.btnConfirmB)

    hoursWP.setAtmospheric(true)
    hoursWP.isCyclic = true
    hoursWP.isCurved = true
    //Set Data
    hoursWP.data = mDataHoursList

    minutesWP.setAtmospheric(true)
    minutesWP.isCyclic = true
    minutesWP.isCurved = true
    //Set Data
    minutesWP.data = mDataMinutesList

    ampmWP.setAtmospheric(true)
    ampmWP.isCyclic = true
    ampmWP.isCurved = true
    //Set Data
    ampmWP.data = mDataAmPmList

    txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }

    btnConfirmB.setOnClickListener {
        var mHour  = mDataHoursList.get(hoursWP.currentItemPosition)
        var mMinute  = mDataMinutesList.get(minutesWP.currentItemPosition)
        var mAmPm  = mDataAmPmList.get(ampmWP.currentItemPosition)
        mTextView.setText(mHour + ":" +mMinute + " " + mAmPm)
        Log.e("","")
        mBottomSheetDialog.dismiss()
    }
}

//Convert PlayDate-Format-To-DB-Format:
fun Context.convert12To24HoursFormat(mDob : String) : String{
    val originalFormat: DateFormat = SimpleDateFormat("hh:mm a")
    val targetFormat: DateFormat = SimpleDateFormat("HH:mm:ss")
    val date = originalFormat.parse(mDob)
    val formattedDate = targetFormat.format(date)
    return formattedDate
}



//January 11, 2022 at 4:04:30 PM UTC+5:30
fun Context.getCurrentTime() : String{
    val sdf = SimpleDateFormat("MMMM dd, yyyy 'at' HH:mm:ss a z")
    val currentDateandTime = sdf.format(Date())
    return currentDateandTime
}

//4:04:30 PM  From Full Date
fun Context.getOnlyTimeFromFullDate(mString: String) : String{
    val mArray = mString.split("T")
    return mArray[1]
}

//January 11, 2022 at 4:04:30 PM UTC+5:30
//Getting Firebase Server Time:
fun Context.updatedAt() : FieldValue {
    val mTimeStamp = FieldValue.serverTimestamp()
    return mTimeStamp
}


//YYYY-MM-DD
fun getCurrentDateInFormat() : String{
    val sdf = SimpleDateFormat("yyyy-MM-dd")
    val currentDateandTime = sdf.format(Date())
    return currentDateandTime
}


//YYYY-MM-DDTHH:mm:ss
fun Context.getCurrentDateInDobFormat() : String{
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val currentDateandTime = sdf.format(Date())
    return currentDateandTime
}

//Convert DateOfBirth-Format-To-DB-Format:
fun Context.convertDobToDbFormat(mDob : String) : String{
    val originalFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy")
    val targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val date = originalFormat.parse(mDob)
    val formattedDate = targetFormat.format(date)
    return formattedDate
}

//Convert PlayDate-Format-To-DB-Format:
fun Context.convertPlayDateDobToDbFormat(mDob : String) : String{
    val originalFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
    val targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val date = originalFormat.parse(mDob)
    val formattedDate = targetFormat.format(date)
    return formattedDate
}
//Convert PlayDate-Format-To-DB-Format:
fun Context.convertPlayDateDobToDbFormatEdit(mDob : String) : String{
    val originalFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm")
    val targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val date = originalFormat.parse(mDob)
    val formattedDate = targetFormat.format(date)
    return formattedDate
}

//Convert DB-Format-To-DateOfBirth-Format:
fun Context.convertDBToDobFormat(mDob : String) : String{
    val originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val targetFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy")
    val date = originalFormat.parse(mDob)
    val formattedDate = targetFormat.format(date)
    return formattedDate
}

//Calculate Date of Birth:
fun Context.calculateDateOfBirthWithCurrentTime(mDob : String) : String{
    var mDatePrevious : Date? = null
    var mDateCurrent : Date? = null
    val mDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    mDatePrevious = mDateFormat.parse(mDob)
    mDateCurrent = mDateFormat.parse(getCurrentDateInDobFormat())

    val difference: Long = Math.abs(mDatePrevious.getTime() - mDateCurrent.getTime())
    val differenceDates = difference / (24 * 60 * 60 * 1000 * 365)
    val yearDifference = java.lang.Long.toString(differenceDates)
    return yearDifference
}






/*
  * Getting Gender From Number
  * */
fun Context.getGenderString(mGender : String) : String{
    if (mGender == "0"){
        return getString(R.string.male)
    }else if (mGender == "1"){
        return getString(R.string.female)
    }else if (mGender == "2"){
        return getString(R.string.other)
    }

    return ""
}

/*
  * Getting Number From Gender
  * */
fun Context.getGenderNumber(mGender : String) : String {
    if (mGender == getString(R.string.male)) {
        return "0"
    } else if (mGender == getString(R.string.female)) {
        return "1"
    } else if (mGender == getString(R.string.other)) {
        return "2"
    }

    return ""
}


/*
 * Getting Relationship From Number
 * */
fun Context.getRelationShipString(mGender : String) : String{
    if (mGender == "0"){
        return getString(R.string.mother)
    }else if (mGender == "1"){
        return getString(R.string.father)
    }else if (mGender == "2"){
        return getString(R.string.other)
    }

    return ""
}

/*
 * Getting Number From Relationship
 * */
fun Context.getRelationShipNumber(mGender : String) : String{
    if (mGender == getString(R.string.mother)){
        return "0"
    }else if (mGender == getString(R.string.father)){
        return "1"
    }else if (mGender == getString(R.string.other)){
        return "2"
    }

    return ""
}

fun Context.getZipcodeFromLong(lat : Double, lng : Double) : String{
    val addresses: List<Address>
    val geocoder: Geocoder = Geocoder(this, Locale.getDefault())

    addresses = geocoder.getFromLocation(lat, lng, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5


    val address: String = addresses[0].getAddressLine(0)
    var citySelected = addresses[0].locality
    var stateSelectd = addresses[0].adminArea
    var postalCode1= addresses[0].postalCode

    return postalCode1
}

fun Context.getAddressFromLatLong(lat : Double, lng : Double) : String{
    val addresses: List<Address>
    val geocoder: Geocoder = Geocoder(this, Locale.getDefault())

    addresses = geocoder.getFromLocation(lat, lng, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5


    val address: String = addresses[0].getAddressLine(0)
    var citySelected = addresses[0].locality
    var stateSelectd = addresses[0].adminArea
    var postalCode1= addresses[0].postalCode

    return address
}



fun  Context.getDistanceFromLatLong(latU : Double, lngU : Double, latPd : Double, lngPd : Double) : Double{
    var mDistance : Float = 0f
    var mLocation1 = Location("LocationA")
    mLocation1.latitude = latU
    mLocation1.longitude = lngU

    var mLocation2 = Location("LocationA")
    mLocation2.latitude = latPd
    mLocation2.longitude = lngPd
    mDistance = mLocation1.distanceTo(mLocation2)

    var mDistanceInKM = mDistance.toDouble() / 1000

    var mFinalValue = DecimalFormat("##.###").format(mDistanceInKM)
    return mFinalValue.toDouble()
}


fun Context.getNumberFromDay(mDay : String) : String{
    if (mDay == "Monday"){
        return "1"
    }else if (mDay == "Tuesday"){
        return "2"
    }else if (mDay == "Wednesday"){
        return "3"
    }else if (mDay == "Thursday"){
        return "4"
    }else if (mDay == "Friday"){
        return "5"
    }else if (mDay == "Saturday"){
        return "6"
    }else if (mDay == "Sunday"){
        return "7"
    }else{
        return ""
    }
}


fun Context.getDataFromTimeStamp(timestamp: Long) : String{
    val c = Calendar.getInstance()
    c.timeInMillis = timestamp
    val d = c.time
    val sdf = SimpleDateFormat("hh:mm aa")
    return sdf.format(d)
}

fun Context.getDataFromTimeStampForDayMessage(timestamp: Long) : String{
    val c = Calendar.getInstance()
    c.timeInMillis = timestamp
    val d = c.time
    val sdf = SimpleDateFormat("MMM dd, yyyy hh:mm aa")
    return sdf.format(d)
}







