package com.squirrel.app.fragments.babysetter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.aigestudio.wheelpicker.WheelPicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import com.squirrel.app.activities.babysitter.SendBabySitterRequestActivity
import com.squirrel.app.adapters.babysitterAdapters.ExploreBabySittersAdapter
import com.squirrel.app.interfaces.BabySitterItemClickListner
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.models.RequestsModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.fragment_parent_baby_home.*

class BabySitterHomeFragment : Fragment() {
    var TAG = this@BabySitterHomeFragment.javaClass.simpleName
    lateinit var mUnbinder: Unbinder
    lateinit var mBaySittersRV:RecyclerView
    var mAdapter : ExploreBabySittersAdapter? = null
    var mBabySitterList: ArrayList<BabysitterModel> = ArrayList<BabysitterModel>()

    var mFilterAnyDayBabySitterList: ArrayList<BabysitterModel> = ArrayList()
    var mFilterAnyTimeBabySitterList: ArrayList<BabysitterModel> = ArrayList()
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mRequestsList : ArrayList<RequestsModel>  = ArrayList()
    var mArrayKidsList : ArrayList<String>  = ArrayList()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_parent_baby_home, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        setUpHeaderText(view)
        getMyBabySittersData()
        return view
    }

    private fun setUpHeaderText(mView : View) {
        var txtTitleTV : TextView = mView.findViewById(R.id.txtTitleTV)
        mBaySittersRV = mView.findViewById(R.id.mBaySittersRV)
        val mTitleText = "<font color=#0b0f35>Explore</font> <font color=#10B502>babysitters</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }

    @OnClick(
        R.id.dateLL,
        R.id.timeLL)

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.dateLL -> activity?.let { createAnyDayDialog(it,dateTv) }
            R.id.timeLL -> activity?.let { createAnytimeDialog(it,timeTv) }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        mUnbinder?.unbind()
    }



    private fun getMyBabySittersData() {
        if (activity?.isNetworkAvailable(requireActivity())!!) {
            executeGetBabySittersData()
        } else {
            activity?.showToast(activity, getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetBabySittersData() {
        var mBabySitterIDS = requireActivity().getBabySitterRemoveIds(requireActivity())
        var mBabySitterArray = mBabySitterIDS.split(",")

        activity?.showProgressDialog(requireActivity())
        mFireStoreDB!!.collection(BABYSITTERS_COLLECTION)
            .whereNotIn("userid", mBabySitterArray)
            .get().addOnSuccessListener { documentSnapshot ->
                activity?.dismissProgressDialog()
                mBabySitterList.clear()

                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mBabySitterList.add(document.toObject(BabysitterModel::class.java))
                    }
                    Log.e(TAG, "**BabySitter Size**${mBabySitterList.size}")
                    if (mBabySitterList.size > 0) {

                        for (i in 0 until mBabySitterList.size) {
                            var mModel = mBabySitterList.get(i)
                            if (activity?.getUserLatitude(requireActivity())!! != null && mModel.lattitude!! != null && mModel.lattitude!!.isNotEmpty()) {
                                mModel.distance = activity?.getDistanceFromLatLong(activity?.getUserLatitude(requireActivity())!!.toDouble(),activity?.getUserLongitude(requireActivity())!!.toDouble(), mModel.lattitude!!.toDouble(), mModel.longitude!!.toDouble())
                                mBabySitterList.set(i, mModel)
                            }else{
                                mModel.distance = 0.0
                                mBabySitterList.set(i, mModel)
                            }
                        }

                        mBabySitterList.sortWith(compareBy { it.distance })
                        setAdapter()

                        noDataFountLL.visibility = View.GONE
                    } else {
                        noDataFountLL.visibility = View.VISIBLE
                    }
                } else {
                    noDataFountLL.visibility = View.VISIBLE
                }
            }
            .addOnFailureListener { e ->
                activity?.dismissProgressDialog()
                noDataFountLL.visibility = View.VISIBLE
                activity?.showToast(activity, e.localizedMessage)
            }
    }



    private fun setAdapter() {
        mAdapter = ExploreBabySittersAdapter(requireActivity(),mBabySitterList,mBabySitterItemClickListner)
        val layoutManager : LinearLayoutManager
        layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        mBaySittersRV.layoutManager = layoutManager
        mBaySittersRV.adapter = mAdapter
    }



    fun createAnytimeDialog(mActivity: Activity, mTextView: TextView) {
        var mStartTimeList: java.util.ArrayList<String> = java.util.ArrayList<String>()
        var mEndTimeList: java.util.ArrayList<String> = java.util.ArrayList<String>()
        var mDataAmPmList: java.util.ArrayList<String> = java.util.ArrayList<String>()
        for (i in 0..FILTER_START_TIME.size - 1) {
            mStartTimeList.add(FILTER_START_TIME.get(i))
        }
        for (i in 0..FILTER_END_TIME.size - 1) {
            mEndTimeList.add(FILTER_END_TIME.get(i))
        }


        val mBottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.AppBottomSheetDialogTheme)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_any_time, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(requireActivity().assets, "poppins_medium.ttf")
        val startWP: WheelPicker = sheetView.findViewById(R.id.startWP)
        val endWP: WheelPicker = sheetView.findViewById(R.id.endWP)

        startWP.typeface = mTypeface
        endWP.typeface = mTypeface


        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val btnConfirmB = sheetView.findViewById<androidx.appcompat.widget.AppCompatButton>(R.id.btnConfirmB)

        startWP.setAtmospheric(true)
        startWP.isCyclic = false
        startWP.isCurved = true
        //Set Data
        startWP.data = mStartTimeList

        endWP.setAtmospheric(true)
        endWP.isCyclic = false
        endWP.isCurved = true
        //Set Data
        endWP.data = mEndTimeList



        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }

        btnConfirmB.setOnClickListener {
            var mStartTime = mStartTimeList.get(startWP.currentItemPosition)
            var mEndTime = mEndTimeList.get(endWP.currentItemPosition)

            mTextView.text = mStartTime /*+ "-" + mEndTime*/


            //SetUp Local Filter Here
            if (mTextView.text.toString().trim() == "All"){
                setAdapter()
            }else{
                mFilterAnyTimeBabySitterList.clear()
                mFilterAnyDayBabySitterList.clear()
                for (i in 0 until mBabySitterList.size){
                    var mModel = mBabySitterList.get(i)
                    for (j in 0 until mModel.days.size){
                        var mDayModel = mModel.days.get(j)
                        if (mDayModel.from?.contains(mTextView.text.toString().trim()) == true && mDayModel.isAvailable == "1"){
                            mFilterAnyTimeBabySitterList.add(mModel)
                        }
                    }
                }

                if (mFilterAnyTimeBabySitterList.size > 0) {
                    setAnyTimeFilterAdapter()
                    noDataFountLL.visibility = View.GONE
                } else {
                    mFilterAnyTimeBabySitterList.clear()
                    setAnyTimeFilterAdapter()
                    noDataFountLL.visibility = View.VISIBLE
                }
            }

            //Close Filter
            mBottomSheetDialog.dismiss()
        }
    }



    fun createAnyDayDialog(mActivity: Activity, mTextView: TextView) {
        var mWeekDaysList: java.util.ArrayList<String> = java.util.ArrayList<String>()
        for (i in 0..WEEK_DAYS.size - 1) {
            mWeekDaysList.add(WEEK_DAYS.get(i))
        }
        val mBottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.AppBottomSheetDialogTheme)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_any_day, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(requireActivity().assets, "poppins_medium.ttf")
        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val btnConfirmB = sheetView.findViewById<androidx.appcompat.widget.AppCompatButton>(R.id.btnConfirmB)
        val weekDaysWP = sheetView.findViewById<WheelPicker>(R.id.weekDaysWP)

        weekDaysWP.typeface = mTypeface

        weekDaysWP.setAtmospheric(true)
        weekDaysWP.isCyclic = false
        weekDaysWP.isCurved = true

        //Set Data
        weekDaysWP.data = mWeekDaysList

        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }

        btnConfirmB.setOnClickListener {
            mTextView.text =  mWeekDaysList.get(weekDaysWP.currentItemPosition)
            //SetUp Local Filter Here
            if (mTextView.text.toString().trim() == "All"){
                setAdapter()
            }else{
                mFilterAnyDayBabySitterList.clear()
                mFilterAnyTimeBabySitterList.clear()
                for (i in 0 until mBabySitterList.size){
                    var mModel = mBabySitterList.get(i)
                    for (j in 0 until mModel.days.size){
                        var mDayModel = mModel.days.get(j)
                        if (mDayModel.day?.contains(activity?.getNumberFromDay(mTextView.text.toString().trim())!!) == true && mDayModel.isAvailable == "1"){
                            mFilterAnyDayBabySitterList.add(mModel)
                        }
                    }
                }

                if (mFilterAnyDayBabySitterList.size > 0) {
                    setAnyDayFilterAdapter()
                    noDataFountLL.visibility = View.GONE
                } else {
                    mFilterAnyDayBabySitterList.clear()
                    setAnyDayFilterAdapter()
                    noDataFountLL.visibility = View.VISIBLE
                }
            }

            //Close Filter
            mBottomSheetDialog.dismiss()
        }
    }

    fun setAnyDayFilterAdapter(){
        mAdapter = ExploreBabySittersAdapter(requireActivity(),mFilterAnyDayBabySitterList,mBabySitterItemClickListner)
        val layoutManager : LinearLayoutManager
        layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        mBaySittersRV.layoutManager = layoutManager
        mBaySittersRV.adapter = mAdapter
    }

    fun setAnyTimeFilterAdapter(){
        mAdapter = ExploreBabySittersAdapter(requireActivity(),mFilterAnyTimeBabySitterList,mBabySitterItemClickListner)
        val layoutManager : LinearLayoutManager
        layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        mBaySittersRV.layoutManager = layoutManager
        mBaySittersRV.adapter = mAdapter
    }





    var mBabySitterItemClickListner : BabySitterItemClickListner = object :  BabySitterItemClickListner{
        override fun onItemClickListner(mModel: BabysitterModel,pos : Int) {
            super.onItemClickListner(mModel, pos)
        }

        override fun onCloseSwipeListner(mModel: BabysitterModel,pos : Int) {
            super.onCloseSwipeListner(mModel,pos)
            perforCloseClickListner(mModel,pos)
        }

        override fun onRightSwipeListner(mModel: BabysitterModel,pos : Int) {
            super.onRightSwipeListner(mModel,pos)
            performRightClickListner(mModel,pos)
        }
    }



    private fun perforCloseClickListner(mModel: BabysitterModel, pos : Int) {
       activity?.showProgressDialog(requireActivity())
        mFireStoreDB!!.collection(REQUESTS_COLLECTION)
            .whereEqualTo("playdate_parent_id",mModel.userid)
            .whereEqualTo("request_parent_id", requireActivity().getUserID(requireActivity())).limit(1)
            .get().addOnSuccessListener { documentSnapshot ->
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mRequestsList.add(document.toObject(RequestsModel::class.java))
                    }
                    var mRequestsModel : RequestsModel = mRequestsList[0]
                    if (mRequestsModel.status == "0" || mRequestsModel.status.isEmpty()){
                        activity?.dismissProgressDialog()
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_already_sent))
                    }else if (mRequestsModel.status == "1"){
                        activity?.dismissProgressDialog()
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_already_approved))
                    }else if (mRequestsModel.status == "2"){
                        activity?.dismissProgressDialog()
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_rejected))
                    }else if (mRequestsModel.status == "3"){
                        activity?.dismissProgressDialog()
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_own_cancel))
                    }else{
                        sendRequest(mModel)
                    }
                }else{
                    sendRequest(mModel)
                }
            }.addOnFailureListener { e ->
                activity?.dismissProgressDialog()
                Log.e(TAG,"**Error**${e.toString()}")
            }
    }




    var sendRequestLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == 444) {
            // There are no request codes
            val data: Intent? = result.data
            var mbbModel : BabysitterModel = data?.getParcelableExtra<BabysitterModel>(MODEL)!!
            var mPos : Int = data?.getIntExtra(POS,0)!!

            var mBabySitterRemoveIDs = requireActivity().getBabySitterRemoveIds(requireActivity())
            if (mBabySitterRemoveIDs.isNotEmpty()){
                mBabySitterRemoveIDs = mBabySitterRemoveIDs + "," + mbbModel.userid!!
            }else{
                mBabySitterRemoveIDs = mbbModel.userid!!
            }
            AppPrefrences().writeString(requireActivity(), BABYSITTER_REMOVE_PREF, mBabySitterRemoveIDs)!!

            if (mFilterAnyDayBabySitterList.size > 0){
                mFilterAnyDayBabySitterList.remove(mbbModel)
                mAdapter?.notifyDataSetChanged()
            }else if (mFilterAnyTimeBabySitterList.size > 0){
                mFilterAnyTimeBabySitterList.remove(mbbModel)
                mAdapter?.notifyDataSetChanged()
            }else{
                mBabySitterList.removeAt(mPos)
                mAdapter?.notifyDataSetChanged()
            }
        }
    }

    private fun performRightClickListner(mModel: BabysitterModel, pos : Int) {
        activity?.showProgressDialog(requireActivity())
        mFireStoreDB!!.collection(REQUESTS_COLLECTION)
            .whereEqualTo("playdate_parent_id",mModel.userid)
            .whereEqualTo("request_parent_id", requireActivity().getUserID(requireActivity())).limit(1)
            .get().addOnSuccessListener { documentSnapshot ->
                activity?.dismissProgressDialog()
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mRequestsList.add(document.toObject(RequestsModel::class.java))
                    }
                    var mRequestsModel : RequestsModel = mRequestsList[0]

                    if (mRequestsModel.status == "0" || mRequestsModel.status.isEmpty()){
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_already_sent))
                    }else if (mRequestsModel.status == "1"){
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_already_approved))
                    }else if (mRequestsModel.status == "2"){
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_rejected))
                    }else if (mRequestsModel.status == "3"){
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_own_cancel))
                    }else{
                        val mIntent = Intent(requireActivity(), SendBabySitterRequestActivity::class.java)
                        mIntent.putExtra(MODEL,mModel)
                        sendRequestLauncher.launch(mIntent)
                    }
                }else{
                    val mIntent = Intent(requireActivity(), SendBabySitterRequestActivity::class.java)
                    mIntent.putExtra(MODEL,mModel)
                    mIntent.putExtra(POS, pos)
                    sendRequestLauncher.launch(mIntent)
                }
            }.addOnFailureListener { e ->
                activity?.dismissProgressDialog()
                Log.e(TAG,"**Error**${e.toString()}")
            }
    }





    private fun sendRequest(mModel: BabysitterModel) {
        var mRequestID =  mFireStoreDB?.collection(REQUESTS_COLLECTION)?.document()!!.id
        Log.e(TAG,"**REQUEST_ID**$mRequestID")
        val mRequestMap = hashMapOf(
            "createdAt" to requireActivity().updatedAt(),
            "id" to mRequestID,
            "isForBabysitter" to "1",
            "kids" to mArrayKidsList,
            "playdate_id" to mRequestID,
            "playdate_name" to requireActivity().getName(requireActivity()),
            "playdate_parent_id" to mModel.userid,
            "request_parent_id" to requireActivity().getUserID(requireActivity()),
            "status" to "3")
        mFireStoreDB?.collection(REQUESTS_COLLECTION)?.document(mRequestID)
            ?.set(mRequestMap)
            ?.addOnSuccessListener {
                requireActivity().dismissProgressDialog()

                var mBabySitterRemoveIDs = requireActivity().getBabySitterRemoveIds(requireActivity())
                if (mBabySitterRemoveIDs.isNotEmpty()){
                    mBabySitterRemoveIDs = mBabySitterRemoveIDs + "," + mModel.userid!!
                }else{
                    mBabySitterRemoveIDs = mModel.userid!!
                }
                AppPrefrences().writeString(requireActivity(), BABYSITTER_REMOVE_PREF, mBabySitterRemoveIDs)!!


                if (mFilterAnyDayBabySitterList.size > 0){
                    mFilterAnyDayBabySitterList.remove(mModel)
                    mAdapter?.notifyDataSetChanged()
                }else if (mFilterAnyTimeBabySitterList.size > 0){
                    mFilterAnyTimeBabySitterList.remove(mModel)
                    mAdapter?.notifyDataSetChanged()
                }else{
                    mBabySitterList.remove(mModel)
                    mAdapter?.notifyDataSetChanged()
                }
            }?.addOnFailureListener { e ->
                requireActivity().dismissProgressDialog()
                requireActivity().showAlertDialog(requireActivity(),e.localizedMessage)
            }
    }









}