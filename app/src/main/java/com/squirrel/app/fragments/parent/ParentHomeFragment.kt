package com.squirrel.app.fragments.parent

import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import com.squirrel.app.activities.parent.SendRequestActivity
import com.squirrel.app.adapters.parentsAdapter.ExplorePlayDatesAdapter
import com.squirrel.app.interfaces.PlayDatetemClickListner
import com.squirrel.app.models.PlayDatesModel
import com.squirrel.app.models.RequestsModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_kid_parent_profile.*
import kotlinx.android.synthetic.main.fragment_parent_home.*


class ParentHomeFragment : Fragment() {
    var TAG = this@ParentHomeFragment.javaClass.simpleName
    var mUnbinder: Unbinder? = null
    lateinit var mRecyclerViewRV: RecyclerView
    var mAdapter: ExplorePlayDatesAdapter? = null
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mPlayDatesList: ArrayList<PlayDatesModel> = ArrayList()
    var mActualPlayDatesList: ArrayList<PlayDatesModel> = ArrayList()
    var mRequestsList : ArrayList<RequestsModel>  = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var mView: View = inflater.inflate(R.layout.fragment_parent_home, container, false)
        mUnbinder = ButterKnife.bind(this, mView)
        setUpHeaderText(mView)
        getMyPlayDatesData()
        return mView
    }


    private fun setUpHeaderText(mView: View) {
        var txtTitleTV: TextView = mView.findViewById(R.id.txtTitleTV)
        mRecyclerViewRV = mView.findViewById(R.id.mRecyclerViewRV)
        val mTitleText = "<font color=#0b0f35>Explore</font> <font color=#f16c21>playdates</font>"
        txtTitleTV.text = Html.fromHtml(mTitleText)
    }


    override fun onDestroy() {
        super.onDestroy()
        if (mUnbinder != null) {
            mUnbinder?.unbind()
        }
    }

    private fun getMyPlayDatesData() {
        if (requireActivity().isNetworkAvailable(requireActivity())) {
            executeGetPlayDatesData()
        } else {
            requireActivity().showToast(requireActivity(), getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetPlayDatesData() {
        var mPlayDateIDS = requireActivity().getPlayDateRemoveIds(requireActivity())
        var mPlayDateArray = mPlayDateIDS.split(",")
           activity?.showProgressDialog(requireActivity())
           mFireStoreDB!!.collection(EVENTS_COLLECTION)
               .whereNotIn("id", mPlayDateArray)
               .get().addOnSuccessListener { documentSnapshot ->
                activity?.dismissProgressDialog()
                   if (isAdded && activity != null) {
                       mPlayDatesList.clear()
                       mActualPlayDatesList.clear()
                       if (!documentSnapshot.isEmpty) {
                           for (document in documentSnapshot) {
                               mPlayDatesList.add(document.toObject(PlayDatesModel::class.java))
                           }
                           if (mPlayDatesList.size > 0) {
                               for (i in 0 until mPlayDatesList.size) {
                                   var mModel = mPlayDatesList.get(i)
                                   if (activity?.getUserLatitude(requireActivity())!! != null && mModel.lattitude!! != null && mModel.lattitude!!.isNotEmpty()) {
                                       if (activity?.getDistanceFromLatLong(activity?.getUserLatitude(requireActivity())!!.toDouble(), activity?.getUserLongitude(requireActivity())!!.toDouble(), mModel.lattitude!!.toDouble(), mModel.longitude!!.toDouble())!! <= 25.0) {
                                           mModel.distance = activity?.getDistanceFromLatLong(activity?.getUserLatitude(requireActivity())!!.toDouble(), activity?.getUserLongitude(requireActivity())!!.toDouble(), mModel.lattitude!!.toDouble(), mModel.longitude!!.toDouble())
                                           if (mModel.parentID != requireActivity().getUserID(requireActivity())) {
                                               mActualPlayDatesList.add(mModel)
                                           }
                                       }
                                   }
                               }
                               mActualPlayDatesList.sortWith(compareBy { it.date })
                               mActualPlayDatesList.sortWith(compareBy { it.distance })

                               setAdapter()
                               noDataFountLL.visibility = View.GONE
                           } else {
                               noDataFountLL.visibility = View.VISIBLE
                           }
                       } else {
                           noDataFountLL.visibility = View.VISIBLE
                       }
                   }
            }
            .addOnFailureListener { e ->
                activity?.dismissProgressDialog()
                noDataFountLL.visibility = View.VISIBLE
            }
    }


    private fun setAdapter() {
        mAdapter = ExplorePlayDatesAdapter(activity, mActualPlayDatesList, mPlayDatetemClickListner)
        val layoutManager: LinearLayoutManager
        layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        mRecyclerViewRV.layoutManager = layoutManager
        mRecyclerViewRV.adapter = mAdapter
    }

    var mPlayDatetemClickListner: PlayDatetemClickListner = object : PlayDatetemClickListner {
        override fun onItemClickListner(mModel: PlayDatesModel) {
            super.onItemClickListner(mModel)
        }

        override fun onCloseSwipeListner(mModel: PlayDatesModel) {
            super.onCloseSwipeListner(mModel)
            perforCloseClickListner(mModel)
        }

        override fun onRightSwipeListner(mModel: PlayDatesModel) {
            super.onRightSwipeListner(mModel)
            performRightClickListner(mModel)
        }
    }


    private fun perforCloseClickListner(mModel: PlayDatesModel) {
        activity?.showProgressDialog(requireActivity())
        mFireStoreDB!!.collection(REQUESTS_COLLECTION)
            .whereEqualTo("playdate_id",mModel.id)
            .whereEqualTo("playdate_parent_id",mModel.parentID)
            .whereEqualTo("request_parent_id", requireActivity().getUserID(requireActivity())).limit(1)
            .get().addOnSuccessListener { documentSnapshot ->
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mRequestsList.add(document.toObject(RequestsModel::class.java))
                    }
                    var mRequestsModel : RequestsModel  = mRequestsList[0]
                    if (mRequestsModel.status == "0" || mRequestsModel.status.isEmpty()){
                        activity?.dismissProgressDialog()
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_already_sent))
                    }else if (mRequestsModel.status == "1"){
                        activity?.dismissProgressDialog()
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_already_approved))
                    }else if (mRequestsModel.status == "2"){
                        activity?.dismissProgressDialog()
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_rejected))
                    }else if (mRequestsModel.status == "3"){
                        activity?.dismissProgressDialog()
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_own_cancel))
                    }else{
                       sendRequest(mModel)
                    }
                }else{
                    sendRequest(mModel)
                }
            }.addOnFailureListener { e ->
                activity?.dismissProgressDialog()
                Log.e(TAG,"**Error**${e.toString()}")
            }
    }

    private fun performRightClickListner(mModel: PlayDatesModel) {
        activity?.showProgressDialog(requireActivity())
        mFireStoreDB!!.collection(REQUESTS_COLLECTION)
            .whereEqualTo("playdate_id",mModel.id)
            .whereEqualTo("playdate_parent_id",mModel.parentID)
            .whereEqualTo("request_parent_id", requireActivity().getUserID(requireActivity())).limit(1)
            .get().addOnSuccessListener { documentSnapshot ->
                activity?.dismissProgressDialog()
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mRequestsList.add(document.toObject(RequestsModel::class.java))
                    }
                    var mRequestsModel : RequestsModel  = mRequestsList[0]

                    if (mRequestsModel.status == "0" || mRequestsModel.status.isEmpty()){
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_already_sent))
                    }else if (mRequestsModel.status == "1"){
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_already_approved))
                    }else if (mRequestsModel.status == "2"){
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_rejected))
                    }else if (mRequestsModel.status == "3"){
                        requireActivity().showAlertDialog(requireActivity(),getString(R.string.request_own_cancel))
                    }else{
                        var mIntent : Intent = Intent(requireActivity(), SendRequestActivity::class.java)
                        mIntent.putExtra(MODEL, mModel)
                        startActivityForResult(mIntent,258)
                    }
                }else{
                    var mIntent : Intent = Intent(requireActivity(), SendRequestActivity::class.java)
                    mIntent.putExtra(MODEL,mModel)
                    startActivityForResult(mIntent,258)
                }
            }.addOnFailureListener { e ->
                activity?.dismissProgressDialog()
                Log.e(TAG,"**Error**${e.toString()}")
            }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
            when (resultCode) {
                258 -> {
                    var mPlayDatesModel : PlayDatesModel = data?.getParcelableExtra<PlayDatesModel>(MODEL)!!

                    var mPlayDateRemoveIDs = requireActivity().getPlayDateRemoveIds(requireActivity())
                    if (mPlayDateRemoveIDs.isNotEmpty()){
                        mPlayDateRemoveIDs = mPlayDateRemoveIDs + "," + mPlayDatesModel.id
                    }else{
                        mPlayDateRemoveIDs = mPlayDatesModel.id!!
                    }
                    AppPrefrences().writeString(requireActivity(), PLAYDATE_REMOVE_PREF, mPlayDateRemoveIDs)!!

                    mActualPlayDatesList.remove(mPlayDatesModel)
                    mAdapter?.notifyDataSetChanged()
                }

        }
    }


    private fun sendRequest(mModel: PlayDatesModel) {
        var mRequestID =  mFireStoreDB?.collection(REQUESTS_COLLECTION)?.document()!!.id
        Log.e(TAG,"**REQUEST_ID**$mRequestID")
        val mRequestMap = hashMapOf(
            "createdAt" to requireActivity().updatedAt(),
            "id" to mRequestID,
            "isForBabysitter" to "0",
            "kids" to mModel.childList,
            "playdate_id" to mModel?.id,
            "playdate_name" to mModel?.title,
            "playdate_parent_id" to mModel?.parentID,
            "request_parent_id" to requireActivity().getUserID(requireActivity()),
            "status" to "3")
        mFireStoreDB?.collection(REQUESTS_COLLECTION)?.document(mRequestID)
            ?.set(mRequestMap)
            ?.addOnSuccessListener {
                requireActivity().dismissProgressDialog()

                var mPlayDateRemoveIDs = requireActivity().getPlayDateRemoveIds(requireActivity())
                if (mPlayDateRemoveIDs.isNotEmpty()){
                    mPlayDateRemoveIDs = mPlayDateRemoveIDs + "," + mModel.id
                }else{
                    mPlayDateRemoveIDs = mModel.id!!
                }
                AppPrefrences().writeString(requireActivity(), PLAYDATE_REMOVE_PREF, mPlayDateRemoveIDs)!!


                mActualPlayDatesList.remove(mModel)
                mAdapter?.notifyDataSetChanged()
            }?.addOnFailureListener { e ->
                requireActivity().dismissProgressDialog()
                requireActivity().showAlertDialog(requireActivity(),e.localizedMessage)
            }
    }





}