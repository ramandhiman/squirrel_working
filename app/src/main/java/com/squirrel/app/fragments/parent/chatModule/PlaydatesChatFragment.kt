package com.squirrel.app.fragments.parent.chatModule

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.Unbinder
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squirrel.app.R
import com.squirrel.app.adapters.PlayDatesChatAdapter
import com.squirrel.app.adapters.PlayDatesJoinRequestAdapter
import com.squirrel.app.models.ConversationalModel
import com.squirrel.app.models.RequestsModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.fragment_playdates_chat.*

class PlaydatesChatFragment : Fragment() {
    var TAG = this@PlaydatesChatFragment.javaClass.simpleName
    var mUnbinder: Unbinder? = null
    var mRequestsList: ArrayList<RequestsModel> = ArrayList()
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mChatUsersList: ArrayList<ConversationalModel> = ArrayList()


    // 0. Initiated, 1. Approved 2. Rejected 3. Own cancel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_playdates_chat, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        getWidgetsData()
        return view
    }


    private fun getWidgetsData() {
        if (activity?.isNetworkAvailable(requireActivity())!!){
            executeRequestsData()
        }else{
            activity?.showToast(requireActivity(),getString(R.string.internet_connection_error))
        }

        getConversationData()
    }

    private fun executeRequestsData() {
        activity?.showProgressDialog(activity)
        mFireStoreDB!!.collection(REQUESTS_COLLECTION)
            .whereEqualTo("playdate_parent_id",  requireActivity().getUserID(requireActivity()))
            .orderBy("createdAt", Query.Direction.DESCENDING)
            .addSnapshotListener { documentSnapshot, e ->
                activity?.dismissProgressDialog()
                mRequestsList.clear()
                if (!documentSnapshot!!.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        var mModel : RequestsModel = document.toObject(RequestsModel::class.java)
                        if (mModel.status == "0") {
                            mRequestsList.add(mModel)
                        }
                    }

                    if (mRequestsList.size > 0) {
                        setupJoinedRequestRV()
                        joinRequestTv.visibility = View.VISIBLE
                        joinRequestRL.visibility = View.VISIBLE
                    } else {
                        joinRequestTv.visibility = View.GONE
                        joinRequestRL.visibility = View.GONE
                    }
                } else {
                    joinRequestTv.visibility = View.GONE
                    joinRequestRL.visibility = View.GONE
                }
            }
    }


    private fun setupJoinedRequestRV() {
        var mAdapter: PlayDatesJoinRequestAdapter = PlayDatesJoinRequestAdapter(requireActivity(), mRequestsList)
        joinRequestRV?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        joinRequestRV?.setHasFixedSize(true)
        joinRequestRV?.adapter = mAdapter
    }




    private fun getConversationData() {
        if (activity?.isNetworkAvailable(requireActivity())!!){
            executeConversationData()
        }else{
            activity?.showToast(requireActivity(),getString(R.string.internet_connection_error))
        }
    }

    private fun executeConversationData() {
        mFireStoreDB!!.collection(CONVERSATIONS_COLLECTION).orderBy("updatedAt", Query.Direction.DESCENDING)
            .whereArrayContains("access", activity?.getUserID(requireActivity())!!)
            .whereEqualTo("isForBabysitter",  "0")
            .addSnapshotListener { documentSnapshot, e ->
                mChatUsersList.clear()
                if (!documentSnapshot!!.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        var mModel : ConversationalModel = document.toObject(ConversationalModel::class.java)
                        mChatUsersList.add(mModel)
                    }
                    if (mChatUsersList.size > 0) {
                        setupPlayDateChatRV()
                        noDataFountLL.visibility = View.GONE
                    } else {
                        noDataFountLL.visibility = View.VISIBLE
                    }
                } else {
                    noDataFountLL.visibility = View.VISIBLE
                }
            }
    }


    private fun setupPlayDateChatRV() {
        var mAdapter: PlayDatesChatAdapter = PlayDatesChatAdapter(requireActivity(), mChatUsersList)
        playdatesChatRV?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        playdatesChatRV?.setHasFixedSize(true)
        playdatesChatRV?.adapter = mAdapter
    }
}