package com.squirrel.app.fragments.parent.chatModule

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.Unbinder
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squirrel.app.R
import com.squirrel.app.adapters.parentsAdapter.ParentBabysitterChatUsersAdapter
import com.squirrel.app.models.ConversationalModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.fragment_baby_sitters_chat.*

class BabySittersChatFragment : Fragment() {
    var TAG = this@BabySittersChatFragment.javaClass.simpleName
    var mUnbinder: Unbinder? = null
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mChatUsersList: ArrayList<ConversationalModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_baby_sitters_chat, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        getConversationData()
        return view
    }

    private fun getConversationData() {
        if (activity?.isNetworkAvailable(requireActivity())!!){
            executeConversationData()
        }else{
            activity?.showToast(requireActivity(),getString(R.string.internet_connection_error))
        }
    }

    private fun executeConversationData() {
        activity?.showProgressDialog(activity)
        mFireStoreDB!!.collection(CONVERSATIONS_COLLECTION).orderBy("updatedAt", Query.Direction.DESCENDING)
            .whereArrayContains("access", activity?.getUserID(requireActivity())!!)
            .whereEqualTo("isForBabysitter",  "1")
            .addSnapshotListener{ documentSnapshot , e->
                activity?.dismissProgressDialog()
                if (isAdded && activity != null) {
                    mChatUsersList.clear()
                    if (!documentSnapshot!!.isEmpty) {
                        for (document in documentSnapshot) {
                            Log.e(TAG, "${document.id} => ${document.data}")
                            var mModel: ConversationalModel =
                                document.toObject(ConversationalModel::class.java)
                            mChatUsersList.add(mModel)
                        }
                        if (mChatUsersList.size > 0) {
                            setupBabySitterPlayChatRV()
                            noDataFountLL.visibility = View.GONE
                        } else {
                            noDataFountLL.visibility = View.VISIBLE
                        }
                    } else {
                        noDataFountLL.visibility = View.VISIBLE
                    }
                }
            }
    }




    private fun setupBabySitterPlayChatRV() {
        var mParentBabysitterChatUsersAdapter: ParentBabysitterChatUsersAdapter = ParentBabysitterChatUsersAdapter(requireActivity(),  mChatUsersList)
        babySitterChatRV.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        babySitterChatRV.setHasFixedSize(true)
        babySitterChatRV.adapter = mParentBabysitterChatUsersAdapter
    }
}