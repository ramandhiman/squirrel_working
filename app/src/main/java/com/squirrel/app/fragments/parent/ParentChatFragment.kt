package com.squirrel.app.fragments.parent

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.squirrel.app.R
import com.squirrel.app.fragments.parent.chatModule.BabySittersChatFragment
import com.squirrel.app.fragments.parent.chatModule.PlaydatesChatFragment
import com.squirrel.app.utils.PARENT_BABYSITTER_CHAT_TAG
import com.squirrel.app.utils.PARENT_PLAYDATE_CHAT_TAG
import kotlinx.android.synthetic.main.fragment_parent_chat.*

class ParentChatFragment : Fragment() {
    var TAG = this@ParentChatFragment.javaClass.simpleName
    var mUnbinder: Unbinder? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupPlaydateChat()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var mView: View = inflater.inflate(R.layout.fragment_parent_chat, container, false)
        mUnbinder = ButterKnife.bind(this, mView)

        return mView
    }

    @OnClick(
        R.id.playdatesLL,
        R.id.babysitterLL,
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.playdatesLL -> setupPlaydateChat()
            R.id.babysitterLL -> setupBabySitterChat()

        }
    }

    fun setupPlaydateChat() {
        viewOrange.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorOrange))
        viewGreen.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
        playeDateTv.typeface= ResourcesCompat.getFont(requireContext(), R.font.poppins_bold)
        babySitterTv.typeface= ResourcesCompat.getFont(requireContext(), R.font.poppins_medium)
        switchFragment(PlaydatesChatFragment(), PARENT_PLAYDATE_CHAT_TAG,false,null)
    }

    fun setupBabySitterChat() {
        viewGreen.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorGreen))
        viewOrange.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
        babySitterTv.typeface= ResourcesCompat.getFont(requireContext(), R.font.poppins_bold)
        playeDateTv.typeface= ResourcesCompat.getFont(requireContext(), R.font.poppins_medium)
        switchFragment(BabySittersChatFragment(), PARENT_BABYSITTER_CHAT_TAG,false,null)

    }

    override fun onDestroy() {
        super.onDestroy()
        mUnbinder?.unbind()
    }


    /*Switch between fragments*/
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = childFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.playDateChatContainer, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

}