package com.squirrel.app.viewholder

import android.app.Activity
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squirrel.app.R
import com.squirrel.app.models.MessageModel
import java.text.SimpleDateFormat
import java.util.*

class ItemRightViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var txtMessageTV = itemView.findViewById<TextView>(R.id.txtMessageTV) as TextView
    var txtTimeTV = itemView.findViewById<TextView>(R.id.txtTimeTV) as TextView

    fun bindData(mActivity: Activity?, mModel: MessageModel?) {
        if (mModel?.date != null){
            txtTimeTV.setText(mModel?.date?.toDate()?.time!!.toPrettyDate())
        }
        if (mModel!!.message != null){
            txtMessageTV.setText(mModel!!.message)
        }
    }


    fun Long.toPrettyDate(): String {
        val nowTime = Calendar.getInstance()
        val neededTime = Calendar.getInstance()
        neededTime.timeInMillis = this

        return if (neededTime[Calendar.YEAR] == nowTime[Calendar.YEAR]) {
            if (neededTime[Calendar.MONTH] == nowTime[Calendar.MONTH]) {
                when {
                    neededTime[Calendar.DATE] - nowTime[Calendar.DATE] == 1 -> {
                        //here return like "Tomorrow at 12:00"
                        "Tomorrow at " +  SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date(this))
                    }
                    nowTime[Calendar.DATE] == neededTime[Calendar.DATE] -> {
                        //here return like "Today at 12:00"
                        "Today at " +  SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date(this))
                    }
                    nowTime[Calendar.DATE] - neededTime[Calendar.DATE] == 1 -> {
                        //here return like "Yesterday at 12:00"
                        "Yesterday at " +  SimpleDateFormat("HH:mm", Locale.getDefault()).format(
                            Date(this)
                        )
                    }
                    else -> {
                        //here return like "May 31, 12:00"
                        SimpleDateFormat("MMMM d, HH:mm", Locale.getDefault()).format(Date(this))
                    }
                }
            } else {
                //here return like "May 31, 12:00"
                SimpleDateFormat("MMMM d, HH:mm", Locale.getDefault()).format(Date(this))
            }
        } else {
            //here return like "May 31 2022, 12:00" - it's a different year we need to show it
            SimpleDateFormat("MMMM dd yyyy, HH:mm", Locale.getDefault()).format(Date(this))
        }
    }

}