package com.comeonnow.patient.app.util

import com.squirrel.app.interfaces.PlacesLatLongModel
import com.squirrel.app.models.location.PlacesModel
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @GET
    fun searchPlacesRequest(
        @Url mUrl: String,
    ): Call<PlacesModel>?



    @GET
    fun searchPlacesLatLongRequest(
        @Url mUrl: String,
    ): Call<PlacesLatLongModel>?




}
