package com.squirrel.app.retrofit

import com.comeonnow.patient.app.util.ApiInterface
import com.squirrel.app.utils.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    val retrofitClient: Retrofit.Builder by lazy {
        //An OkHttp interceptor which logs request and response information
        val levelType: HttpLoggingInterceptor.Level = HttpLoggingInterceptor.Level.BODY
        val logging = HttpLoggingInterceptor()
        logging.setLevel(levelType)

        val okhttpClient = OkHttpClient.Builder()
        okhttpClient.addInterceptor(logging)

        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okhttpClient.build())
            .addConverterFactory(GsonConverterFactory.create())
    //Call the created instance of that class (done via create()) to your Retrofit instance with the method addConverterFactory()
    }

    val apiInterface: ApiInterface by lazy {
        retrofitClient
            .build()
            .create(ApiInterface::class.java)
    }
}






