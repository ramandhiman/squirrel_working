package com.squirrel.app.adapters.babysitterAdapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squirrel.app.R
import de.hdodenhof.circleimageview.CircleImageView


public class GroupProfileAdapter(var mActivity: Activity?, var mArrayList: Array<Int>) :
    RecyclerView.Adapter<GroupProfileAdapter.MyViewHolder>() {

    var lastCheckedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_kid_avtar, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.imgAvtarCIV.setImageResource(mArrayList[position])
        lastCheckedPosition = position
       // notifyDataSetChanged()
//        holder.itemView.setOnClickListener({
//            lastCheckedPosition = position
//            notifyDataSetChanged()
//        })
        if (lastCheckedPosition == position){
            holder.imgAvtarCIV.setBorderColor(mActivity?.resources?.getColor(R.color.green)!!)
            holder.imgTickIV.setVisibility(View.VISIBLE)
        }else{
            holder.imgAvtarCIV.setBorderColor(mActivity?.resources?.getColor(R.color.white)!!)
            holder.imgTickIV.setVisibility(View.GONE)
        }


    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    public class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imgAvtarCIV: CircleImageView
        var imgTickIV: ImageView
        init {
            imgAvtarCIV = itemView.findViewById(R.id.imgAvtarCIV)
            imgTickIV = itemView.findViewById(R.id.imgTickIV)
        }
    }


}
