package com.squirrel.app.adapters.parentsAdapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squirrel.app.R
import com.squirrel.app.interfaces.PlayDateKidsListnerInterface
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.utils.AVTAR_ARRAY
import de.hdodenhof.circleimageview.CircleImageView


class PlayDateKidsAvtarAdapter(var mActivity: Activity?,
                               var mArrayList : ArrayList<MyKidsModel>,
                               var mPlayDateKidsListnerInterface : PlayDateKidsListnerInterface
) : RecyclerView.Adapter<PlayDateKidsAvtarAdapter.MyViewHolder>() {

    var mSelectedArrayList : ArrayList<MyKidsModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_playdate_kid_avtar, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel = mArrayList[position]
        holder.imgAvtarCIV.setImageResource(AVTAR_ARRAY[Integer.parseInt(mModel.avatar)])

        if (mModel.isChecked == true){
            holder.imgAvtarCIV.setBorderColor(mActivity?.resources?.getColor(R.color.green)!!)
            holder.imgTickIV.setVisibility(View.VISIBLE)
        }else{
            holder.imgAvtarCIV.setBorderColor(mActivity?.resources?.getColor(R.color.white)!!)
            holder.imgTickIV.setVisibility(View.GONE)
        }

        holder.itemView.setOnClickListener{
            if (mModel.isChecked == false){
                mModel.isChecked = true
                mSelectedArrayList.add(mModel)
                notifyItemChanged(position)
            }else if (mModel.isChecked == true){
                mModel.isChecked = false
                mSelectedArrayList.remove(mModel)
                notifyItemChanged(position)
            }

            mPlayDateKidsListnerInterface.onSelectedKidListner(mSelectedArrayList)
        }

    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgAvtarCIV: CircleImageView
        var imgTickIV: ImageView
        init {
            imgAvtarCIV = itemView.findViewById(R.id.imgAvtarCIV)
            imgTickIV = itemView.findViewById(R.id.imgTickIV)
        }
    }

}


