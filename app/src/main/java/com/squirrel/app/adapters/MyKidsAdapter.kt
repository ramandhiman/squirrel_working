package com.squirrel.app.adapters

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squirrel.app.R
import com.squirrel.app.activities.parent.AddNewKidsActivity
import com.squirrel.app.activities.parent.EditDeleteKidActivity
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.utils.AVTAR_ARRAY
import com.squirrel.app.utils.KIDS_OBJECT
import de.hdodenhof.circleimageview.CircleImageView
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MyKidsAdapter(
    var mActivity: Activity?,
    var mArrayList : ArrayList<MyKidsModel>) : RecyclerView.Adapter<MyKidsAdapter.MyViewHolder>(),Serializable {

    var TAG = "MyKidsAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_my_kids_row, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel : MyKidsModel = mArrayList.get(position)
        var mAvtarPos = Integer.parseInt(mModel.avatar)
        Log.e(TAG,"**Avtar Position**$mAvtarPos")
        holder.myKidIV.setImageResource(AVTAR_ARRAY[mAvtarPos])


        holder.myKidNameTv.text=mModel.name + ", " + calculateDateOfBirthWithCurrentTime(mModel.dob!!) + "y"

        holder.itemView.setOnClickListener {
            val intent=Intent(mActivity, EditDeleteKidActivity::class.java)
            intent.putExtra(KIDS_OBJECT, mModel)
            mActivity!!.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var myKidIV: CircleImageView
        var myKidNameTv: TextView
        init {
            myKidIV = itemView.findViewById(R.id.myKidIV)
            myKidNameTv = itemView.findViewById(R.id.myKidNameTv)
        }
    }


    //Calculate Date of Birth:
    fun calculateDateOfBirthWithCurrentTime(mDob : String) : String{
        var mDatePrevious : Date? = null
        var mDateCurrent : Date? = null
        val mDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        mDatePrevious = mDateFormat.parse(mDob)
        mDateCurrent = mDateFormat.parse(getCurrentDateInDobFormat())

        val difference: Long = Math.abs(mDatePrevious.getTime() - mDateCurrent.getTime())
        val differenceDates = difference / (24 * 60 * 60 * 1000)
        val yearDifference = differenceDates / 365
        return ""+yearDifference
    }

    //YYYY-MM-DDTHH:mm:ss
    fun getCurrentDateInDobFormat() : String{
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val currentDateandTime = sdf.format(Date())
        return currentDateandTime
    }

}
