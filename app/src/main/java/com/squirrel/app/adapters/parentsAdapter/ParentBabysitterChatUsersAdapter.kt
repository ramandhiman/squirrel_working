package com.squirrel.app.adapters.parentsAdapter

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import com.squirrel.app.activities.parent.ParentBabySitterChatActivity
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.models.ConversationalModel
import com.squirrel.app.utils.*
import de.hdodenhof.circleimageview.CircleImageView


public class ParentBabysitterChatUsersAdapter(var mActivity: Activity?, var mArrayList: ArrayList<ConversationalModel>) : RecyclerView.Adapter<ParentBabysitterChatUsersAdapter.MyViewHolder>() {
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_parent_babysitter_chat_row, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel = mArrayList.get(position)

        holder.joinedMessageTv.setText(mModel.last_msg)
        holder.joinedTimeTv.setText(mActivity?.getDataFromTimeStamp(mModel.updatedAt?.toDate()?.time!!))
        holder.requestJoinCountLL.visibility = View.GONE
        /*
       * Open Chat with Other User
       * */
        holder.itemView.setOnClickListener{
            val intent=Intent(mActivity, ParentBabySitterChatActivity::class.java)
            intent.putExtra(MODEL,mModel)
            mActivity!!.startActivity(intent)
        }

           /*
           * Getting Other User Details:
           * */
        var mOtherUserId = ""
        for (i in 0 until mModel.access!!.size){
            if (mActivity?.getUserID(mActivity!!) != mModel.access!!.get(i)){
                mOtherUserId = mModel.access!!.get(i)
            }
        }
            Log.e("TAG","**ConversationalID**"+mOtherUserId)
            mFireStoreDB!!.collection(BABYSITTERS_COLLECTION)
                .whereEqualTo("userid", mOtherUserId).limit(1)
                .get().addOnSuccessListener { documentSnapshot ->
                    if (!documentSnapshot.isEmpty) {
                        var mBabySitterList: ArrayList<BabysitterModel> = ArrayList<BabysitterModel>()
                        for (document in documentSnapshot) {
                            Log.e("TAG", "${document.id} => ${document.data}")
                            mBabySitterList.add(document.toObject(BabysitterModel::class.java))
                        }
                        val mBabysitterModel: BabysitterModel = mBabySitterList[0]
                        Log.e("TAG","**BabySitterImageUrl**"+mBabysitterModel.imgsource)

                        Glide.with(mActivity!!).load(mBabysitterModel.imgsource)
                            .placeholder(R.drawable.ic_placeholder)
                            .error(R.drawable.ic_placeholder).into(holder.joinedequestIV)

                        if (mBabysitterModel.name!!.isNotEmpty()) {
                            holder.joinedRequestNameTv.setText(mBabysitterModel.name)
                        }
                    }
                }



    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    public class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var joinedequestIV: CircleImageView
        var joinedRequestNameTv: TextView
        var joinedMessageTv: TextView
        var joinedTimeTv: TextView
        var txtChildCountTV: TextView
        var chatPointerView: View
        var requestJoinCountLL: LinearLayout

        init {
            joinedequestIV = itemView.findViewById(R.id.joinedequestIV)
            joinedRequestNameTv = itemView.findViewById(R.id.joinedRequestNameTv)
            joinedMessageTv = itemView.findViewById(R.id.joinedMessageTv)
            chatPointerView = itemView.findViewById(R.id.chatPointerView)
            joinedTimeTv = itemView.findViewById(R.id.joinedTimeTv)
            txtChildCountTV = itemView.findViewById(R.id.txtChildCountTV)
            requestJoinCountLL = itemView.findViewById(R.id.requestJoinCountLL)
        }
    }


}
