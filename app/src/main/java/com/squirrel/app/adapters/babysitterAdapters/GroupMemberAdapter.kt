package com.squirrel.app.adapters

import android.app.Activity
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.squirrel.app.R
import com.squirrel.app.adapters.babysitterAdapters.GroupProfileAdapter
import com.squirrel.app.utils.AVTAR_ARRAY
import de.hdodenhof.circleimageview.CircleImageView

class GroupMemberAdapter(
    var mActivity: Activity?,
    var mArrayList: Array<String>,
) :
    RecyclerView.Adapter<GroupMemberAdapter.MyViewHolder>() {
    var dialog: BottomSheetDialog? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_group_member, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position == 0) {
            holder.viewChild2.visibility = View.GONE
            holder.viewChild1.visibility = View.VISIBLE
            holder.myKidNameTv1.text = mArrayList[0]

        } else if (position == 1) {
            holder.viewChild2.visibility = View.VISIBLE
            holder.viewChild1.visibility = View.GONE
           // holder.myKidNameTv.text = mArrayList[1]
           // holder.myKidNameTv.setTextColor(mActivity!!.resources.getColor(R.color.black))
        }

        holder.itemView.setOnClickListener {
            showGroupProfileSheet()
        }

    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var myKidIV: CircleImageView
        var myKidNameTv1: TextView
        var myKidNameTv: TextView
        var viewChild1: RelativeLayout
        var viewChild2: RelativeLayout

        init {
            myKidIV = itemView.findViewById(R.id.myKidIV)
            myKidNameTv1 = itemView.findViewById(R.id.myKidNameTv1)
            myKidNameTv = itemView.findViewById(R.id.myKidNameTv)
            viewChild1 = itemView.findViewById(R.id.viewChild1)
            viewChild2 = itemView.findViewById(R.id.viewChild2)
        }
    }


    private fun showGroupProfileSheet() {

        dialog = BottomSheetDialog(mActivity!!)
        val view: View = LayoutInflater.from(mActivity)
            .inflate(R.layout.layout_bottom_group_member_profile, null)
        dialog!!.setContentView(view)

        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)
        //recyclerview
        val avatarPickKidsRV: RecyclerView? =
            dialog!!.findViewById<RecyclerView>(R.id.avatarPickKidsRV)

        setupAvatarRV(avatarPickKidsRV)
        dialog!!.setCanceledOnTouchOutside(true)
        //disabling the drag down of sheet
        dialog!!.setCancelable(true)
        dialog!!.show()
    }


    fun setupAvatarRV(avatarPickKidsRV: RecyclerView?) {
        var mAdapter: GroupProfileAdapter = GroupProfileAdapter(mActivity, AVTAR_ARRAY)
        avatarPickKidsRV!!.layoutManager = LinearLayoutManager(
            mActivity,
            LinearLayoutManager.HORIZONTAL, false
        )
        avatarPickKidsRV.setHasFixedSize(true)
        avatarPickKidsRV.adapter = mAdapter
    }
}
