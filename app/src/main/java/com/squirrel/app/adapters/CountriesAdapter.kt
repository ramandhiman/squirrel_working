package com.squirrel.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.squirrel.app.R
import com.squirrel.app.interfaces.ItemClickListner
import com.squirrel.app.models.FlagModel
import kotlin.collections.ArrayList


class CountriesAdapter(var mActivity: Activity?, var mArrayList: ArrayList<FlagModel>, var mItemClickListner : ItemClickListner) :
    RecyclerView.Adapter<CountriesAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_countries, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel = mArrayList!!.get(position)

        holder.txtCountryNameTV.text = mModel!!.countryName
        holder.txtCountryCodeTV.text = mModel!!.countryCode + "(" + mModel!!.countryPhoneCode+")"


        mActivity?.let {
            Glide.with(it).load(mModel!!.countryImage)
                .placeholder(R.drawable.ic_flag_ph)
                .error(R.drawable.ic_flag_ph)
                .into(holder.imgCountryIV)
        }

        holder.itemView.setOnClickListener({
            mItemClickListner.onItemClickListner(mModel)
        })

    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imgCountryIV: ImageView
        var txtCountryNameTV: TextView
        var txtCountryCodeTV: TextView

        init {
            imgCountryIV = itemView.findViewById(R.id.imgCountryIV)
            txtCountryNameTV = itemView.findViewById(R.id.txtCountryNameTV)
            txtCountryCodeTV = itemView.findViewById(R.id.txtCountryCodeTV)


        }
    }

    //This method will filter the list
    //here we are passing the filtered data
    //and assigning it to the list with notifydatasetchanged method
    fun filterList(filterdList: ArrayList<FlagModel>) {
        this.mArrayList = filterdList
        notifyDataSetChanged()
    }

}
