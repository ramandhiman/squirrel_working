package com.squirrel.app.adapters.babysitterAdapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squirrel.app.R
import com.squirrel.app.models.MessageModel
import com.squirrel.app.utils.LEFT_VIEW_HOLDER
import com.squirrel.app.utils.RIGHT_VIEW_HOLDER
import kotlin.collections.ArrayList


class BabySitterChatListingAdapter(var mActivity: Activity?,var mArrayList : ArrayList<MessageModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == RIGHT_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_chat_right_row, parent, false)
            return ItemRightViewHolder(mView)
        } else if (viewType == LEFT_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_chat_left_row, parent, false)
            return ItemLeftViewHolder(mView)
        }
        return null!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == RIGHT_VIEW_HOLDER) {

        } else if (holder.itemViewType == LEFT_VIEW_HOLDER) {

        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }


    override fun getItemViewType(position: Int): Int {
        if (mArrayList!![position]!!.viewType == 1) return RIGHT_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 0) return LEFT_VIEW_HOLDER
        return -1
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtTitleTV: TextView
        var txtDescriptionTV: TextView
        var distanceTv: TextView
        var priceTv: TextView
        var ratingCountTv: TextView


        init {
            txtTitleTV = itemView.findViewById(R.id.txtTitleTV)
            txtDescriptionTV = itemView.findViewById(R.id.txtDescriptionTV)
            distanceTv = itemView.findViewById(R.id.distanceTv)
            priceTv = itemView.findViewById(R.id.priceTv)
            ratingCountTv = itemView.findViewById(R.id.ratingCountTv)

        }
    }

    class ItemLeftViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    class ItemRightViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}


