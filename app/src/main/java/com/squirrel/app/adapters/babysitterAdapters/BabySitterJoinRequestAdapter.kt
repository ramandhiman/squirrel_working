package com.squirrel.app.adapters.babysitterAdapters

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import com.squirrel.app.activities.babysitter.AdmitChild2Activity
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.models.RequestsModel
import com.squirrel.app.utils.AVTAR_ARRAY
import com.squirrel.app.utils.KIDS_COLLECTION
import com.squirrel.app.utils.MODEL
import de.hdodenhof.circleimageview.CircleImageView


public class BabySitterJoinRequestAdapter(var mActivity: Activity?, var mArrayList: ArrayList<RequestsModel>) :
    RecyclerView.Adapter<BabySitterJoinRequestAdapter.MyViewHolder>() {
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_baby_sitter_joined_row, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel = mArrayList.get(position)

        var mChildCount = mModel.kids!!.size - 1

        if (mChildCount >= 1){
            holder.txtChildCountTV.setText("+"+mChildCount)
            holder.requestJoinCountLL.visibility = View.VISIBLE
        }else{
            holder.requestJoinCountLL.visibility = View.GONE
        }


        //Getting  Details
        mFireStoreDB!!.collection(KIDS_COLLECTION).document(mModel!!.kids!![0])
            .get().addOnSuccessListener { documentSnapshot ->
                if (documentSnapshot.exists()) {
                    val mMyKidsModel: MyKidsModel =
                        documentSnapshot.toObject(MyKidsModel::class.java)!!
                    Log.e("TAG", "**Avatar**" + mMyKidsModel.avatar)
                    Glide.with(mActivity!!).load(AVTAR_ARRAY[Integer.parseInt(mMyKidsModel.avatar)])
                        .placeholder(R.drawable.ic_placeholder)
                        .error(R.drawable.ic_placeholder)
                        .into(holder.joinedequestIV)
                }
            }

        holder.itemView.setOnClickListener{
            var mIntent : Intent = Intent(mActivity, AdmitChild2Activity::class.java)
            mIntent.putExtra(MODEL, mModel)
            mActivity?.startActivity(mIntent)
        }
    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    public class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var joinedequestIV: CircleImageView
        var requestJoinCountLL: LinearLayout
        var txtChildCountTV: TextView

        init {
            joinedequestIV = itemView.findViewById(R.id.joinedequestIV)
            requestJoinCountLL = itemView.findViewById(R.id.requestJoinCountLL)
            txtChildCountTV = itemView.findViewById(R.id.txtChildCountTV)
        }
    }


}
