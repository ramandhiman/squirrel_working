package com.squirrel.app.adapters.babysitterAdapters

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squirrel.app.R
import com.squirrel.app.activities.babysitter.BabySitterChatActivity
import com.squirrel.app.activities.parent.ParentBabySitterChatActivity
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.models.ConversationalModel
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.models.ParentModel
import com.squirrel.app.utils.*
import de.hdodenhof.circleimageview.CircleImageView


public class BabySittersChatAdapter(var mActivity: Activity?, var mArrayList: ArrayList<ConversationalModel>) :
    RecyclerView.Adapter<BabySittersChatAdapter.MyViewHolder>() {
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_baby_sitter_chat_row, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel = mArrayList.get(position)

        holder.joinedMessageTv.setText(mModel.last_msg)
        holder.joinedTimeTv.setText(mActivity?.getDataFromTimeStamp(mModel.updatedAt?.toDate()?.time!!))

        var mChildCount = mModel.kidsList!!.size - 1
        if (mChildCount >= 1){
            holder.txtChildCountTV.setText("+"+mChildCount)
            holder.requestJoinCountLL.visibility = View.VISIBLE
        }else{
            holder.requestJoinCountLL.visibility = View.GONE
        }


        Glide.with(mActivity!!).load(mModel.avatar)
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder).into(holder.joinedequestIV)

        /*
       * Open Chat with Other User
       * */
        holder.itemView.setOnClickListener{
            val intent=Intent(mActivity, ParentBabySitterChatActivity::class.java)
            intent.putExtra(MODEL,mModel)
            mActivity!!.startActivity(intent)
        }


        //Getting  Details
        if (mModel.kidsList!!.isNotEmpty()) {
            mFireStoreDB!!.collection(KIDS_COLLECTION)
                .whereIn("firebaseID", mModel.kidsList!!)
                .get().addOnSuccessListener { documentSnapshot ->
                    if (!documentSnapshot.isEmpty) {
                        var mKidsList : ArrayList<MyKidsModel>  = ArrayList()
                            for (document in documentSnapshot) {
                                Log.e("TAG", "${document.id} => ${document.data}")
                                mKidsList.add(document.toObject(MyKidsModel::class.java))
                            }
                        holder.joinedequestIV.setImageResource(AVTAR_ARRAY[Integer.parseInt(mKidsList[0].avatar)])
                        var mAllChildsName = ""

                        for (i in 0 until mKidsList.size){
                            if (i == 0){
                                mAllChildsName = mKidsList[i].name!!
                            }else{
                                mAllChildsName = mAllChildsName + ", " + mKidsList[i].name!!
                            }
                        }
                        holder.joinedRequestNameTv.setText(mAllChildsName)
                    }else{
                        Log.e("TAG","**NoDataFound**")
                    }
                }.addOnFailureListener{ e ->
                    Log.e("TAG","**Error**"+e.message)
                }
        }

    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    public class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var joinedequestIV: CircleImageView
        var joinedRequestNameTv: TextView
        var joinedMessageTv: TextView
        var joinedTimeTv: TextView
        var txtChildCountTV: TextView
        var chatPointerView: View
        var requestJoinCountLL: LinearLayout
        init {
            joinedequestIV = itemView.findViewById(R.id.joinedequestIV)
            joinedRequestNameTv = itemView.findViewById(R.id.joinedRequestNameTv)
            joinedMessageTv = itemView.findViewById(R.id.joinedMessageTv)
            chatPointerView = itemView.findViewById(R.id.chatPointerView)
            joinedTimeTv = itemView.findViewById(R.id.joinedTimeTv)
            txtChildCountTV = itemView.findViewById(R.id.txtChildCountTV)
            requestJoinCountLL = itemView.findViewById(R.id.requestJoinCountLL)
        }
    }


}
