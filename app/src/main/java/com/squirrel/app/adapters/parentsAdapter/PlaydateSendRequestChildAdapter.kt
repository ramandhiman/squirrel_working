package com.squirrel.app.adapters.parentsAdapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squirrel.app.R
import com.squirrel.app.interfaces.AddKidAvtarClickListner
import com.squirrel.app.interfaces.PlayDateKidsListnerInterface
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.utils.AVTAR_ARRAY
import de.hdodenhof.circleimageview.CircleImageView


class PlaydateSendRequestChildAdapter(var mActivity: Activity?, var mArrayList : ArrayList<MyKidsModel>, var mPlayDateKidsListnerInterface : PlayDateKidsListnerInterface) : RecyclerView.Adapter<PlaydateSendRequestChildAdapter.MyViewHolder>() {
    var mSelectedArrayList : ArrayList<MyKidsModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_playdate_request_send_child, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel = mArrayList.get(position)
        holder.imgKidImageIV.setImageResource(AVTAR_ARRAY[Integer.parseInt(mModel.avatar)])


        if (mModel.isChecked == true){
            holder.imgSelectedIV.setVisibility(View.VISIBLE)
        }else{
            holder.imgSelectedIV.setVisibility(View.GONE)
        }


        holder.itemView.setOnClickListener{
            if (mModel.isChecked == false){
                mModel.isChecked = true
                mSelectedArrayList.add(mModel)
                notifyItemChanged(position)
                mPlayDateKidsListnerInterface.onSelectedKidListner(mSelectedArrayList)
            }else if (mModel.isChecked == true){
                mModel.isChecked = false
                mSelectedArrayList.remove(mModel)
                notifyItemChanged(position)
                mPlayDateKidsListnerInterface.onSelectedKidListner(mSelectedArrayList)
            }



        }


    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgKidImageIV: CircleImageView
        var imgSelectedIV: ImageView
        init {
            imgKidImageIV = itemView.findViewById(R.id.imgKidImageIV)
            imgSelectedIV = itemView.findViewById(R.id.imgSelectedIV)
        }
    }

}


