package com.squirrel.app.adapters

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import com.squirrel.app.activities.ParentPlayDateChatActivity
import com.squirrel.app.activities.babysitter.BabySitterChatActivity
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.models.ConversationalModel
import com.squirrel.app.utils.*
import de.hdodenhof.circleimageview.CircleImageView


class PlayDatesChatAdapter(
    var mActivity: Activity?,
    var mArrayList: ArrayList<ConversationalModel>
) :
    RecyclerView.Adapter<PlayDatesChatAdapter.MyViewHolder>() {

    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_play_date_chat_row, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel = mArrayList.get(position)
        holder.joinedMessageTv.text = mModel.last_msg
        holder.joinedTimeTv.setText(mActivity?.getDataFromTimeStamp(mModel.updatedAt?.toDate()?.time!!))
        holder.requestJoinCountLL.visibility = View.GONE

        var mChildCount = mModel.kidsList!!.size - 1
        if (mChildCount >= 1) {
            holder.txtChildCountTV.setText("+" + mChildCount)
            holder.requestJoinCountLL.visibility = View.VISIBLE
        } else {
            holder.requestJoinCountLL.visibility = View.GONE
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(mActivity, ParentPlayDateChatActivity::class.java)
            intent.putExtra(MODEL,mModel)
            mActivity!!.startActivity(intent)
        }


        /*
         * Getting Other User Details:
         * */
        var mOtherUserId = ""
        for (i in 0 until mModel.access!!.size){
            if (mActivity?.getUserID(mActivity!!) != mModel.access!!.get(i)){
                mOtherUserId = mModel.access!!.get(i)
            }
        }
        Log.e("TAG","**ConversationalID**"+mModel.conversationId)
        mFireStoreDB!!.collection(PARENTS_COLLECTION)
            .whereEqualTo("userid", mOtherUserId).limit(1)
            .get().addOnSuccessListener { documentSnapshot ->
                if (!documentSnapshot.isEmpty) {
                    var mBabySitterList: ArrayList<BabysitterModel> = ArrayList<BabysitterModel>()
                    for (document in documentSnapshot) {
                        Log.e("TAG", "${document.id} => ${document.data}")
                        mBabySitterList.add(document.toObject(BabysitterModel::class.java))
                    }
                    val mBabysitterModel: BabysitterModel = mBabySitterList[0]
                    Log.e("TAG","**BabySitterImageUrl**"+mBabysitterModel.imgsource)

                    Glide.with(mActivity!!).load(mBabysitterModel.imgsource)
                        .placeholder(R.drawable.ic_placeholder)
                        .error(R.drawable.ic_placeholder).into(holder.joinedequestIV)

                    if (mBabysitterModel.name!!.isNotEmpty()) {
                        holder.joinedRequestNameTv.setText(mBabysitterModel.name)
                    }
                }
            }





    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList.size > 0)
            return mArrayList.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var joinedequestIV: CircleImageView
        var joinedRequestNameTv: TextView
        var joinedMessageTv: TextView
        var joinedTimeTv: TextView
        var txtChildCountTV: TextView
        var chatPointerView: View
        var requestJoinCountLL: LinearLayout

        init {
            joinedequestIV = itemView.findViewById(R.id.joinedequestIV)
            joinedRequestNameTv = itemView.findViewById(R.id.joinedRequestNameTv)
            joinedMessageTv = itemView.findViewById(R.id.joinedMessageTv)
            txtChildCountTV = itemView.findViewById(R.id.txtChildCountTV)
            joinedTimeTv = itemView.findViewById(R.id.joinedTimeTv)
            chatPointerView = itemView.findViewById(R.id.chatPointerView)
            requestJoinCountLL = itemView.findViewById(R.id.requestJoinCountLL)
        }
    }


}
