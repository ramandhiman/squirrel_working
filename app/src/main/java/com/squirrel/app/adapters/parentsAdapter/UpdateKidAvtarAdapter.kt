package com.squirrel.app.adapters.parentsAdapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squirrel.app.R
import com.squirrel.app.interfaces.AddKidAvtarClickListner
import de.hdodenhof.circleimageview.CircleImageView


public class UpdateKidAvtarAdapter(var mActivity: Activity?, var mArrayList: Array<Int>,
                                   var mAddKidAvtarClickListner : AddKidAvtarClickListner,
                                   var mAvatarPosition : Int) : RecyclerView.Adapter<UpdateKidAvtarAdapter.MyViewHolder>() {

    var lastCheckedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_add_kid_avtar, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.imgAvtarCIV.setImageResource(mArrayList[position])

        holder.itemView.setOnClickListener {
            lastCheckedPosition = position
            mAvatarPosition = lastCheckedPosition
            notifyDataSetChanged()
            mAddKidAvtarClickListner.onAdapterPosition(position)
        }

        if (lastCheckedPosition == position){
            holder.imgAvtarCIV.setBorderColor(mActivity?.resources?.getColor(R.color.green)!!)
            holder.imgTickIV.setVisibility(View.VISIBLE)
        }else{
            holder.imgAvtarCIV.setBorderColor(mActivity?.resources?.getColor(R.color.white)!!)
            holder.imgTickIV.setVisibility(View.GONE)
        }

        if(mAvatarPosition==position) {
            holder.imgAvtarCIV.setBorderColor(mActivity?.resources?.getColor(R.color.green)!!)
            holder.imgTickIV.setVisibility(View.VISIBLE)
        }

    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgAvtarCIV: CircleImageView
        var imgTickIV: ImageView
        init {
            imgAvtarCIV = itemView.findViewById(R.id.imgAvtarCIV)
            imgTickIV = itemView.findViewById(R.id.imgTickIV)
        }
    }

}


