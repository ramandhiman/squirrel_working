package com.squirrel.app.adapters.babysitterAdapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.squirrel.app.R
import com.squirrel.app.activities.babysitter.BabySitterProfileReviewActivity
import com.squirrel.app.interfaces.BabySitterItemClickListner
import com.squirrel.app.models.BabysitterModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ExploreBabySittersAdapter(var mActivity: Activity?, var mArrayList: ArrayList<BabysitterModel>, var mBabySitterItemClickListner : BabySitterItemClickListner) :
    RecyclerView.Adapter<ExploreBabySittersAdapter.MyViewHolder>() {
    var dialog: BottomSheetDialog? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_babysitter, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel = mArrayList.get(position)

        Glide.with(mActivity!!).load(mModel.imgsource).placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder).into(holder.imgProfileIV)

        holder.txtTitleTV.setText(mModel.name + ", " + calculateDateOfBirthWithCurrentTime(mModel.dob!!)  + "y")
        holder.txtDescriptionTV.setText(mModel.location)
        holder.ratingCountTv.setText(mModel.userRating)
        holder.priceTv.setText("$" + mModel.rate + "/hr")
        holder.distanceTv.setText(""+mModel.distance + "km")


        holder.mainLL.setOnClickListener {
         showProfileSheet(mModel)
        }


        holder.imgCloseIV.setOnClickListener {
            mBabySitterItemClickListner.onCloseSwipeListner(mModel, position)
        }

        holder.imgRightIV.setOnClickListener {
            mBabySitterItemClickListner.onRightSwipeListner(mModel, position)
        }

    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtTitleTV: TextView
        var txtDescriptionTV: TextView
        var distanceTv: TextView
        var priceTv: TextView
        var ratingCountTv: TextView
        var imgProfileIV : de.hdodenhof.circleimageview.CircleImageView
        var mainLL : LinearLayout
        var imgRightIV: ImageView
        var imgCloseIV: ImageView

        init {
            imgCloseIV = itemView.findViewById(R.id.imgCloseIV)
            imgRightIV = itemView.findViewById(R.id.imgRightIV)
            txtTitleTV = itemView.findViewById(R.id.txtTitleTV)
            txtDescriptionTV = itemView.findViewById(R.id.txtDescriptionTV)
            distanceTv = itemView.findViewById(R.id.distanceTv)
            priceTv = itemView.findViewById(R.id.priceTv)
            ratingCountTv = itemView.findViewById(R.id.ratingCountTv)
            imgProfileIV = itemView.findViewById(R.id.imgProfileIV)
            mainLL = itemView.findViewById(R.id.mainLL)

        }
    }


    fun createEditDeteleOptionDialog(mActivity: Activity) {
        val mBottomSheetDialog = BottomSheetDialog(mActivity,R.style.AppBottomSheetDialogTheme)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_playdates_options, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val layoutPlayDateLL = sheetView.findViewById<LinearLayout>(R.id.layoutPlayDateLL)
        val layoutDeleteLL = sheetView.findViewById<LinearLayout>(R.id.layoutDeleteLL)

        layoutPlayDateLL.setOnClickListener {
            mBottomSheetDialog.dismiss()
        }

        layoutDeleteLL.setOnClickListener{
            mBottomSheetDialog.dismiss()
            createDeteleClickDialog(mActivity)
        }
    }



    fun createDeteleClickDialog(mActivity: Activity) {
        val mBottomSheetDialog = BottomSheetDialog(mActivity,R.style.AppBottomSheetDialogThemeDelete)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_delete_playdates, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val btnConfirmB = sheetView.findViewById<AppCompatButton>(R.id.btnConfirmB)
        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)

        btnConfirmB.setOnClickListener {
            mBottomSheetDialog.dismiss()
        }

        txtCancelTV.setOnClickListener{
            mBottomSheetDialog.dismiss()
        }
    }


    private fun showProfileSheet(mModel : BabysitterModel) {
        dialog = BottomSheetDialog(mActivity!!)
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.layout_bottom_baby_profile, null)
        dialog!!.setContentView(view)

        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)
        //recyclerview
        val ratingProfileLL: LinearLayout? = dialog!!.findViewById<LinearLayout>(R.id.ratingProfileLL)
        val babySitterProfile: de.hdodenhof.circleimageview.CircleImageView? = dialog!!.findViewById<de.hdodenhof.circleimageview.CircleImageView>(R.id.babySitterProfile)
        val txtBabysitterNameTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtBabysitterNameTV)
        val txtRateTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtRateTV)
        val txtRationTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtRationTV)
        val txtAddressTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtAddressTV)
        val mondayTV: TextView? = dialog!!.findViewById<TextView>(R.id.mondayTV)
        val txtMondayTimeTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtMondayTimeTV)
        val tuesdayTV: TextView? = dialog!!.findViewById<TextView>(R.id.tuesdayTV)
        val txtTusedayTimeTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtTusedayTimeTV)
        val wednesdayTV: TextView? = dialog!!.findViewById<TextView>(R.id.wednesdayTV)
        val txtWednesdayTimeTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtWednesdayTimeTV)
        val thursdayTV: TextView? = dialog!!.findViewById<TextView>(R.id.thursdayTV)
        val txtThursdayTimeTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtThursdayTimeTV)
        val fridayTV: TextView? = dialog!!.findViewById<TextView>(R.id.fridayTV)
        val txtFridayTimeTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtFridayTimeTV)
        val saturdayTV: TextView? = dialog!!.findViewById<TextView>(R.id.saturdayTV)
        val txtSaturdayTimeTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtSaturdayTimeTV)
        val sundayTV: TextView? = dialog!!.findViewById<TextView>(R.id.sundayTV)
        val txtSundayTimeTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtSundayTimeTV)
        val txtUrgentRequestTV: TextView? = dialog!!.findViewById<TextView>(R.id.txtUrgentRequestTV)




        Glide.with(mActivity!!).load(mModel.imgsource).placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder).into(babySitterProfile!!)
        txtBabysitterNameTV?.setText(mModel.name + ", " + calculateDateOfBirthWithCurrentTime(mModel.dob!!))
        txtRateTV?.setText("$" + mModel.rate + "/hr")
        txtRationTV?.setText(mModel.userRating)
        txtAddressTV?.setText(mModel.location)
        if (mModel.isAcceptUrgentRequest == "1"){
            txtUrgentRequestTV?.visibility = View.VISIBLE
        }else{
            txtUrgentRequestTV?.visibility = View.GONE
        }

        //colorVeryLightGrey
        //text_color_default
        for (i in 0..mModel.days.size - 1){
            var mDayModel = mModel.days.get(i)

            if (i == 0) {
                if (i == 0 && mDayModel.isAvailable == "1") {
                    mondayTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    txtMondayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    var mFromArray = mDayModel.from?.split("T")
                    var mToArray = mDayModel.to?.split("T")
                    var mFromToText = mFromArray!![1] + " - " + mToArray!![1]
                    txtMondayTimeTV?.setText(mFromToText)
                } else {
                    mondayTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtMondayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtMondayTimeTV?.setText(mActivity!!.resources.getString(R.string.not_avilable))
                }
            }

            if (i == 1) {
                if (i == 1 && mDayModel.isAvailable == "1") {
                    tuesdayTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    txtTusedayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    var mFromArray = mDayModel.from?.split("T")
                    var mToArray = mDayModel.to?.split("T")
                    var mFromToText = mFromArray!![1] + " - " + mToArray!![1]
                    txtTusedayTimeTV?.setText(mFromToText)
                } else {
                    tuesdayTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtTusedayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtTusedayTimeTV?.setText(mActivity!!.resources.getString(R.string.not_avilable))
                }
            }

            if (i == 2) {
                if (i == 2 && mDayModel.isAvailable == "1") {
                    wednesdayTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    txtWednesdayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    var mFromArray = mDayModel.from?.split("T")
                    var mToArray = mDayModel.to?.split("T")
                    var mFromToText = mFromArray!![1] + " - " + mToArray!![1]
                    txtWednesdayTimeTV?.setText(mFromToText)
                } else {
                    wednesdayTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtWednesdayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtWednesdayTimeTV?.setText(mActivity!!.resources.getString(R.string.not_avilable))
                }
            }
            if (i == 3) {
                if (i == 3 && mDayModel.isAvailable == "1") {
                    thursdayTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    txtThursdayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    var mFromArray = mDayModel.from?.split("T")
                    var mToArray = mDayModel.to?.split("T")
                    var mFromToText = mFromArray!![1] + " - " + mToArray!![1]
                    txtThursdayTimeTV?.setText(mFromToText)
                } else {
                    thursdayTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtThursdayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtThursdayTimeTV?.setText(mActivity!!.resources.getString(R.string.not_avilable))
                }
            }
            if (i == 4) {
                if (i == 4 && mDayModel.isAvailable == "1") {
                    fridayTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    txtFridayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    var mFromArray = mDayModel.from?.split("T")
                    var mToArray = mDayModel.to?.split("T")
                    var mFromToText = mFromArray!![1] + " - " + mToArray!![1]
                    txtFridayTimeTV?.setText(mFromToText)
                } else {
                    fridayTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtFridayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtFridayTimeTV?.setText(mActivity!!.resources.getString(R.string.not_avilable))
                }
            }
            if (i == 5) {
                if (i == 5 && mDayModel.isAvailable == "1") {
                    saturdayTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    txtSaturdayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    var mFromArray = mDayModel.from?.split("T")
                    var mToArray = mDayModel.to?.split("T")
                    var mFromToText = mFromArray!![1] + " - " + mToArray!![1]
                    txtSaturdayTimeTV?.setText(mFromToText)
                } else {
                    saturdayTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtSaturdayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtSaturdayTimeTV?.setText(mActivity!!.resources.getString(R.string.not_avilable))
                }
            }
            if (i == 6) {
                if (i == 6 && mDayModel.isAvailable == "1") {
                    sundayTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    txtSundayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.text_color_default))
                    var mFromArray = mDayModel.from?.split("T")
                    var mToArray = mDayModel.to?.split("T")
                    var mFromToText = mFromArray!![1] + " - " + mToArray!![1]
                    txtSundayTimeTV?.setText(mFromToText)
                } else {
                    sundayTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtSundayTimeTV?.setTextColor(mActivity!!.resources.getColor(R.color.colorVeryLightGrey))
                    txtSundayTimeTV?.setText(mActivity!!.resources.getString(R.string.not_avilable))
                }
            }
        }

        ratingProfileLL?.setOnClickListener {
//            val intent=Intent(mActivity,BabySitterProfileReviewActivity::class.java)
//            mActivity!!.startActivity(intent)
//            dialog?.dismiss()
        }

        dialog!!.setCanceledOnTouchOutside(true)
        //disabling the drag down of sheet
        dialog!!.setCancelable(true)
        dialog!!.show()
    }


    //Calculate Date of Birth:
    fun calculateDateOfBirthWithCurrentTime(mDob: String): String {
        var mDatePrevious: Date? = null
        var mDateCurrent: Date? = null
        val mDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        mDatePrevious = mDateFormat.parse(mDob)
        mDateCurrent = mDateFormat.parse(getCurrentDateInDobFormat())

        val difference: Long = Math.abs(mDatePrevious.getTime() - mDateCurrent.getTime())
        val differenceDates = difference / (24 * 60 * 60 * 1000)
        val yearDifference = differenceDates / 365
        return "" + yearDifference
    }

    //YYYY-MM-DDTHH:mm:ss
    fun getCurrentDateInDobFormat(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val currentDateandTime = sdf.format(Date())
        return currentDateandTime
    }

}
