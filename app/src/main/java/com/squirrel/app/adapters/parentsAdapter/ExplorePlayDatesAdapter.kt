package com.squirrel.app.adapters.parentsAdapter

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import com.squirrel.app.interfaces.PlayDatetemClickListner
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.models.ParentModel
import com.squirrel.app.models.PlayDatesModel
import com.squirrel.app.utils.AVTAR_ARRAY
import com.squirrel.app.utils.KIDS_COLLECTION
import com.squirrel.app.utils.PARENTS_COLLECTION
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ExplorePlayDatesAdapter(var mActivity: Activity?, var mArrayList: ArrayList<PlayDatesModel>,
                              var mPlayDatetemClickListner : PlayDatetemClickListner) :
    RecyclerView.Adapter<ExplorePlayDatesAdapter.MyViewHolder>() {

    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_explore_home_playdate, parent,false)


        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel  = mArrayList.get(position)
        holder.txtDescriptionTV.setText(mModel.title)

        var mChildCount = mModel.childList.size - 1
        if (mChildCount >= 1){
            holder.txtChildCountTV.setText("+"+mChildCount)
            holder.txtChildCountTV.visibility = View.VISIBLE
        }else{
            holder.txtChildCountTV.visibility = View.GONE
        }


        holder.txtDistanceTV.setText(""+mModel.distance + "km")


        if (mModel.groupEvent == true){
            holder.txtGroupTV.visibility = View.VISIBLE
        }else{
            holder.txtGroupTV.visibility = View.GONE
        }

        var mArray = mModel.date?.split("T")
        var mDate = mArray?.get(0)
        var mTime = mArray?.get(1)

        var mDateArray = mDate?.split("-")
        holder.txtDayTV.setText(mDateArray?.get(2))
        holder.txtMonthTV.setText(getMonthSortName(mDateArray?.get(1)!!))

        var mTimeArray = mTime?.split(":")
        holder.txtTimeTV.setText(mTimeArray?.get(0) + ":" + mTimeArray?.get(1))


        holder.imgMenuIV.setOnClickListener {
            createEditDeteleOptionDialog(mActivity!!)
        }

        holder.itemView.setOnClickListener{
            mPlayDatetemClickListner.onItemClickListner(mModel)
        }


        mFireStoreDB!!.collection(PARENTS_COLLECTION).document(mModel.parentID!!).get().addOnSuccessListener {documentSnapshot ->
            if (documentSnapshot.exists()) {
                val mParentModel: ParentModel = documentSnapshot.toObject(ParentModel::class.java)!!
                Glide.with(mActivity!!).load(mParentModel.imgsource)
                    .placeholder(R.drawable.ic_placeholder)
                    .error(R.drawable.ic_placeholder).into(holder.imgParentProfileIV)
            }
        }.addOnFailureListener {
            Glide.with(mActivity!!).load(R.drawable.ic_placeholder)
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder).into(holder.imgParentProfileIV)
        }

        //Getting Child Details
        if (mModel.childList.isNotEmpty() && mModel.childList.size > 0) {
            mFireStoreDB!!.collection(KIDS_COLLECTION).document(mModel.childList.get(0))
                .get().addOnSuccessListener { documentSnapshot ->
                    if (documentSnapshot.exists()) {
                        val mKidModel: MyKidsModel = documentSnapshot.toObject(MyKidsModel::class.java)!!
                        var mAvtarPos = Integer.parseInt(mKidModel.avatar)
                        Glide.with(mActivity!!).load(AVTAR_ARRAY[mAvtarPos])
                            .placeholder(R.drawable.ic_placeholder)
                            .error(R.drawable.ic_placeholder).into(holder.imgProfileIV)
                        if (mKidModel.name!!.isNotEmpty()) {
                            holder.txtTitleTV.setText(mKidModel.name + ", " + calculateDateOfBirthWithCurrentTime(mKidModel.dob!!)   + "y")
                        }else{
                            holder.txtTitleTV.setText(mActivity!!.resources.getString(R.string.no_kids_were_selected))
                        }
                    }else{
                        holder.txtTitleTV.setText(mActivity!!.resources.getString(R.string.no_kids_were_selected))
                    }
                }
                .addOnFailureListener { e ->
                    Log.e("TAG","**Error**"+e.message)
                    holder.txtTitleTV.setText(mActivity!!.resources.getString(R.string.no_kids_were_selected))
                }


            holder.imgCloseIV.setOnClickListener {
                mPlayDatetemClickListner.onCloseSwipeListner(mModel)
            }

            holder.imgRightIV.setOnClickListener {
                mPlayDatetemClickListner.onRightSwipeListner(mModel)
            }
        }


    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtTitleTV: TextView
        var txtDayTV: TextView
        var txtTimeTV: TextView
        var txtMonthTV: TextView
        var txtDescriptionTV: TextView
        var txtGroupTV: TextView
        var txtChildCountTV: TextView
        var txtDistanceTV: TextView
        var imgMenuIV: ImageView
        var imgRightIV: ImageView
        var imgCloseIV: ImageView
        var imgProfileIV: de.hdodenhof.circleimageview.CircleImageView
        var imgParentProfileIV: de.hdodenhof.circleimageview.CircleImageView

        init {
            imgCloseIV = itemView.findViewById(R.id.imgCloseIV)
            imgRightIV = itemView.findViewById(R.id.imgRightIV)
            txtTitleTV = itemView.findViewById(R.id.txtTitleTV)
            imgMenuIV = itemView.findViewById(R.id.imgMenuIV)
            imgProfileIV = itemView.findViewById(R.id.imgProfileIV)
            txtDayTV = itemView.findViewById(R.id.txtDayTV)
            txtChildCountTV = itemView.findViewById(R.id.txtChildCountTV)
            txtTimeTV = itemView.findViewById(R.id.txtTimeTV)
            txtMonthTV = itemView.findViewById(R.id.txtMonthTV)
            txtDescriptionTV = itemView.findViewById(R.id.txtDescriptionTV)
            txtDistanceTV = itemView.findViewById(R.id.txtDistanceTV)
            txtGroupTV = itemView.findViewById(R.id.txtGroupTV)
            imgParentProfileIV = itemView.findViewById(R.id.imgParentProfileIV)
        }
    }


    fun createEditDeteleOptionDialog(mActivity: Activity) {
        val mBottomSheetDialog = BottomSheetDialog(mActivity,R.style.AppBottomSheetDialogTheme)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_playdates_options, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val layoutPlayDateLL = sheetView.findViewById<LinearLayout>(R.id.layoutPlayDateLL)
        val layoutDeleteLL = sheetView.findViewById<LinearLayout>(R.id.layoutDeleteLL)

        layoutPlayDateLL.setOnClickListener {
            mBottomSheetDialog.dismiss()
        }

        layoutDeleteLL.setOnClickListener{
            mBottomSheetDialog.dismiss()
            createDeteleClickDialog(mActivity)
        }
    }



    fun createDeteleClickDialog(mActivity: Activity) {
        val mBottomSheetDialog = BottomSheetDialog(mActivity,R.style.AppBottomSheetDialogThemeDelete)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_delete_playdates, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val btnConfirmB = sheetView.findViewById<AppCompatButton>(R.id.btnConfirmB)
        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)

        btnConfirmB.setOnClickListener {
            mBottomSheetDialog.dismiss()
        }

        txtCancelTV.setOnClickListener{
            mBottomSheetDialog.dismiss()
        }
    }



    fun getMonthSortName(monthNo : String) : String{
        if (monthNo == "01"){
            return "JAN"
        }else if (monthNo == "02"){
            return "FEB"
        }else if (monthNo == "03"){
            return "MAR"
        }else if (monthNo == "04"){
            return "APR"
        }else if (monthNo == "05"){
            return "MAY"
        }else if (monthNo == "06"){
            return "JUN"
        }else if (monthNo == "07"){
            return "JUL"
        }else if (monthNo == "08"){
            return "AUG"
        }else if (monthNo == "09"){
            return "SEP"
        }else if (monthNo == "10"){
            return "OCT"
        }else if (monthNo == "11"){
            return "NOV"
        }else if (monthNo == "12"){
            return "DEC"
        }else{
            return ""
        }
    }

    //Calculate Date of Birth:
    fun calculateDateOfBirthWithCurrentTime(mDob : String) : String{
        var mDatePrevious : Date? = null
        var mDateCurrent : Date? = null
        val mDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        mDatePrevious = mDateFormat.parse(mDob)
        mDateCurrent = mDateFormat.parse(getCurrentDateInDobFormat())

        val difference: Long = Math.abs(mDatePrevious.getTime() - mDateCurrent.getTime())
        val differenceDates = difference / (24 * 60 * 60 * 1000)
        val yearDifference = differenceDates / 365
        return ""+yearDifference
    }

    //YYYY-MM-DDTHH:mm:ss
    fun getCurrentDateInDobFormat() : String{
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val currentDateandTime = sdf.format(Date())
        return currentDateandTime
    }

}
