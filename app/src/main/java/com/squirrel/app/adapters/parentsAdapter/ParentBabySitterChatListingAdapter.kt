package com.squirrel.app.adapters.parentsAdapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squirrel.app.R
import com.squirrel.app.models.MessageModel
import com.squirrel.app.utils.LEFT_VIEW_HOLDER
import com.squirrel.app.utils.RIGHT_VIEW_HOLDER
import com.squirrel.app.viewholder.ItemLeftViewHolder
import com.squirrel.app.viewholder.ItemRightViewHolder
import kotlin.collections.ArrayList


class ParentBabySitterChatListingAdapter(var mActivity: Activity?, var mArrayList : ArrayList<MessageModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == RIGHT_VIEW_HOLDER) {
            var mView: View = LayoutInflater.from(mActivity).inflate(R.layout.item_chat_right_row, parent, false)
            return ItemRightViewHolder(mView)
        } else if (viewType == LEFT_VIEW_HOLDER) {
            var mView: View = LayoutInflater.from(mActivity).inflate(R.layout.item_chat_left_row, parent, false)
            return ItemLeftViewHolder(mView)
        }
        return null!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if (holder.itemViewType == RIGHT_VIEW_HOLDER) {
                (holder as ItemRightViewHolder).bindData(mActivity, mArrayList!![position] as MessageModel?)
            } else if (holder.itemViewType == LEFT_VIEW_HOLDER) {
                (holder as ItemLeftViewHolder).bindData(mActivity, mArrayList!![position] as MessageModel?)
            }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }


    override fun getItemViewType(position: Int): Int {
        if (mArrayList!![position]!!.viewType == 1) return RIGHT_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 0) return LEFT_VIEW_HOLDER
        return -1
    }


}


