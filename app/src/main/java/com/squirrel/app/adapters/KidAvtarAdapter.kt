package com.squirrel.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squirrel.app.R
import com.squirrel.app.activities.parent.AddKidActivity
import com.squirrel.app.interfaces.AvtarClickListner
import de.hdodenhof.circleimageview.CircleImageView


public class KidAvtarAdapter(
    var mActivity: Activity?,
    var mArrayList: Array<Int>,
    var mAvtarClickListner : AvtarClickListner,
    var mType : Int
) :
    RecyclerView.Adapter<KidAvtarAdapter.MyViewHolder>()
{

    var lastCheckedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_kid_avtar, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel = mArrayList!!.get(position)
        holder.imgAvtarCIV.setImageResource(mArrayList[position])
        holder.itemView.setOnClickListener {
            lastCheckedPosition = position
            notifyDataSetChanged()
            mAvtarClickListner.onAdapterPosition(position, mType)
        }
        if (lastCheckedPosition == position){
            holder.imgAvtarCIV.setBorderColor(mActivity?.resources?.getColor(R.color.green)!!)
            holder.imgTickIV.setVisibility(View.VISIBLE)
        }else{
            holder.imgAvtarCIV.setBorderColor(mActivity?.resources?.getColor(R.color.white)!!)
            holder.imgTickIV.setVisibility(View.GONE)
        }


    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgAvtarCIV: CircleImageView
        var imgTickIV: ImageView
        init {
            imgAvtarCIV = itemView.findViewById(R.id.imgAvtarCIV)
            imgTickIV = itemView.findViewById(R.id.imgTickIV)
        }
    }
    interface  positionCallBack{
        fun getAvatarPosition(position: Int)
    }

}


