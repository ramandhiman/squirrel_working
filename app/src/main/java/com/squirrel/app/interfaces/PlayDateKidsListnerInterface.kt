package com.squirrel.app.interfaces

import com.squirrel.app.models.MyKidsModel

interface PlayDateKidsListnerInterface {
    fun onSelectedKidListner(mArrayList : ArrayList<MyKidsModel>)
}