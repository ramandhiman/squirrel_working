package com.squirrel.app.interfaces

import com.squirrel.app.models.BabysitterModel


interface BabySitterItemClickListner {
    fun onItemClickListner(mModel : BabysitterModel, pos : Int){
    }

    fun onCloseSwipeListner(mModel : BabysitterModel, pos : Int){

    }

    fun onRightSwipeListner(mModel : BabysitterModel, pos : Int){

    }
}