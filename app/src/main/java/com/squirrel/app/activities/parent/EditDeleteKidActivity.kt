package com.squirrel.app.activities.parent

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import com.squirrel.app.adapters.parentsAdapter.UpdateKidAvtarAdapter
import com.squirrel.app.interfaces.AddKidAvtarClickListner
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_edit_delete_kid.*
import kotlinx.android.synthetic.main.layout_my_kids_toolbar.*

class EditDeleteKidActivity : AppCompatActivity() {
    var TAG = this@EditDeleteKidActivity.javaClass.simpleName
    var mActivity: Activity = this@EditDeleteKidActivity
    var mGenderType: String = ""
    var mAvatarPosition = -1
    var mMyKidsModel : MyKidsModel? = null
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_delete_kid)
        ButterKnife.bind(this)
        getIntentData()
        statusBarColor(mActivity)
        setUpToolBar()
    }

    private fun getIntentData() {
        if (intent != null){
            mMyKidsModel = intent.getParcelableExtra(KIDS_OBJECT)
            setDataOnWidgets()
        }
    }

    private fun setDataOnWidgets() {
        mGenderType = mMyKidsModel?.gender!!
        mAvatarPosition = Integer.parseInt(mMyKidsModel?.avatar!!)
        Log.e(TAG,"**Position**$mAvatarPosition")
        editNameET.setText(mMyKidsModel?.name)
        editNameET.setSelection(mMyKidsModel?.name!!.length)

        txtDateOfBirthKidTV.setText(convertDBToDobFormat(mMyKidsModel?.dob!!))

        var mGenderPos = Integer.parseInt(mMyKidsModel?.gender)
        tabSelection(mGenderPos, txtMaleKidTV, txtFemaleKidTV, txtOthersKidTV, editSpecificTV)

        if (mMyKidsModel?.gender == "2"){
            editSpecificTV.visibility = View.VISIBLE
            editSpecificTV.setText(mMyKidsModel?.genderText)
            editSpecificTV.setSelection(mMyKidsModel?.genderText!!.length)
        }else{
            editSpecificTV.visibility = View.GONE
        }

        editDecriptionET.setText(mMyKidsModel?.hobbies)
        editDecriptionET.setSelection(mMyKidsModel?.hobbies!!.length)

        setupKidsAvatarRV()
    }



    private fun setUpToolBar() {
        txtHeadingKidsTV.setText(getString(R.string.edit_))
        txtDeleteTv.visibility = View.VISIBLE
    }

    @OnClick(
        R.id.imgBackKidsIV,
        R.id.txtDateOfBirthKidTV,
        R.id.txtMaleKidTV,
        R.id.txtFemaleKidTV,
        R.id.txtOthersKidTV,
        R.id.txtDeleteTv,
        R.id.btnSave
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackKidsIV -> onBackPressed()
            R.id.txtDateOfBirthKidTV -> showDatePicker(txtDateOfBirthKidTV)
            R.id.txtMaleKidTV ->  tabSelection(0, txtMaleKidTV, txtFemaleKidTV, txtOthersKidTV, editSpecificTV)
            R.id.txtFemaleKidTV ->  tabSelection(1, txtMaleKidTV, txtFemaleKidTV, txtOthersKidTV, editSpecificTV)
            R.id.txtOthersKidTV ->  tabSelection(2, txtMaleKidTV, txtFemaleKidTV, txtOthersKidTV, editSpecificTV)
            R.id.txtDeleteTv -> performDeleteClick()
            R.id.btnSave -> performSaveClick()
        }
    }

    private fun performDeleteClick() {
        showDeleteKidAlertDialog()
    }


    private fun performSaveClick() {
        if(isValidate()) {
            if (isNetworkAvailable(mActivity)){
                executeUpdateKidData()
            }else{
                showToast(mActivity,getString(R.string.internet_connection_error))
            }
        }
    }

    private fun executeUpdateKidData() {
        //Insert data in DB
        val mKidData = hashMapOf(
            "avatar" to ""+mAvatarPosition,
            "dob" to convertDobToDbFormat(txtDateOfBirthKidTV.text.toString().trim()),
            "firebaseID" to mMyKidsModel?.firebaseID,
            "gender" to mGenderType,
            "genderText" to editSpecificTV.text.toString().trim(),
            "hobbies" to editDecriptionET.text.toString().trim(),
            "name" to editNameET.text.toString().trim(),
            "parentID" to getUserID(mActivity),
            "updatedAt" to updatedAt())
        showProgressDialog(mActivity)
        mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mMyKidsModel?.firebaseID!!)
            ?.update(mKidData)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                showToast(mActivity,getString(R.string.kid_updated_successfully))
                finish()
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                Log.e(TAG, "Error: ${e.toString()}")
                showAlertDialog(mActivity,e.localizedMessage)
            }

    }


    fun setupKidsAvatarRV() {
        //Add Avtar Images
        var mAdapter : UpdateKidAvtarAdapter = UpdateKidAvtarAdapter(mActivity, AVTAR_ARRAY,mAddKidAvtarClickListner, mAvatarPosition)
        avatarPickKidsRV.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL,false)
        avatarPickKidsRV.setHasFixedSize(true)
        avatarPickKidsRV.adapter = mAdapter
    }

    var mAddKidAvtarClickListner : AddKidAvtarClickListner = object  : AddKidAvtarClickListner {
        override fun onAdapterPosition(pos: Int) {
            mAvatarPosition = pos
        }
    }



    // - - To Change the color of Icon and Text When Tab is selected
    private fun tabSelection(mPos: Int, txtMaleTV: TextView, txtFemaleTV: TextView, txtOthersTV: TextView, txtSpecificTV: EditText) {
        val mTypeface = Typeface.createFromAsset(assets, "poppins_regular.ttf")
        val mTypefaceBold = Typeface.createFromAsset(assets, "poppins_bold.ttf")
        txtMaleTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtMaleTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtMaleTV.setTypeface(mTypeface)
        txtFemaleTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtFemaleTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtFemaleTV.setTypeface(mTypeface)
        txtOthersTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtOthersTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtOthersTV.setTypeface(mTypeface)
        when (mPos) {
            0 -> {
                mGenderType="0"
                txtSpecificTV.visibility = View.GONE
                txtMaleTV.getBackground().setColorFilter(getResources().getColor(R.color.box_color_highlight), PorterDuff.Mode.SRC_ATOP)
                txtMaleTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtMaleTV.setTypeface(mTypefaceBold)
            }
            1 -> {
                mGenderType="1"
                txtSpecificTV.visibility = View.GONE
                txtFemaleTV.getBackground().setColorFilter(getResources().getColor(R.color.box_color_highlight), PorterDuff.Mode.SRC_ATOP)
                txtFemaleTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtFemaleTV.setTypeface(mTypefaceBold)
            }
            2 -> {
                mGenderType="2"
                txtSpecificTV.visibility = View.VISIBLE
                txtOthersTV.getBackground().setColorFilter(getResources().getColor(R.color.box_color_highlight), PorterDuff.Mode.SRC_ATOP)
                txtOthersTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtOthersTV.setTypeface(mTypefaceBold)
            }
        }
    }



    fun isValidate(): Boolean {
        var flag = true
        when {
            editNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_name))
                flag = false
            }
            txtDateOfBirthKidTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_date_of_birth))
                flag = false
            }
            mGenderType == ""   -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_gender))
                flag = false
            }
            mGenderType == "2" && editSpecificTV.text.toString().trim{ it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_specify_gender))
                flag = false
            }
            editDecriptionET.text.toString().trim{ it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_provide_the_about_kid))
                flag = false
            }

        }
        return flag
    }


    /*
    * Delete Kid
    * */
    fun showDeleteKidAlertDialog() {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_delete_kid)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtCancelTV = alertDialog.findViewById<TextView>(R.id.txtCancelTV)
        val txtDeleteTV = alertDialog.findViewById<TextView>(R.id.txtDeleteTV)

        txtCancelTV.setOnClickListener { alertDialog.dismiss() }
        txtDeleteTV.setOnClickListener {
            alertDialog.dismiss()
            showProgressDialog(mActivity)
            mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))!!.update("kidsList", FieldValue.arrayRemove(mMyKidsModel?.firebaseID!!))
            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mMyKidsModel?.firebaseID!!)?.delete()
                ?.addOnSuccessListener {
                    dismissProgressDialog()
                    showToast(mActivity,getString(R.string.kid_deleted_successfully))
                    finish()
                 }?.addOnFailureListener { e ->
                    Log.e(TAG, "Error deleting document", e)
                    dismissProgressDialog()
                }
        }

        alertDialog.show()
    }

}