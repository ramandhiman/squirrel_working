package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squirrel.app.adapters.babysitterAdapters.BabySitterSendRequestChildAdapter
import com.squirrel.app.adapters.parentsAdapter.PlaydateSendRequestChildAdapter
import com.squirrel.app.interfaces.PlayDateKidsListnerInterface
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_send_babysitter_request.*
import kotlinx.android.synthetic.main.layout_request_toolbar.*

class SendBabySitterRequestActivity : AppCompatActivity() {
    var TAG = this@SendBabySitterRequestActivity.javaClass.simpleName
    var mActivity: Activity = this@SendBabySitterRequestActivity
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mKidsList : ArrayList<MyKidsModel>  = ArrayList()
    var mAdapter : BabySitterSendRequestChildAdapter? = null
    var mSelectedKidsList : ArrayList<MyKidsModel>  = ArrayList()
    var mArrayKidsList : ArrayList<String>  = ArrayList()
    var mBabysitterModel: BabysitterModel? = null
    var mPos: Int =0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_babysitter_request)
        ButterKnife.bind(this)
        statusRequestColor(mActivity,getString(R.string.babySitterTag))
        setUpToolBar()
        getIntentData()
    }

    private fun getIntentData() {
        if (intent != null){
            mBabysitterModel = intent.getParcelableExtra(MODEL)
            mPos = intent.getIntExtra(POS,0)
            getChildDetailsData()
        }
    }


    private fun setUpToolBar() {
        txtRequestTV.setText(getString(R.string.sendRequestTitle))
    }


    @OnClick(
        R.id.imgCrossIV,
        R.id.btnSendRequest,
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> onBackPressed()
            R.id.btnSendRequest -> performSendRequestClick()

        }
    }

    private fun performSendRequestClick() {
        if (isNetworkAvailable(mActivity)){
            if (mSelectedKidsList.isNotEmpty()){
                sendRequest()
            }else{
                showAlertDialog(mActivity,getString(R.string.please_select_the_child_to_whom))
            }

        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }


    private fun getChildDetailsData() {
        if (isNetworkAvailable(mActivity)){
            executeAllKidsData()
        }else{
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    fun executeAllKidsData() {
        showProgressDialog(mActivity)
        mFireStoreDB!!.collection(KIDS_COLLECTION).orderBy("updatedAt", Query.Direction.ASCENDING)
            .whereEqualTo("parentID",getUserID(mActivity))
            .get().addOnSuccessListener {documentSnapshot ->
                dismissProgressDialog()
                mKidsList.clear()
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mKidsList.add(document.toObject(MyKidsModel::class.java))
                    }
                    Log.e(TAG,"**KidsList Size**${mKidsList.size}")
                    setKidsAdapter()
                } else {
                    showToast(mActivity, getString(R.string.no_kids))
                }
            }
            .addOnFailureListener { e ->
                dismissProgressDialog()
                showAlertDialog(mActivity,getString(R.string.some_thing_went_wrong))
            }
    }

    private fun setKidsAdapter() {
        if (mKidsList != null && mKidsList.size == 1){
            mKidsList[0].isChecked = true
            mSelectedKidsList = mKidsList
        }
        mAdapter = BabySitterSendRequestChildAdapter(mActivity, mKidsList,mPlayDateKidsListnerInterface)
        mChildRV.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL,false)
        mChildRV.setHasFixedSize(true)
        mChildRV.adapter = mAdapter
    }

    var mPlayDateKidsListnerInterface : PlayDateKidsListnerInterface = object  :
        PlayDateKidsListnerInterface {
        override fun onSelectedKidListner(mArrayList: ArrayList<MyKidsModel>) {
            mSelectedKidsList = mArrayList
            Log.e(TAG,"**SIZE**${mSelectedKidsList.size}")
        }
    }



    private fun sendRequest() {
        showProgressDialog(mActivity)
        for (i in 0 until mSelectedKidsList.size){
            mArrayKidsList.add(mSelectedKidsList[i].firebaseID!!)
        }
        var mRequestID =  mFireStoreDB?.collection(REQUESTS_COLLECTION)?.document()!!.id
        Log.e(TAG,"**REQUEST_ID**$mRequestID")
        val mRequestMap = hashMapOf(
            "createdAt" to updatedAt(),
            "id" to mRequestID,
            "isForBabysitter" to "1",
            "kids" to mArrayKidsList,
            "playdate_id" to mRequestID,
            "playdate_name" to getName(mActivity),
            "playdate_parent_id" to mBabysitterModel?.userid,
            "request_parent_id" to getUserID(mActivity),
            "status" to "0")
        mFireStoreDB?.collection(REQUESTS_COLLECTION)?.document(mRequestID)
            ?.set(mRequestMap)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                showReturnAlertDialog(mActivity,getString(R.string.babysitter_request_sent))
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                showAlertDialog(mActivity,e.localizedMessage)
            }
    }



    /*
   *
   * Finish Error Alert Dialog
   * */
    fun showReturnAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            var mIntent = Intent()
            mIntent.putExtra(MODEL,mBabysitterModel)
            mIntent.putExtra(POS,mPos)
            setResult(444,mIntent)
            mActivity.finish()
        }
        alertDialog.show()
    }

}