package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_edit_baby_sitters_dobactivity.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*
import android.content.Intent
import com.squirrel.app.utils.PREVIOUS_DATA
import com.squirrel.app.utils.getDateOfBirth
import com.squirrel.app.utils.showDatePicker
import com.squirrel.app.utils.statusBarColor


class EditBabySittersDOBActivity : AppCompatActivity() {
    var TAG = this@EditBabySittersDOBActivity.javaClass.simpleName
    var mActivity: Activity = this@EditBabySittersDOBActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_baby_sitters_dobactivity)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setDataOnWidget()
    }

    private fun setDataOnWidget() {
        dateTv.setText(getDateOfBirth(mActivity))
    }

    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.edit_dob_text))

    }

    @OnClick(
        R.id.imgBackIV,
        R.id.btnSaveB,
        R.id.dobLL,
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnSaveB -> performSaveDobAction()
            R.id.dobLL -> showDatePicker(dateTv)

        }
    }

    private fun performSaveDobAction() {
        val intent = Intent()
        intent.putExtra(PREVIOUS_DATA,dateTv.text.toString().trim())
        setResult(333, intent)
        finish()
    }
}