package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.statusBarColor
import kotlinx.android.synthetic.main.activity_profile_pending.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class ProfilePendingActivity : AppCompatActivity() {
    var TAG = this@ProfilePendingActivity.javaClass.simpleName
    var mActivity: Activity = this@ProfilePendingActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_pending)
        ButterKnife.bind(this)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
    }
    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.setup_profile))
        val mTitleText = "<font color=#0b0f35>Profile is</font> <font color=#10B502>pending</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.txtSupportTV,
    R.id.imgIV
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.txtSupportTV -> performSupportClick()
            R.id.imgIV -> performBabySitterHomeClick()

        }
    }

    private fun performBabySitterHomeClick() {
//        val intent=Intent(mActivity,BabySItterNewHomeActivity::class.java)
//        startActivity(intent)
    }

    private fun performSupportClick() {
        val intent = Intent(Intent.ACTION_SEND)
        val strTo = arrayOf("squirrelsupport@gmail.com")
        intent.putExtra(Intent.EXTRA_EMAIL, strTo)
        intent.putExtra(Intent.EXTRA_SUBJECT, "Squirrel Support")
        intent.putExtra(Intent.EXTRA_TEXT, "Send from my Android:")
        intent.type = "message/rfc822"
        intent.setPackage("com.google.android.gm")
        startActivity(intent)
    }


}