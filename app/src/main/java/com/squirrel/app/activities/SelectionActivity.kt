package com.squirrel.app.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import com.squirrel.app.utils.statusBarColor

class SelectionActivity : AppCompatActivity() {
    var TAG = this@SelectionActivity.javaClass.simpleName
    var mActivity: Activity = this@SelectionActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selection)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
    }

    @OnClick(
        R.id.btnContinueWithPhoneB
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnContinueWithPhoneB -> performContinueWithPhoneClick()
        }
    }

    private fun performContinueWithPhoneClick() {
        startActivity(Intent(mActivity,EnterPhoneNoActivity::class.java))
    }


}