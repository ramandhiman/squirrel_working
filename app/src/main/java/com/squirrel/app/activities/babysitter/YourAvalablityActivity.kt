package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.models.BabysitterDays
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_your_avalablity.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class YourAvalablityActivity : AppCompatActivity() {
    var TAG = this@YourAvalablityActivity.javaClass.simpleName
    var mActivity: Activity = this@YourAvalablityActivity
    var mAlwaysAvailable : String = "0"
    var mAcceptUrgentRequest : String = "0"
    var mondayChecked: String = "0"
    var tuesdayChecked: String = "0"
    var wednesdayChecked: String = "0"
    var thirsdayChecked: String = "0"
    var fridayChecked: String = "0"
    var saturdayChecked: String = "0"
    var sundayChecked: String = "0"
    var mDaysList : ArrayList<BabysitterDays> = ArrayList()

    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_your_avalablity)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setUpValidations()
    }

    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.setup_profile))
        val mTitleText = "<font color=#0b0f35>Your</font> <font color=#10B502>availability</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.btnNextB,
        R.id.txtMondayFromTV,
        R.id.txtMondayToTV,
        R.id.txtTusedayFromTV,
        R.id.txtTusedayToTV,
        R.id.txtWednesdayFromTV,
        R.id.txtWednesdayToTV,
        R.id.txtThursdayFromTV,
        R.id.txtThursdayToTV,
        R.id.txtFridayFromTV,
        R.id.txtFridayToTV,
        R.id.txtSaturdayFromTV,
        R.id.txtSaturdayToTV,
        R.id.txtSundayFromTV,
        R.id.txtSundayToTV
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnNextB -> performNextClick()
            R.id.txtMondayFromTV -> createFromToTimeDialog(mActivity,txtMondayFromTV)
            R.id.txtMondayToTV -> createFromToTimeDialog(mActivity,txtMondayToTV)
            R.id.txtTusedayFromTV -> createFromToTimeDialog(mActivity,txtTusedayFromTV)
            R.id.txtTusedayToTV -> createFromToTimeDialog(mActivity,txtTusedayToTV)
            R.id.txtWednesdayFromTV -> createFromToTimeDialog(mActivity,txtWednesdayFromTV)
            R.id.txtWednesdayToTV -> createFromToTimeDialog(mActivity,txtWednesdayToTV)
            R.id.txtThursdayFromTV -> createFromToTimeDialog(mActivity,txtThursdayFromTV)
            R.id.txtThursdayToTV -> createFromToTimeDialog(mActivity,txtThursdayToTV)
            R.id.txtFridayFromTV -> createFromToTimeDialog(mActivity,txtFridayFromTV)
            R.id.txtFridayToTV -> createFromToTimeDialog(mActivity,txtFridayToTV)
            R.id.txtSaturdayFromTV -> createFromToTimeDialog(mActivity,txtSaturdayFromTV)
            R.id.txtSaturdayToTV -> createFromToTimeDialog(mActivity,txtSaturdayToTV)
            R.id.txtSundayFromTV -> createFromToTimeDialog(mActivity,txtSundayFromTV)
            R.id.txtSundayToTV -> createFromToTimeDialog(mActivity,txtSundayToTV)
        }
    }


    private fun performNextClick() {
        if (isValidate()){
            if (isNetworkAvailable(mActivity)){
                executeSubmitDaysRequest()
            }else{
                showToast(mActivity,getString(R.string.internet_connection_error))
            }
        }
    }

    private fun executeSubmitDaysRequest() {

            var mDay1 : BabysitterDays = BabysitterDays("1",mondayChecked,getCurrentDateInFormat()+ "T"+txtMondayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtMondayToTV.text.toString().trim())
            var mDay2 : BabysitterDays = BabysitterDays("2",tuesdayChecked,getCurrentDateInFormat()+ "T"+txtTusedayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtTusedayToTV.text.toString().trim())
            var mDay3 : BabysitterDays = BabysitterDays("3",wednesdayChecked,getCurrentDateInFormat()+ "T"+txtWednesdayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtWednesdayToTV.text.toString().trim())
            var mDay4 : BabysitterDays = BabysitterDays("4",thirsdayChecked,getCurrentDateInFormat()+ "T"+txtThursdayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtThursdayToTV.text.toString().trim())
            var mDay5 : BabysitterDays = BabysitterDays("5",fridayChecked,getCurrentDateInFormat()+ "T"+txtFridayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtFridayToTV.text.toString().trim())
            var mDay6 : BabysitterDays = BabysitterDays("6",saturdayChecked,getCurrentDateInFormat()+ "T"+txtSaturdayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtSaturdayToTV.text.toString().trim())
            var mDay7 : BabysitterDays = BabysitterDays("7",sundayChecked,getCurrentDateInFormat()+ "T"+txtSundayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtSundayToTV.text.toString().trim())

            mDaysList.add(mDay1)
            mDaysList.add(mDay2)
            mDaysList.add(mDay3)
            mDaysList.add(mDay4)
            mDaysList.add(mDay5)
            mDaysList.add(mDay6)
            mDaysList.add(mDay7)


        showProgressDialog(mActivity)

        val mDaysData = hashMapOf(
            "rate" to editNameET.text.toString().trim(),
            "isAlwaysAvailable" to mAlwaysAvailable,
            "isAcceptUrgentRequest" to mAcceptUrgentRequest,
            "updatedAt" to updatedAt(),
            "days" to mDaysList
        )

        mFireStoreDB?.collection(BABYSITTERS_COLLECTION)?.document(getUserID(mActivity))
            ?.update(mDaysData as Map<String, Any>)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                val intent= Intent(mActivity, EnableLocationBabySitterActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                Log.e(TAG, "Error: ${e.toString()}")
            }



    }


    // - - Validations...
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_hourly_rate_information))
                flag = false
            }

            mondaySB.isChecked && txtMondayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_monday_availability_from_time))
                flag = false
            }
            mondaySB.isChecked && txtMondayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_monday_availability_to_time))
                flag = false
            }


            tusedaySB.isChecked && txtTusedayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_tuseday_availability_from_time))
                flag = false
            }
            tusedaySB.isChecked && txtTusedayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_tuseday_availability_to_time))
                flag = false
            }


            wednesdaySB.isChecked && txtWednesdayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_wednesday_availability_from_time))
                flag = false
            }
            wednesdaySB.isChecked && txtWednesdayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_wednesday_availability_to_time))
                flag = false
            }

            thursdaySB.isChecked && txtThursdayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_thursday_availability_from_time))
                flag = false
            }
            thursdaySB.isChecked && txtThursdayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_thursday_availability_to_time))
                flag = false
            }


            fridaySB.isChecked && txtFridayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_friday_availability_from_time))
                flag = false
            }
            fridaySB.isChecked && txtFridayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_friday_availability_to_time))
                flag = false
            }


            saturdaySB.isChecked && txtSaturdayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_saturday_availability_from_time))
                flag = false
            }
            saturdaySB.isChecked && txtSaturdayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_saturday_availability_to_time))
                flag = false
            }

            sundaySB.isChecked && txtSundayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_sunday_availability_from_time))
                flag = false
            }
            sundaySB.isChecked && txtSundayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_sunday_availability_to_time))
                flag = false
            }


        }
        return flag
    }




    private fun setUpValidations() {
        acceptUrgentRequestSB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                txtacceptUrgentRequestTV.visibility = View.GONE
                mAcceptUrgentRequest = "1"
            } else {
                mAcceptUrgentRequest = "0"
                txtacceptUrgentRequestTV.visibility = View.VISIBLE
            }
        }

        alwaysAvailableSB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                mAlwaysAvailable = "1"
                mondaySB.visibility = View.GONE
                tusedaySB.visibility = View.GONE
                wednesdaySB.visibility = View.GONE
                thursdaySB.visibility = View.GONE
                fridaySB.visibility = View.GONE
                saturdaySB.visibility = View.GONE
                sundaySB.visibility = View.GONE
                mondayToFromLL.visibility = View.GONE
                tusedayToFromLL.visibility = View.GONE
                wednesdayToFromLL.visibility = View.GONE
                thursdayToFromLL.visibility = View.GONE
                fridayToFromLL.visibility = View.GONE
                saturdayToFromLL.visibility = View.GONE
                sundayToFromLL.visibility = View.GONE
            } else {
                mAlwaysAvailable = "0"
                mondaySB.visibility = View.VISIBLE
                tusedaySB.visibility = View.VISIBLE
                wednesdaySB.visibility = View.VISIBLE
                thursdaySB.visibility = View.VISIBLE
                fridaySB.visibility = View.VISIBLE
                saturdaySB.visibility = View.VISIBLE
                sundaySB.visibility = View.VISIBLE
                mondayToFromLL.visibility = View.GONE
                tusedayToFromLL.visibility = View.GONE
                wednesdayToFromLL.visibility = View.GONE
                thursdayToFromLL.visibility = View.GONE
                fridayToFromLL.visibility = View.GONE
                saturdayToFromLL.visibility = View.GONE
                sundayToFromLL.visibility = View.GONE
            }
        }

        mondaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                mondayChecked = "0"
                mondayToFromLL.visibility = View.GONE
            }else{
                mondayChecked = "1"
                mondayToFromLL.visibility = View.VISIBLE
            }
        }

        tusedaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                tuesdayChecked = "0"
                tusedayToFromLL.visibility = View.GONE
            }else{
                tuesdayChecked = "1"
                tusedayToFromLL.visibility = View.VISIBLE
            }
        }

        wednesdaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                wednesdayChecked = "0"
                wednesdayToFromLL.visibility = View.GONE
            }else{
                wednesdayChecked = "1"
                wednesdayToFromLL.visibility = View.VISIBLE
            }
        }

        thursdaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                thirsdayChecked = "0"
                thursdayToFromLL.visibility = View.GONE
            }else{
                thirsdayChecked = "1"
                thursdayToFromLL.visibility = View.VISIBLE
            }
        }

        fridaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                fridayChecked = "0"
                fridayToFromLL.visibility = View.GONE
            }else{
                fridayChecked = "1"
                fridayToFromLL.visibility = View.VISIBLE
            }
        }

        saturdaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                saturdayChecked = "0"
                saturdayToFromLL.visibility = View.GONE
            }else{
                saturdayChecked = "1"
                saturdayToFromLL.visibility = View.VISIBLE
            }
        }

        sundaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                sundayChecked = "0"
                sundayToFromLL.visibility = View.GONE
            }else{
                sundayChecked = "1"
                sundayToFromLL.visibility = View.VISIBLE
            }
        }

    }


}