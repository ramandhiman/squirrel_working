package com.squirrel.app.activities.parent

import android.app.Activity
import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import com.squirrel.app.adapters.KidAvtarAdapter
import com.squirrel.app.interfaces.AvtarClickListner
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_add_kid.*
import kotlinx.android.synthetic.main.item_kid1.*
import kotlinx.android.synthetic.main.item_kid2.*
import kotlinx.android.synthetic.main.item_kid3.*
import kotlinx.android.synthetic.main.item_kid4.*
import kotlinx.android.synthetic.main.item_kid5.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*


class AddKidActivity : AppCompatActivity(){
    var TAG = this@AddKidActivity.javaClass.simpleName
    var mActivity: Activity = this@AddKidActivity
    var mChildPosition = 0
    var mGenderType1: String = ""
    var mGenderType2: String = ""
    var mGenderType3: String = ""
    var mGenderType4: String = ""
    var mGenderType5: String = ""

    var mAvtar1: String = ""
    var mAvtar2: String = ""
    var mAvtar3: String = ""
    var mAvtar4: String = ""
    var mAvtar5: String = ""

    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_kid)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setChildAvtar1()
        setChildAvtar2()
        setChildAvtar3()
        setChildAvtar4()
        setChildAvtar5()
    }


    var mAvtarClickListner : AvtarClickListner = object  : AvtarClickListner{
        override fun onAdapterPosition(pos: Int, childCount: Int) {
            if (childCount == 1){
                mAvtar1 = ""+pos
            }
            if (childCount == 2){
                mAvtar2 = ""+pos
            }
            if (childCount == 3){
                mAvtar3 = ""+pos
            }
            if (childCount == 4){
                mAvtar4 = ""+pos
            }
            if (childCount == 5){
                mAvtar5 = ""+pos
            }
        }
    }

    private fun setChildAvtar1() {
        //Add Avtar Images
        var mAdapter : KidAvtarAdapter = KidAvtarAdapter(mActivity, AVTAR_ARRAY,mAvtarClickListner,1)
        avatarPickRV1.layoutManager = LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false)
        avatarPickRV1.setHasFixedSize(true)
        avatarPickRV1.adapter = mAdapter
    }

    private fun setChildAvtar2() {
        //Add Avtar Images
        var mAdapter : KidAvtarAdapter = KidAvtarAdapter(mActivity, AVTAR_ARRAY,mAvtarClickListner,2)
        avatarPickRV2.layoutManager = LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false)
        avatarPickRV2.setHasFixedSize(true)
        avatarPickRV2.adapter = mAdapter
    }

    private fun setChildAvtar3() {
        //Add Avtar Images
        var mAdapter : KidAvtarAdapter = KidAvtarAdapter(mActivity, AVTAR_ARRAY,mAvtarClickListner,3)
        avatarPickRV3.layoutManager = LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false)
        avatarPickRV3.setHasFixedSize(true)
        avatarPickRV3.adapter = mAdapter
    }

    private fun setChildAvtar4() {
        //Add Avtar Images
        var mAdapter : KidAvtarAdapter = KidAvtarAdapter(mActivity, AVTAR_ARRAY,mAvtarClickListner,4)
        avatarPickRV4.layoutManager = LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false)
        avatarPickRV4.setHasFixedSize(true)
        avatarPickRV4.adapter = mAdapter
    }

    private fun setChildAvtar5() {
        //Add Avtar Images
        var mAdapter : KidAvtarAdapter = KidAvtarAdapter(mActivity, AVTAR_ARRAY,mAvtarClickListner,5)
        avatarPickRV5.layoutManager = LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false)
        avatarPickRV5.setHasFixedSize(true)
        avatarPickRV5.adapter = mAdapter
    }


    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.setup_profile))
        val mTitleText = "<font color=#0b0f35>Add</font> <font color=#f16c21>your kids</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.txtAddKidTV,
        R.id.txtDeleteTV1,
        R.id.txtDeleteTV2,
        R.id.txtDeleteTV3,
        R.id.txtDeleteTV4,
        R.id.txtDeleteTV5,
        R.id.txtDateOfBirthTV1,
        R.id.txtDateOfBirthTV2,
        R.id.txtDateOfBirthTV3,
        R.id.txtDateOfBirthTV4,
        R.id.txtDateOfBirthTV5,
        R.id.txtMaleTV1,
        R.id.txtMaleTV2,
        R.id.txtMaleTV3,
        R.id.txtMaleTV4,
        R.id.txtMaleTV5,
        R.id.txtFemaleTV1,
        R.id.txtFemaleTV2,
        R.id.txtFemaleTV3,
        R.id.txtFemaleTV4,
        R.id.txtFemaleTV5,
        R.id.txtOthersTV1,
        R.id.txtOthersTV2,
        R.id.txtOthersTV3,
        R.id.txtOthersTV4,
        R.id.txtOthersTV5,
        R.id.btnNextB)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.txtAddKidTV -> performAddKidClick()
            R.id.txtDeleteTV1 -> performDelete1Click()
            R.id.txtDeleteTV2 -> performDelete2Click()
            R.id.txtDeleteTV3 -> performDelete3Click()
            R.id.txtDeleteTV4 -> performDelete4Click()
            R.id.txtDeleteTV5 -> performDelete5Click()
            R.id.txtDateOfBirthTV1 -> showDatePicker(txtDateOfBirthTV1!!)
            R.id.txtDateOfBirthTV2 -> showDatePicker(txtDateOfBirthTV2!!)
            R.id.txtDateOfBirthTV3 -> showDatePicker(txtDateOfBirthTV3!!)
            R.id.txtDateOfBirthTV4 -> showDatePicker(txtDateOfBirthTV4!!)
            R.id.txtDateOfBirthTV5 -> showDatePicker(txtDateOfBirthTV5!!)
            R.id.txtMaleTV1->tabSelection(0,txtMaleTV1!!,txtFemaleTV1!!,txtOthersTV1!!,editSpecificET1!!)
            R.id.txtMaleTV2->tabSelection(0,txtMaleTV2!!,txtFemaleTV2!!,txtOthersTV2!!,editSpecificET2!!)
            R.id.txtMaleTV3->tabSelection(0,txtMaleTV3!!,txtFemaleTV3!!,txtOthersTV3!!,editSpecificET3!!)
            R.id.txtMaleTV4->tabSelection(0,txtMaleTV4!!,txtFemaleTV4!!,txtOthersTV4!!,editSpecificET4!!)
            R.id.txtMaleTV5 ->tabSelection(0,txtMaleTV5!!,txtFemaleTV5!!,txtOthersTV5!!,editSpecificET5!!)
            R.id.txtFemaleTV1->tabSelection(1,txtMaleTV1!!,txtFemaleTV1!!,txtOthersTV1!!,editSpecificET1!!)
            R.id.txtFemaleTV2->tabSelection(1,txtMaleTV2!!,txtFemaleTV2!!,txtOthersTV2!!,editSpecificET2!!)
            R.id.txtFemaleTV3->tabSelection(1,txtMaleTV3!!,txtFemaleTV3!!,txtOthersTV3!!,editSpecificET3!!)
            R.id.txtFemaleTV4->tabSelection(1,txtMaleTV4!!,txtFemaleTV4!!,txtOthersTV4!!,editSpecificET4!!)
            R.id.txtFemaleTV5 ->tabSelection(1,txtMaleTV5!!,txtFemaleTV5!!,txtOthersTV5!!,editSpecificET5!!)
            R.id.txtOthersTV1->tabSelection(2,txtMaleTV1!!,txtFemaleTV1!!,txtOthersTV1!!,editSpecificET1!!)
            R.id.txtOthersTV2->tabSelection(2,txtMaleTV2!!,txtFemaleTV2!!,txtOthersTV2!!,editSpecificET2!!)
            R.id.txtOthersTV3->tabSelection(2,txtMaleTV3!!,txtFemaleTV3!!,txtOthersTV3!!,editSpecificET3!!)
            R.id.txtOthersTV4->tabSelection(2,txtMaleTV4!!,txtFemaleTV4!!,txtOthersTV4!!,editSpecificET4!!)
            R.id.txtOthersTV5 ->tabSelection(2,txtMaleTV5!!,txtFemaleTV5!!,txtOthersTV5!!,editSpecificET5!!)
            R.id.btnNextB -> performNextClick()
        }
    }

    private fun performDelete1Click() {
        mChildPosition = mChildPosition - 1
        ll1.visibility = View.GONE
        txtAddKidTV.visibility = View.VISIBLE
    }

    private fun performDelete2Click() {
        mChildPosition = mChildPosition - 1
        ll2.visibility = View.GONE
        txtAddKidTV.visibility = View.VISIBLE
    }

    private fun performDelete3Click() {
        mChildPosition = mChildPosition - 1
        ll3.visibility = View.GONE
        txtAddKidTV.visibility = View.VISIBLE
    }

    private fun performDelete4Click() {
        mChildPosition = mChildPosition - 1
        ll4.visibility = View.GONE
        txtAddKidTV.visibility = View.VISIBLE
    }

    private fun performDelete5Click() {
        mChildPosition = mChildPosition - 1
        ll5.visibility = View.GONE
        txtAddKidTV.visibility = View.VISIBLE
    }

    private fun performAddKidClick() {
            mChildPosition = mChildPosition + 1
            if (mChildPosition == 1){
                ll1.visibility = View.VISIBLE
            }
            if (mChildPosition == 2){
                ll2.visibility = View.VISIBLE
            }
            if (mChildPosition == 3){
                ll3.visibility = View.VISIBLE
            }
            if (mChildPosition == 4){
                ll4.visibility = View.VISIBLE
            }
            if (mChildPosition == 5){
                ll5.visibility = View.VISIBLE
            }

        if (ll1.visibility == View.VISIBLE &&ll2.visibility == View.VISIBLE &&ll3.visibility == View.VISIBLE &&ll4.visibility == View.VISIBLE &&ll5.visibility == View.VISIBLE ){
            txtAddKidTV.visibility = View.GONE
        }else{
            txtAddKidTV.visibility = View.VISIBLE
        }
    }


    // - - To Change the color of Icon and Text When Tab is selected
    private fun tabSelection(
        mPos: Int,
        txtMaleTV: TextView,
        txtFemaleTV: TextView,
        txtOthersTV: TextView,
        editSpecificET: EditText
    ) {
        val mTypeface = Typeface.createFromAsset(assets, "poppins_regular.ttf")
        val mTypefaceBold = Typeface.createFromAsset(assets, "poppins_bold.ttf")

        txtMaleTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtMaleTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtMaleTV.setTypeface(mTypeface)

        txtFemaleTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtFemaleTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtFemaleTV.setTypeface(mTypeface)

        txtOthersTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtOthersTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtOthersTV.setTypeface(mTypeface)

        when (mPos) {
            0 -> {
                mGenderType1 = "0"
                mGenderType2 = "0"
                mGenderType3 = "0"
                mGenderType4 = "0"
                mGenderType5 = "0"
                editSpecificET.visibility = View.GONE
                txtMaleTV.getBackground().setColorFilter(
                    getResources().getColor(R.color.box_color_highlight),
                    PorterDuff.Mode.SRC_ATOP
                )
                txtMaleTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtMaleTV.setTypeface(mTypefaceBold)
            }
            1 -> {
                mGenderType1 = "1"
                mGenderType2 = "1"
                mGenderType3 = "1"
                mGenderType4 = "1"
                mGenderType5 = "1"
                editSpecificET.visibility = View.GONE
                txtFemaleTV.getBackground().setColorFilter(
                    getResources().getColor(R.color.box_color_highlight),
                    PorterDuff.Mode.SRC_ATOP
                )
                txtFemaleTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtFemaleTV.setTypeface(mTypefaceBold)
            }
            2 -> {
                mGenderType1 = "2"
                mGenderType2 = "2"
                mGenderType3 = "2"
                mGenderType4 = "2"
                mGenderType5 = "2"
                editSpecificET.visibility = View.VISIBLE
                txtOthersTV.getBackground().setColorFilter(
                    getResources().getColor(R.color.box_color_highlight),
                    PorterDuff.Mode.SRC_ATOP
                )
                txtOthersTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtOthersTV.setTypeface(mTypefaceBold)
            }
        }
    }




    private fun performNextClick() {
        if (ll1.visibility == View.VISIBLE || ll2.visibility == View.VISIBLE || ll3.visibility == View.VISIBLE || ll4.visibility == View.VISIBLE || ll5.visibility == View.VISIBLE ) {
                if (ll1.visibility == View.VISIBLE && editNameET1?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_your_kid_name))
                } else if (ll1.visibility == View.VISIBLE && txtDateOfBirthTV1?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_kid_date_of_birth))
                } else if (ll1.visibility == View.VISIBLE && mGenderType1.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_your_gender))
                } else if (ll1.visibility == View.VISIBLE && mAvtar1.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_kid_avatar))
                } else if (ll1.visibility == View.VISIBLE && editDecriptionET1?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_child_description))
                }

                else if (ll2.visibility == View.VISIBLE && editNameET2?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_your_kid_name))
                } else if (ll2.visibility == View.VISIBLE && txtDateOfBirthTV2?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_kid_date_of_birth))
                } else if (ll2.visibility == View.VISIBLE && mGenderType2.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_your_gender))
                } else if (ll2.visibility == View.VISIBLE && mAvtar2.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_kid_avatar))
                } else if (ll2.visibility == View.VISIBLE && editDecriptionET2?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_child_description))
                }
                else if (ll3.visibility == View.VISIBLE && editNameET3?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_your_kid_name))
                } else if (ll3.visibility == View.VISIBLE && txtDateOfBirthTV3?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_kid_date_of_birth))
                } else if (ll3.visibility == View.VISIBLE && mGenderType3.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_your_gender))
                } else if (ll3.visibility == View.VISIBLE && mAvtar3.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_kid_avatar))
                } else if (ll3.visibility == View.VISIBLE && editDecriptionET3?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_child_description))
                }
                else if (ll4.visibility == View.VISIBLE && editNameET4?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_your_kid_name))
                } else if (ll4.visibility == View.VISIBLE && txtDateOfBirthTV4?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_kid_date_of_birth))
                } else if (ll4.visibility == View.VISIBLE && mGenderType4.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_your_gender))
                } else if (ll4.visibility == View.VISIBLE && mAvtar4.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_kid_avatar))
                } else if (ll4.visibility == View.VISIBLE && editDecriptionET4?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_child_description))
                }
                else if (ll5.visibility == View.VISIBLE && editNameET5?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_your_kid_name))
                } else if (ll5.visibility == View.VISIBLE && txtDateOfBirthTV5?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_kid_date_of_birth))
                } else if (ll5.visibility == View.VISIBLE && mGenderType5.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_your_gender))
                } else if (ll5.visibility == View.VISIBLE && mAvtar5.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_select_kid_avatar))
                } else if (ll5.visibility == View.VISIBLE && editDecriptionET5?.text.toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_child_description))
                }else{
                    executeSaveChildsData()
                }
            }else{
                //Submit Child Details
              showAlertDialog(mActivity, getString(R.string.please_add_your_child))
            }
    }

    private fun executeSaveChildsData() {
        showProgressDialog(mActivity)
        var mKidsArray : ArrayList<String> = ArrayList()
        if (ll1.visibility == View.VISIBLE && ll2.visibility == View.VISIBLE && ll3.visibility == View.VISIBLE && ll4.visibility == View.VISIBLE && ll5.visibility == View.VISIBLE) {
            var mFirebaseID1 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild1 = hashMapOf(
                "avatar" to mAvtar1,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV1.text.toString().trim()),
                "firebaseID" to mFirebaseID1,
                "gender" to mGenderType1,
                "genderText" to editSpecificET1.text.toString().trim(),
                "hobbies" to editDecriptionET1.text.toString().trim(),
                "name" to editNameET1.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID1!!)
                ?.set(mChild1)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }


            var mFirebaseID2 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild2 = hashMapOf(
                "avatar" to mAvtar2,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV2.text.toString().trim()),
                "firebaseID" to mFirebaseID2,
                "gender" to mGenderType2,
                "genderText" to editSpecificET2.text.toString().trim(),
                "hobbies" to editDecriptionET2.text.toString().trim(),
                "name" to editNameET2.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID2!!)
                ?.set(mChild2)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }


            var mFirebaseID3 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild3 = hashMapOf(
                "avatar" to mAvtar3,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV3.text.toString().trim()),
                "firebaseID" to mFirebaseID3,
                "gender" to mGenderType3,
                "genderText" to editSpecificET3.text.toString().trim(),
                "hobbies" to editDecriptionET3.text.toString().trim(),
                "name" to editNameET3.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID3!!)
                ?.set(mChild3)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }

            var mFirebaseID4 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild4 = hashMapOf(
                "avatar" to mAvtar4,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV4.text.toString().trim()),
                "firebaseID" to mFirebaseID4,
                "gender" to mGenderType4,
                "genderText" to editSpecificET4.text.toString().trim(),
                "hobbies" to editDecriptionET4.text.toString().trim(),
                "name" to editNameET4.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID4!!)
                ?.set(mChild4)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }

            var mFirebaseID5 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild5 = hashMapOf(
                "avatar" to mAvtar5,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV5.text.toString().trim()),
                "firebaseID" to mFirebaseID5,
                "gender" to mGenderType5,
                "genderText" to editSpecificET5.text.toString().trim(),
                "hobbies" to editDecriptionET5.text.toString().trim(),
                "name" to editNameET5.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID5!!)
                ?.set(mChild5)
                ?.addOnSuccessListener {

                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }

            val mKidsArrayList: ArrayList<String> = ArrayList()
            mKidsArrayList.add(mFirebaseID1!!)
            mKidsArrayList.add(mFirebaseID2!!)
            mKidsArrayList.add(mFirebaseID3!!)
            mKidsArrayList.add(mFirebaseID4!!)
            mKidsArrayList.add(mFirebaseID5!!)

            val mParentUpdate = hashMapOf("kidsList" to mKidsArrayList)
            mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
                ?.update(mParentUpdate as Map<String, Any>)
                ?.addOnSuccessListener {
                    dismissProgressDialog()
                    showToast(mActivity,getString(R.string.childadded_sccuessfully))
                    val intent=Intent(mActivity,EnableLocationActivity::class.java)
                    startActivity(intent)
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }

        }else if (ll1.visibility == View.VISIBLE && ll2.visibility == View.VISIBLE && ll3.visibility == View.VISIBLE && ll4.visibility == View.VISIBLE){
            var mFirebaseID1 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild1 = hashMapOf(
                "avatar" to mAvtar1,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV1.text.toString().trim()),
                "firebaseID" to mFirebaseID1,
                "gender" to mGenderType1,
                "genderText" to editSpecificET1.text.toString().trim(),
                "hobbies" to editDecriptionET1.text.toString().trim(),
                "name" to editNameET1.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID1!!)
                ?.set(mChild1)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }


            var mFirebaseID2 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild2 = hashMapOf(
                "avatar" to mAvtar2,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV2.text.toString().trim()),
                "firebaseID" to mFirebaseID2,
                "gender" to mGenderType2,
                "genderText" to editSpecificET2.text.toString().trim(),
                "hobbies" to editDecriptionET2.text.toString().trim(),
                "name" to editNameET2.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID2!!)
                ?.set(mChild2)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }


            var mFirebaseID3 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild3 = hashMapOf(
                "avatar" to mAvtar3,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV3.text.toString().trim()),
                "firebaseID" to mFirebaseID3,
                "gender" to mGenderType3,
                "genderText" to editSpecificET3.text.toString().trim(),
                "hobbies" to editDecriptionET3.text.toString().trim(),
                "name" to editNameET3.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID3!!)
                ?.set(mChild3)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }

            var mFirebaseID4 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild4 = hashMapOf(
                "avatar" to mAvtar4,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV4.text.toString().trim()),
                "firebaseID" to mFirebaseID4,
                "gender" to mGenderType4,
                "genderText" to editSpecificET4.text.toString().trim(),
                "hobbies" to editDecriptionET4.text.toString().trim(),
                "name" to editNameET4.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID4!!)
                ?.set(mChild4)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }

            val mKidsArrayList: ArrayList<String> = ArrayList()
            mKidsArrayList.add(mFirebaseID1!!)
            mKidsArrayList.add(mFirebaseID2!!)
            mKidsArrayList.add(mFirebaseID3!!)
            mKidsArrayList.add(mFirebaseID4!!)
            val mParentUpdate = hashMapOf("kidsList" to mKidsArrayList)
            mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
                ?.update(mParentUpdate as Map<String, Any>)
                ?.addOnSuccessListener {
                    dismissProgressDialog()
                    showToast(mActivity,getString(R.string.childadded_sccuessfully))
                    val intent=Intent(mActivity,EnableLocationActivity::class.java)
                    startActivity(intent)
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }

        } else if (ll1.visibility == View.VISIBLE && ll2.visibility == View.VISIBLE && ll3.visibility == View.VISIBLE){
            var mFirebaseID1 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild1 = hashMapOf(
                "avatar" to mAvtar1,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV1.text.toString().trim()),
                "firebaseID" to mFirebaseID1,
                "gender" to mGenderType1,
                "genderText" to editSpecificET1.text.toString().trim(),
                "hobbies" to editDecriptionET1.text.toString().trim(),
                "name" to editNameET1.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID1!!)
                ?.set(mChild1)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }


            var mFirebaseID2 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild2 = hashMapOf(
                "avatar" to mAvtar2,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV2.text.toString().trim()),
                "firebaseID" to mFirebaseID2,
                "gender" to mGenderType2,
                "genderText" to editSpecificET2.text.toString().trim(),
                "hobbies" to editDecriptionET2.text.toString().trim(),
                "name" to editNameET2.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID2!!)
                ?.set(mChild2)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }


            var mFirebaseID3 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild3 = hashMapOf(
                "avatar" to mAvtar3,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV3.text.toString().trim()),
                "firebaseID" to mFirebaseID3,
                "gender" to mGenderType3,
                "genderText" to editSpecificET3.text.toString().trim(),
                "hobbies" to editDecriptionET3.text.toString().trim(),
                "name" to editNameET3.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID3!!)
                ?.set(mChild3)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }


            val mKidsArrayList: ArrayList<String> = ArrayList()
            mKidsArrayList.add(mFirebaseID1!!)
            mKidsArrayList.add(mFirebaseID2!!)
            mKidsArrayList.add(mFirebaseID3!!)
            val mParentUpdate = hashMapOf("kidsList" to mKidsArrayList)
            mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
                ?.update(mParentUpdate as Map<String, Any>)
                ?.addOnSuccessListener {
                    dismissProgressDialog()
                    showToast(mActivity,getString(R.string.childadded_sccuessfully))
                    val intent=Intent(mActivity,EnableLocationActivity::class.java)
                    startActivity(intent)
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }


        }else if (ll1.visibility == View.VISIBLE && ll2.visibility == View.VISIBLE){
            var mFirebaseID1 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild1 = hashMapOf(
                "avatar" to mAvtar1,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV1.text.toString().trim()),
                "firebaseID" to mFirebaseID1,
                "gender" to mGenderType1,
                "genderText" to editSpecificET1.text.toString().trim(),
                "hobbies" to editDecriptionET1.text.toString().trim(),
                "name" to editNameET1.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID1!!)
                ?.set(mChild1)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }


            var mFirebaseID2 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild2 = hashMapOf(
                "avatar" to mAvtar2,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV2.text.toString().trim()),
                "firebaseID" to mFirebaseID2,
                "gender" to mGenderType2,
                "genderText" to editSpecificET2.text.toString().trim(),
                "hobbies" to editDecriptionET2.text.toString().trim(),
                "name" to editNameET2.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID2!!)
                ?.set(mChild2)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }
            val mKidsArrayList: ArrayList<String> = ArrayList()
            mKidsArrayList.add(mFirebaseID1!!)
            mKidsArrayList.add(mFirebaseID2!!)
            val mParentUpdate = hashMapOf("kidsList" to mKidsArrayList)
            mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
                ?.update(mParentUpdate as Map<String, Any>)
                ?.addOnSuccessListener {
                    dismissProgressDialog()
                    showToast(mActivity,getString(R.string.childadded_sccuessfully))
                    val intent=Intent(mActivity,EnableLocationActivity::class.java)
                    startActivity(intent)
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }

        }else{
            var mFirebaseID1 = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
            val mChild1 = hashMapOf(
                "avatar" to mAvtar1,
                "dob" to convertDobToDbFormat(txtDateOfBirthTV1.text.toString().trim()),
                "firebaseID" to mFirebaseID1,
                "gender" to mGenderType1,
                "genderText" to editSpecificET1.text.toString().trim(),
                "hobbies" to editDecriptionET1.text.toString().trim(),
                "name" to editNameET1.text.toString().trim(),
                "parentID" to getUserID(mActivity),
                "updatedAt" to updatedAt()
            )

            mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID1!!)
                ?.set(mChild1)
                ?.addOnSuccessListener {
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }

            val mKidsArrayList: ArrayList<String> = ArrayList()
            mKidsArrayList.add(mFirebaseID1!!)
            val mParentUpdate = hashMapOf("kidsList" to mKidsArrayList)
            mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
                ?.update(mParentUpdate as Map<String, Any>)
                ?.addOnSuccessListener {
                    dismissProgressDialog()
                    showToast(mActivity,getString(R.string.childadded_sccuessfully))
                    val intent=Intent(mActivity,EnableLocationActivity::class.java)
                    startActivity(intent)
                }?.addOnFailureListener { e ->
                    dismissProgressDialog()
                    Log.e(TAG, "Error: ${e.toString()}")
                }
        }

    }


}