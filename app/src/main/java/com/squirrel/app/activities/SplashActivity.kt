package com.squirrel.app.activities

import android.app.Activity
import android.app.TaskStackBuilder
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Source
import com.google.firebase.ktx.Firebase
import com.squirrel.app.R
import com.squirrel.app.activities.babysitter.*
import com.squirrel.app.activities.parent.AddKidActivity
import com.squirrel.app.activities.parent.EnableLocationActivity
import com.squirrel.app.activities.parent.ParentHomeActivity
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.models.ParentModel
import com.squirrel.app.utils.*

class SplashActivity : AppCompatActivity() {
    var TAG = this@SplashActivity.javaClass.simpleName
    var mActivity: Activity = this@SplashActivity
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpSplash()
    }


    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT.toLong())

                if (isLogin(mActivity)){
                    if (isNetworkAvailable(mActivity)){
                        getUserDetailsFromDB()
                    }else{
                        showToast(mActivity,getString(R.string.internet_connection_error))
                    }

                }else{
                    val i = Intent(mActivity, SelectionActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }
        }
        mThread.start()
    }


    /*
    * SetUp Splash With Both
    * Parent & Babysitter Validations
    * */
    private fun getUserDetailsFromDB() {
        val mDocumentRefenceParent = mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
        val mDocumentRefenceBabySitter = mFireStoreDB?.collection(BABYSITTERS_COLLECTION)?.document(getUserID(mActivity))!!.get(Source.SERVER)

        mDocumentRefenceParent?.get()?.addOnSuccessListener { documentSnapshot ->
            Log.e(TAG,"***UserID***"+getUserID(mActivity))
            //Parent Model Role
            if (documentSnapshot.exists()){
                val mParentModel=documentSnapshot.toObject(ParentModel::class.java)
                if (mParentModel?.kidsList!!.size > 0 && mParentModel?.lattitude?.length!! > 0){
                    AppPrefrences().writeString(mActivity, USER_ROLE, mParentModel.role)
                    AppPrefrences().writeString(mActivity, NAME, mParentModel.name)
                    AppPrefrences().writeString(mActivity, PROFILEPIC, mParentModel.imgsource)
                    AppPrefrences().writeString(mActivity, USER_GENDER, mParentModel.gender)
                    AppPrefrences().writeString(mActivity, USER_GENDER_SPECIFY, mParentModel.genderText)
                    AppPrefrences().writeString(mActivity, USER_LATITUDE, mParentModel.lattitude)
                    AppPrefrences().writeString(mActivity, USER_LONGITUDE, mParentModel.longitude)
                    val i = Intent(mActivity, ParentHomeActivity::class.java)
                    startActivity(i)
                    finish()
                }else if (mParentModel?.kidsList!!.size > 0  && mParentModel?.lattitude?.length!! == 0){
                    val i = Intent(mActivity, EnableLocationActivity::class.java)
                    startActivity(i)
                    finish()
                }else if (mParentModel?.kidsList!!.size == 0){
                    val i = Intent(mActivity, AddKidActivity::class.java)
                    startActivity(i)
                    finish()
                }else{
                    val i = Intent(mActivity, SelectRoleActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }else{
              //Babysitter Model Role
                mDocumentRefenceBabySitter?.addOnSuccessListener { babyDocumentSnapshot ->
                    if (babyDocumentSnapshot.exists()){

                        val mBabysitterModel : BabysitterModel? = babyDocumentSnapshot.toObject(BabysitterModel::class.java)

                        if (mBabysitterModel?.gender != null && mBabysitterModel?.gender!!.isEmpty()){
                            val intent=Intent(mActivity, WelcomeTeamActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else if (mBabysitterModel?.days != null && mBabysitterModel?.days!!.size == 0){
                            val intent=Intent(mActivity, YourAvalablityActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else if (mBabysitterModel?.lattitude != null && mBabysitterModel?.lattitude!!.isEmpty()){
                            val intent=Intent(mActivity, EnableLocationBabySitterActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else if (mBabysitterModel?.isAdminApproved != null && mBabysitterModel?.isAdminApproved.equals("0")) {
                            val intent = Intent(mActivity, ProfilePendingActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else if (mBabysitterModel?.isAdminApproved != null && mBabysitterModel?.isAdminApproved.equals("2")) {
                            val preferences: SharedPreferences = mActivity?.let { AppPrefrences().getPreferences(it) }!!
                            val editor = preferences.edit()
                            editor.clear()
                            editor.apply()
                            Firebase.auth.signOut()
                            val mIntent = Intent(mActivity, SelectionActivity::class.java)
                            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent).startActivities()
                            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
                            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            mActivity?.startActivity(mIntent)
                            mActivity?.finish()
                            val intent = Intent(mActivity, SelectionActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else if(mBabysitterModel?.isAdminApproved != null && mBabysitterModel?.isAdminApproved.equals("1")){
                            AppPrefrences().writeString(mActivity, USER_ROLE, mBabysitterModel.role)
                            AppPrefrences().writeString(mActivity, USER_DOB, convertDBToDobFormat(mBabysitterModel.dob))
                            AppPrefrences().writeString(mActivity, NAME, mBabysitterModel.name)
                            AppPrefrences().writeString(mActivity, PROFILEPIC, mBabysitterModel.imgsource)
                            AppPrefrences().writeString(mActivity, USER_IC_EPIC, mBabysitterModel.imageId)
                            AppPrefrences().writeString(mActivity, USER_GENDER, mBabysitterModel.gender)
                            AppPrefrences().writeString(mActivity, USER_GENDER_SPECIFY, mBabysitterModel.genderText)
                            AppPrefrences().writeString(mActivity, USER_LATITUDE, mBabysitterModel.lattitude)
                            AppPrefrences().writeString(mActivity, USER_LONGITUDE, mBabysitterModel.longitude)

                            val i = Intent(mActivity, BabySItterNewHomeActivity::class.java)
                            startActivity(i)
                            finish()
                        }else {
                            val i = Intent(mActivity, ProfilePendingActivity::class.java)
                            startActivity(i)
                            finish()
                        }
                    }else{
                        val i = Intent(mActivity, SelectRoleActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                }
            }
        }
    }


}







