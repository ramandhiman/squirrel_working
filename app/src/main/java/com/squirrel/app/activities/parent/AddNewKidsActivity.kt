package com.squirrel.app.activities.parent

import android.app.Activity
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import com.squirrel.app.adapters.parentsAdapter.AddKidAvtarAdapter
import com.squirrel.app.interfaces.AddKidAvtarClickListner
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_add_kids.*
import kotlinx.android.synthetic.main.layout_my_kids_toolbar.*


class AddNewKidsActivity : AppCompatActivity(){
    var TAG = this@AddNewKidsActivity.javaClass.simpleName
    var mActivity: Activity = this@AddNewKidsActivity
    var mGenderType: String = ""
    var mAvatarPosition = -1
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_kids)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setupKidsAvatarRV()
    }


    private fun setUpToolBar() {
        txtHeadingKidsTV.setText(getString(R.string.add))
    }

    @OnClick(
        R.id.imgBackKidsIV,
        R.id.txtDateOfBirthKidTV,
        R.id.txtMaleKidTV,
        R.id.txtFemaleKidTV,
        R.id.txtOthersKidTV,
        R.id.btnSave
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackKidsIV -> onBackPressed()
            R.id.txtDateOfBirthKidTV -> showDatePicker(txtDateOfBirthKidTV)
            R.id.txtMaleKidTV ->  tabSelection(0, txtMaleKidTV, txtFemaleKidTV, txtOthersKidTV, editSpecificTV)
            R.id.txtFemaleKidTV ->  tabSelection(1, txtMaleKidTV, txtFemaleKidTV, txtOthersKidTV, editSpecificTV)
            R.id.txtOthersKidTV ->  tabSelection(2, txtMaleKidTV, txtFemaleKidTV, txtOthersKidTV, editSpecificTV)
            R.id.btnSave -> performSaveClick()
        }
    }



    private fun performSaveClick() {
        if(isValidate()) {
            if (isNetworkAvailable(mActivity)){
                executeAddKidData()
            }else{
                showToast(mActivity,getString(R.string.internet_connection_error))
            }
        }
    }

    private fun executeAddKidData() {
        //Insert data in DB
        var mFirebaseID = mFireStoreDB?.collection(KIDS_COLLECTION)?.document()?.id
        val mKidData = hashMapOf(
            "avatar" to ""+mAvatarPosition,
            "dob" to convertDobToDbFormat(txtDateOfBirthKidTV.text.toString().trim()),
            "firebaseID" to mFirebaseID,
            "gender" to mGenderType,
            "genderText" to editSpecificTV.text.toString().trim(),
            "hobbies" to editDecriptionET.text.toString().trim(),
            "name" to editNameET.text.toString().trim(),
            "parentID" to getUserID(mActivity),
            "updatedAt" to updatedAt())
        showProgressDialog(mActivity)

        mFireStoreDB?.collection(KIDS_COLLECTION)?.document(mFirebaseID!!)
            ?.set(mKidData)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))!!.update("kidsList", FieldValue.arrayUnion(mFirebaseID))
                showToast(mActivity,getString(R.string.kid_added_successfully))
                finish()
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                Log.e(TAG, "Error: ${e.toString()}")
                showAlertDialog(mActivity,e.localizedMessage)
            }

    }


    fun setupKidsAvatarRV() {
        //Add Avtar Images
        var mAdapter : AddKidAvtarAdapter = AddKidAvtarAdapter(mActivity, AVTAR_ARRAY,mAddKidAvtarClickListner)
        avatarPickKidsRV.layoutManager = LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false)
        avatarPickKidsRV.setHasFixedSize(true)
        avatarPickKidsRV.adapter = mAdapter
    }

    var mAddKidAvtarClickListner : AddKidAvtarClickListner = object  : AddKidAvtarClickListner {
        override fun onAdapterPosition(pos: Int) {
            mAvatarPosition = pos
        }
    }



    // - - To Change the color of Icon and Text When Tab is selected
    private fun tabSelection(mPos: Int, txtMaleTV: TextView, txtFemaleTV: TextView, txtOthersTV: TextView, txtSpecificTV: EditText) {
        val mTypeface = Typeface.createFromAsset(assets, "poppins_regular.ttf")
        val mTypefaceBold = Typeface.createFromAsset(assets, "poppins_bold.ttf")
        txtMaleTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtMaleTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtMaleTV.setTypeface(mTypeface)
        txtFemaleTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtFemaleTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtFemaleTV.setTypeface(mTypeface)
        txtOthersTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtOthersTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtOthersTV.setTypeface(mTypeface)
        when (mPos) {
            0 -> {
                mGenderType="0"
                txtSpecificTV.visibility = View.GONE
                txtMaleTV.getBackground().setColorFilter(getResources().getColor(R.color.box_color_highlight), PorterDuff.Mode.SRC_ATOP)
                txtMaleTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtMaleTV.setTypeface(mTypefaceBold)
            }
            1 -> {
                mGenderType="1"
                txtSpecificTV.visibility = View.GONE
                txtFemaleTV.getBackground().setColorFilter(getResources().getColor(R.color.box_color_highlight), PorterDuff.Mode.SRC_ATOP)
                txtFemaleTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtFemaleTV.setTypeface(mTypefaceBold)
            }
            2 -> {
                mGenderType="2"
                txtSpecificTV.visibility = View.VISIBLE
                txtOthersTV.getBackground().setColorFilter(getResources().getColor(R.color.box_color_highlight), PorterDuff.Mode.SRC_ATOP)
                txtOthersTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtOthersTV.setTypeface(mTypefaceBold)
            }
        }
    }



    fun isValidate(): Boolean {
        var flag = true
        when {
            editNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_kid_name))
                flag = false
            }
            txtDateOfBirthKidTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_kid_date_of_birth))
                flag = false
            }
            mGenderType == ""   -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_gender))
                flag = false
            }
            mGenderType == "2" && editSpecificTV.text.toString().trim{ it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_specify_gender))
                flag = false
            }
            editDecriptionET.text.toString().trim{ it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_provide_the_about_kid))
                flag = false
            }

        }
        return flag
    }


}