package com.squirrel.app.activities.parent

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import androidx.appcompat.app.AppCompatActivity
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.squirrel.app.R
import com.squirrel.app.activities.babysitter.*
import com.squirrel.app.models.ParentModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_kid_parent_profile.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*
import java.io.ByteArrayOutputStream
import java.io.IOException

class KidParentProfileActivity : AppCompatActivity() {
    var TAG = this@KidParentProfileActivity.javaClass.simpleName
    var mActivity: Activity = this@KidParentProfileActivity

    var mBitmap: Bitmap? = null
    var mProfilePicURI: Uri? = null

    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = Manifest.permission.CAMERA

    var mFirebaseStorage: FirebaseStorage = FirebaseStorage.getInstance()
    var mStorageRefrence: StorageReference = mFirebaseStorage.reference
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kid_parent_profile)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        getUserDetailsFromDB()
    }


    /*
   * SetUp Splash With Both
   * Parent & Babysitter Validations
   *
   * */
    private fun getUserDetailsFromDB() {
        val mDocumentRefenceParent =
            mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
        mDocumentRefenceParent?.get()?.addOnSuccessListener { documentSnapshot ->
            //Parent Model Role
            if (documentSnapshot.exists()) {
                val mParentModel = documentSnapshot.toObject(ParentModel::class.java)
                AppPrefrences().writeString(mActivity, USER_ROLE, mParentModel?.role)
                AppPrefrences().writeString(mActivity, NAME, mParentModel?.name)
                AppPrefrences().writeString(mActivity, PROFILEPIC, mParentModel?.imgsource)
                AppPrefrences().writeString(mActivity, USER_GENDER, mParentModel?.gender)
                AppPrefrences().writeString(
                    mActivity,
                    USER_GENDER_SPECIFY,
                    mParentModel?.genderText
                )
                setDataOnWidgets()
            }
        }
    }


    private fun setDataOnWidgets() {
        txtNameTV.text = getName(mActivity)
        txtRelationshipTV.text = getRelationShipString(getUserGender(mActivity))
        Glide.with(mActivity).load(getProfilePic(mActivity)).placeholder(R.drawable.ic_cam)
            .error(R.drawable.ic_cam).into(profileIV)
    }

    private fun setUpToolBar() {
        txtHeadingTV.text = getString(R.string.edit_profile)
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.editNameLL,
        R.id.editRelationLL,
        R.id.profilePicEditLL,
        R.id.btnSaveB
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.editNameLL -> performEditNameClick()
            R.id.editRelationLL -> performRealtionShipClick()
            R.id.profilePicEditLL -> performImageClick()
            R.id.btnSaveB -> performSaveClick()
        }
    }


    // User Gender Launcher
    var relationshipLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == 777) {
                // There are no request codes
                val mIntentData: Intent? = result.data
                //Do Some Operation Here
                txtRelationshipTV.text = mIntentData?.getStringExtra(PREVIOUS_DATA)
                AppPrefrences().writeString(
                    mActivity,
                    USER_GENDER_SPECIFY,
                    mIntentData?.getStringExtra(OTHER_SPECIFY)
                )
            }
        }

    private fun performRealtionShipClick() {
        val intent = Intent(this, ParentEditRelationshipActivity::class.java)
        relationshipLauncher.launch(intent)
    }


    // User Name Launcher
    var nameLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == 666) {
                // There are no request codes
                val mIntentData: Intent? = result.data
                //Do Some Operation Here
                txtNameTV.text = mIntentData?.getStringExtra(PREVIOUS_DATA)
            }
        }

    private fun performEditNameClick() {
        val intent = Intent(this, ParentEditNameActivity::class.java)
        nameLauncher.launch(intent)
    }

    private fun performImageClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }


    // - - To Check permissions
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    // - - To Request permissions
    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    // - - After Request permissions
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    onSelectImageClick()
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
        }
    }

    // - - Start Image Picker Activity
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(480)     //Final image size will be less than 1 MB(Optional)
            .maxResultSize(512, 512)                          //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
    }


    // - - SetUp Image from Camera/Gallery on View
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!
                    mProfilePicURI = uri
                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_cam)
                        .into(profileIV)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun performSaveClick() {
        if (isNetworkAvailable(mActivity)) {
            updateProfileData()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun updateProfileData() {
        AppPrefrences().writeString(mActivity, NAME, txtNameTV.text.toString().trim())
        AppPrefrences().writeString(
            mActivity,
            USER_GENDER,
            getRelationShipNumber(txtRelationshipTV.text.toString().trim())
        )
        if (mProfilePicURI == null) {
            insertDataInDB()
        } else {
            executeUploadProfileProfileRequest()
        }
    }


    /*
    * Upload Image and Getting url of Image
    * */
    private fun executeUploadProfileProfileRequest() {
        showProgressDialog(mActivity)
        val ref =
            mStorageRefrence.child(USER_IMAGES + getUserID(mActivity) + "/" + getUserID(mActivity) + ".jpg".toString())
        val uploadTask = ref.putFile(mProfilePicURI!!)

        val urlTask = uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>>
        { task ->
            if (!task.isSuccessful) {
                dismissProgressDialog()
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation ref.downloadUrl
        })?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                AppPrefrences().writeString(mActivity, PROFILEPIC, downloadUri.toString())
                insertDataInDB()
            } else {
                dismissProgressDialog()
                showAlertDialog(mActivity, getString(R.string.some_thing_went_wrong))
            }
        }.addOnFailureListener {
            dismissProgressDialog()
            showAlertDialog(mActivity, it.message)
        }
    }

    private fun insertDataInDB() {
        //Insert data in DB
        val mParentDetailsModel = hashMapOf(
            "gender" to getUserGender(mActivity),
            "genderText" to getUserGenderSpecify(mActivity),
            "imgsource" to getProfilePic(mActivity),
            "name" to getName(mActivity),
            "updatedAt" to updatedAt(),
            "userid" to getUserID(mActivity)
        )

        mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
            ?.update(mParentDetailsModel)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                mProfilePicURI = null
                showToast(mActivity, getString(R.string.profile_updated_sucessfully))
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                Log.e(TAG, "Error: $e")
                showAlertDialog(mActivity, e.localizedMessage)
            }
    }


}