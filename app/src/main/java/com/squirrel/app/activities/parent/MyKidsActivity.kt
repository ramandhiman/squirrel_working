package com.squirrel.app.activities.parent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.adapters.MyKidsAdapter
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_my_kids.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*


class MyKidsActivity : AppCompatActivity() {
    var TAG = this@MyKidsActivity.javaClass.simpleName
    var mActivity: Activity = this@MyKidsActivity
    var mAdapter: MyKidsAdapter? = null
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mKidsList : ArrayList<MyKidsModel>  = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_kids)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
    }

    private fun setUpToolBar() {
        addKidIV.visibility = View.VISIBLE
        txtHeadingTV.setText(getString(R.string.myKidsTitle))
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.addKidIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.addKidIV -> performAddClick()
        }
    }

    private fun performAddClick() {
        startActivity(Intent(mActivity, AddNewKidsActivity::class.java))
    }


    override fun onResume() {
        super.onResume()
        getAllKidsData()
    }

    private fun getAllKidsData() {
        if (isNetworkAvailable(mActivity)){
            executeAllKidsData()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    fun executeAllKidsData() {
        showProgressDialog(mActivity)
        mFireStoreDB!!.collection(KIDS_COLLECTION).orderBy("updatedAt", Query.Direction.ASCENDING)
            .whereEqualTo("parentID",getUserID(mActivity))
            .get().addOnSuccessListener {documentSnapshot ->
                dismissProgressDialog()
                mKidsList.clear()
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mKidsList.add(document.toObject(MyKidsModel::class.java))
                    }
                    Log.e(TAG,"**KidsList Size**${mKidsList.size}")
                    setKidsAdapter()
                } else {
                    showToast(mActivity, getString(R.string.no_kids))
                }
            }
            .addOnFailureListener { e ->
                dismissProgressDialog()
                showAlertDialog(mActivity,getString(R.string.some_thing_went_wrong))
            }
    }



    private fun setKidsAdapter() {
        mAdapter = MyKidsAdapter(mActivity, mKidsList)
        myKidssRV!!.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        myKidssRV!!.adapter = mAdapter
    }



}