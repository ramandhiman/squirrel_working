package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.LINK_PP
import com.squirrel.app.utils.LINK_SUPPORT
import com.squirrel.app.utils.showLogoutAlertDialog
import com.squirrel.app.utils.statusBarColor

class BabySitterLeftMenuActivity : AppCompatActivity() {
    var TAG = this@BabySitterLeftMenuActivity.javaClass.simpleName
    var mActivity: Activity = this@BabySitterLeftMenuActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_baby_sitter_left_menu)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
    }

    @OnClick(
        R.id.imgCloseIV,
        R.id.babSitterHomeLL,
        R.id.settingsLL,
        R.id.babSitterProfileLL,
        R.id.supportLL,
        R.id.privacyPolicytLL,
        R.id.myAvailabilityLL,
        R.id.txtLogoutTV )


    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgCloseIV -> performHomeClick()
            R.id.babSitterHomeLL -> performHomeClick()
            R.id.settingsLL -> performSettingsClick()
            R.id.babSitterProfileLL -> performProfileClick()
            R.id.supportLL -> performSupportClick()
            R.id.privacyPolicytLL -> performPrivacyPolicyClick()
            R.id.myAvailabilityLL -> perforAvailabilityClick()
            R.id.txtLogoutTV -> perforLogoutClick()
        }
    }

    private fun performHomeClick() {
        startActivity(Intent(mActivity, BabySItterNewHomeActivity::class.java))
        finish()
    }

    private fun perforLogoutClick() {
        showLogoutAlertDialog(mActivity)
    }

    private fun perforAvailabilityClick() {
        startActivity(Intent(mActivity, MyAvailabilityActivity::class.java))
    }

    private fun performProfileClick() {
        startActivity(Intent(mActivity, BabySitterProfileActivity::class.java))
    }



    private fun performSupportClick() {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.addCategory(Intent.CATEGORY_BROWSABLE)
        intent.data = Uri.parse(LINK_SUPPORT)
        startActivity(intent)
    }

    private fun performPrivacyPolicyClick() {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.addCategory(Intent.CATEGORY_BROWSABLE)
        intent.data = Uri.parse(LINK_PP)
        startActivity(intent)
    }


    private fun performSettingsClick() {
        startActivity(Intent(mActivity, SettingBabySitterActivity::class.java))
    }

}