package com.squirrel.app.activities.parent

import android.app.Activity
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.showDatePicker
import com.squirrel.app.utils.showToast
import com.squirrel.app.utils.statusBarColor
import kotlinx.android.synthetic.main.activity_edit_baby_sitters_dobactivity.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class ParentEditDOBActivity : AppCompatActivity() {
    var TAG = this@ParentEditDOBActivity.javaClass.simpleName
    var mActivity: Activity = this@ParentEditDOBActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_edit_dobactivity)

        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
    }
    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.edit_dob_text))

    }

    @OnClick(
        R.id.imgBackIV,
        R.id.btnSaveB,
        R.id.dobLL,

        )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnSaveB -> performSaveDobAction()
            R.id.dobLL, -> showDatePicker(dateTv)

        }
    }

    private fun performSaveDobAction() {
        showToast(mActivity,getString(R.string.dob_updated))
        onBackPressed()
    }

}