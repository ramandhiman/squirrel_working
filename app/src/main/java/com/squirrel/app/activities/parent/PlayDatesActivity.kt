package com.squirrel.app.activities.parent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.adapters.parentsAdapter.PlayDatesAdapter
import com.squirrel.app.interfaces.PlayDatetemClickListner
import com.squirrel.app.models.PlayDatesModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_play_dates.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class PlayDatesActivity : AppCompatActivity() {
    var TAG = this@PlayDatesActivity.javaClass.simpleName
    var mActivity: Activity = this@PlayDatesActivity
    var mAdapter : PlayDatesAdapter? = null
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mMyPlayDatesList : ArrayList<PlayDatesModel>  = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play_dates)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        getMyPlayDatesData()
    }


    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.my_playdates))
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.btnCreatePlaydateB
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnCreatePlaydateB -> performCreatePlaydateClick()

        }
    }

    private fun performCreatePlaydateClick() {
        startActivity(Intent(mActivity,CreatePlaydateActivity::class.java))
    }



    private fun getMyPlayDatesData() {
        if (isNetworkAvailable(mActivity)){
            executeGetPlayDatesData()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetPlayDatesData() {
        showProgressDialog(mActivity)
        mFireStoreDB!!.collection(EVENTS_COLLECTION).whereEqualTo("parentID",getUserID(mActivity))
            .get().addOnSuccessListener {documentSnapshot ->
                dismissProgressDialog()
                mMyPlayDatesList.clear()
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mMyPlayDatesList.add(document.toObject(PlayDatesModel::class.java))
                    }
                    Log.e(TAG,"**PlayDates Size**${mMyPlayDatesList.size}")
                    if (mMyPlayDatesList.size > 0){
                        setAdapter()
                        noDataFountLL.visibility = View.GONE
                    }else{
                        noDataFountLL.visibility = View.VISIBLE
                    }
                } else {
                    noDataFountLL.visibility = View.VISIBLE
                    showToast(mActivity, getString(R.string.no_playdates_found))
                }
            }
            .addOnFailureListener { e ->
                dismissProgressDialog()
                noDataFountLL.visibility = View.VISIBLE
                showToast(mActivity,getString(R.string.some_thing_went_wrong))
            }
    }

    private fun setAdapter() {
        mAdapter = PlayDatesAdapter(mActivity, mMyPlayDatesList, mPlayDatetemClickListner)
        val layoutManager : LinearLayoutManager
        layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mRecyclerViewRV.layoutManager = layoutManager
        mRecyclerViewRV.adapter = mAdapter
    }

    var mPlayDatetemClickListner : PlayDatetemClickListner = object : PlayDatetemClickListner{
        override fun onItemClickListner(mModel: PlayDatesModel) {
            super.onItemClickListner(mModel)
        }
    }


}