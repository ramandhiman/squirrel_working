package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.PREVIOUS_DATA
import com.squirrel.app.utils.getName
import com.squirrel.app.utils.statusBarColor
import kotlinx.android.synthetic.main.activity_edit_baby_sitter_name.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class EditBabySitterNameActivity : AppCompatActivity() {
    var TAG = this@EditBabySitterNameActivity.javaClass.simpleName
    var mActivity: Activity = this@EditBabySitterNameActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_baby_sitter_name)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setDataOnWidget()
    }

    private fun setDataOnWidget() {
        editTitleET.setText(getName(mActivity))
        editTitleET.setSelection(getName(mActivity).length)

    }

    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.edit_name_title))

    }

    @OnClick(
        R.id.imgBackIV,
        R.id.btnSaveB,

    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnSaveB -> performSaveNameAction()

        }
    }

    private fun performSaveNameAction() {
        val intent = Intent()
        intent.putExtra(PREVIOUS_DATA,editTitleET.text.toString().trim())
        setResult(666, intent)
        finish()
    }

}