package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squirrel.app.adapters.babysitterAdapters.BabySitterJoinRequestAdapter
import com.squirrel.app.adapters.babysitterAdapters.BabySittersChatAdapter
import com.squirrel.app.models.ConversationalModel
import com.squirrel.app.models.RequestsModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_baby_sitter_new_home.*

class BabySItterNewHomeActivity : AppCompatActivity() {
    var TAG = this@BabySItterNewHomeActivity.javaClass.simpleName
    var mActivity: Activity = this@BabySItterNewHomeActivity
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mNewRequestsList: ArrayList<RequestsModel> = ArrayList()
    var mChatUsersList: ArrayList<ConversationalModel> = ArrayList()
    var mBabySitterJoinRequestAdapter: BabySitterJoinRequestAdapter? = null
    var mBabySittersChatAdapter: BabySittersChatAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_baby_sitter_new_home)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
    }

    override fun onResume() {
        super.onResume()
        getJoinRequestsData()
        getConversationRequestsData()
    }


    private fun getJoinRequestsData() {
        if (isNetworkAvailable(mActivity)) {
            executeJoinRequests()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun executeJoinRequests() {
        showProgressDialog(mActivity)
        mFireStoreDB!!.collection(REQUESTS_COLLECTION)
            .whereEqualTo("playdate_parent_id", getUserID(mActivity))
            .orderBy("createdAt", Query.Direction.DESCENDING)
            .get().addOnSuccessListener { documentSnapshot ->
                dismissProgressDialog()
                mNewRequestsList.clear()
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        var mModel :RequestsModel = document.toObject(RequestsModel::class.java)
                        if (mModel.status == "0") {
                            mNewRequestsList.add(mModel)
                        }
                    }
                    if (mNewRequestsList.size > 0){
                        joinRequestShow()
                        setupJoinedRequestRV()
                    }else{
                        joinRequestHide()
                        setupJoinedRequestRV()
                    }
                } else {
                    joinRequestHide()
                }
            }.addOnFailureListener { e ->
                dismissProgressDialog()
                joinRequestHide()
            }
    }

    fun joinRequestShow(){
        joinRequestTV.visibility = View.VISIBLE
        joinRequestRV.visibility = View.VISIBLE
    }

    fun joinRequestHide(){
        joinRequestTV.visibility = View.GONE
        joinRequestRV.visibility = View.GONE
    }

    private fun getConversationRequestsData() {
        if (isNetworkAvailable(mActivity)) {
            executeConversationRequests()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun executeConversationRequests() {
        mFireStoreDB!!.collection(CONVERSATIONS_COLLECTION)
            .whereArrayContains("access", getUserID(mActivity))
            .whereEqualTo("isForBabysitter",  "1")
            .orderBy("updatedAt", Query.Direction.DESCENDING)
            .get().addOnSuccessListener { documentSnapshot ->
                mChatUsersList.clear()
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        var mModel : ConversationalModel = document.toObject(ConversationalModel::class.java)
                        mChatUsersList.add(mModel)
                    }
                    if (mChatUsersList.size > 0) {
                        setupParentsChatRV()
                        noDataFountLL.visibility = View.GONE
                    } else {
                        noDataFountLL.visibility = View.VISIBLE
                    }
                } else {
                    noDataFountLL.visibility = View.VISIBLE
                }
            }
            .addOnFailureListener { e ->
                noDataFountLL.visibility = View.VISIBLE
            }
    }


    @OnClick(R.id.imgBabyMenuIV)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBabyMenuIV -> performMenuClick()
        }
    }

    private fun performMenuClick() {
        startActivity(Intent(mActivity, BabySitterLeftMenuActivity::class.java))
        finish()
    }


    private fun setupJoinedRequestRV() {
        mBabySitterJoinRequestAdapter = BabySitterJoinRequestAdapter(mActivity, mNewRequestsList)
        joinRequestRV.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
        joinRequestRV.setHasFixedSize(true)
        joinRequestRV.adapter = mBabySitterJoinRequestAdapter
    }

    private fun setupParentsChatRV() {
        mBabySittersChatAdapter = BabySittersChatAdapter(mActivity,mChatUsersList)
        playdatesChatRV.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        playdatesChatRV.setHasFixedSize(true)
        playdatesChatRV.adapter = mBabySittersChatAdapter
    }
}