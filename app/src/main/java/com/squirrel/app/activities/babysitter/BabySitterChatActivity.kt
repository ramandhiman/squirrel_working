package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.adapters.babysitterAdapters.BabySitterChatListingAdapter
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.models.ConversationalModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_baby_sitter_chat.*
import kotlinx.android.synthetic.main.layout_chat_toolbar.*


class BabySitterChatActivity : AppCompatActivity() {
    var TAG = this@BabySitterChatActivity.javaClass.simpleName
    var mActivity: Activity = this@BabySitterChatActivity
    var chatListingRV: RecyclerView? = null
    var mConversationalModel : ConversationalModel = ConversationalModel()
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_baby_sitter_chat)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        getIntentData()
        setupChatListingRV()
        setupSendMessageIcon()

    }

    private fun getIntentData() {
        mConversationalModel = intent.getParcelableExtra<ConversationalModel>(MODEL)!!
        getOtherUserData()
    }

    private fun getOtherUserData() {
        var mOtherUserId = ""
        for (i in 0 until mConversationalModel.access!!.size){
            if (mConversationalModel.owner != mConversationalModel.access!!.get(i)){
                mOtherUserId = mConversationalModel.access!!.get(i)
                Log.e("TAG","**UserID**"+mOtherUserId)
            }
        }


        //Getting Other  Details
        if (mOtherUserId.isNotEmpty()) {
            mFireStoreDB!!.collection(BABYSITTERS_COLLECTION).document(mOtherUserId)
                .get().addOnSuccessListener { documentSnapshot ->
                    if (documentSnapshot.exists()) {
                        val mBabysitterModel: BabysitterModel = documentSnapshot.toObject(
                            BabysitterModel::class.java)!!
                        Glide.with(mActivity!!).load(mBabysitterModel.imgsource)
                            .placeholder(R.drawable.ic_placeholder)
                            .error(R.drawable.ic_placeholder).into(chatProfileIv)

                        if (mBabysitterModel.name!!.isNotEmpty()) {
                            txtUserChatTV.setText(mBabysitterModel.name)
                        }
                    }
                }
        }




    }

    private fun setUpToolBar() {
        txtUserChatTV.setText("Mary")//chat header name
        chatListingRV = findViewById(R.id.chatListingRV) as RecyclerView
    }


    fun setupSendMessageIcon() {
        sendMessageEt.doAfterTextChanged { count: Editable? ->
            scrollToBottom()
            if (count!!.length > 0) {
                senMessageIV.visibility = View.VISIBLE
            } else {
                senMessageIV.visibility = View.GONE
            }
        }
    }

    @OnClick(
        R.id.imgBackChatIV,
        R.id.profileRL,
        R.id.senMessageIV

    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackChatIV -> onBackPressed()
            R.id.profileRL -> performProfileClick()
            R.id.senMessageIV -> sendMessageRequest()

        }
    }

    private fun performProfileClick() {

        val intent = Intent(mActivity, GroupMembersActivity::class.java)
        startActivity(intent)
    }

    private fun setupChatListingRV() {
        val chatListingRV = findViewById(R.id.chatListingRV) as RecyclerView
//        var mAdapter: BabySitterChatListingAdapter = BabySitterChatListingAdapter(mActivity)
//        chatListingRV.layoutManager = LinearLayoutManager(
//            mActivity,
//            LinearLayoutManager.VERTICAL, false
//        )

//        chatListingRV.setHasFixedSize(true)
//        chatListingRV.adapter = mAdapter
//        scrollToBottom()
    }

    fun sendMessageRequest() {
        hideKeyboard(mActivity)
        showToast(mActivity, "Message sent!!!")
        sendMessageEt.setText(" ")

    }

    private fun scrollToBottom() {

//          chatListingRV!!.scrollToPosition(3)


    }
}