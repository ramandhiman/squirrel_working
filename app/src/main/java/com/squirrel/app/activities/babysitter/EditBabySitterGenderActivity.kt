package com.squirrel.app.activities.babysitter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.aigestudio.wheelpicker.WheelPicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_edit_baby_sitter_gender.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class EditBabySitterGenderActivity : AppCompatActivity() {
    var TAG = this@EditBabySitterGenderActivity.javaClass.simpleName
    var mActivity: Activity = this@EditBabySitterGenderActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_baby_sitter_gender)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setDataOnWidget()
    }

    private fun setDataOnWidget() {
        txtGenderTV.setText(getGenderString(getUserGender(mActivity)))
        if (getUserGender(mActivity).equals("2")){
            editGenderTextET.visibility = View.VISIBLE
            editGenderTextET.setText(getUserGenderSpecify(mActivity))
            editGenderTextET.setSelection(getUserGenderSpecify(mActivity).length)
        }else{
            editGenderTextET.visibility = View.GONE
        }
    }

    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.edit_gender))
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.genderLL,
        R.id.btnSaveB)

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.genderLL -> createGenderSpecifyDialog(mActivity)
            R.id.btnSaveB -> performSaveNameAction()

        }
    }

    private fun performSaveNameAction() {
        val intent = Intent()
        intent.putExtra(PREVIOUS_DATA, txtGenderTV.text.toString().trim())
        intent.putExtra(OTHER_SPECIFY, editGenderTextET.text.toString().trim())
        setResult(777, intent)
        finish()
    }


    fun createGenderSpecifyDialog(mActivity: Activity) {
        var mDataList : ArrayList<String> = ArrayList<String>()
        for (i in 0..GENDER?.size!! - 1){
            mDataList.add(GENDER?.get(i))
        }

        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_edit_gender, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "poppins_regular.ttf")
        val relationshipWP: WheelPicker = sheetView.findViewById(R.id.relationshipWP)
        relationshipWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)

        relationshipWP.setAtmospheric(true)
        relationshipWP.isCyclic = false
        relationshipWP.isCurved = true
        //Set Data
        relationshipWP.data = mDataList

        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }

        txtSaveTV.setOnClickListener {
            var mRelationShip  = mDataList.get(relationshipWP.currentItemPosition)
            txtGenderTV.setText(mRelationShip)
            mBottomSheetDialog.dismiss()
        }
    }

}