package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Source
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.models.BabysitterDays
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_my_availability.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class MyAvailabilityActivity :AppCompatActivity() {
    var TAG = this@MyAvailabilityActivity.javaClass.simpleName
    var mActivity: Activity = this@MyAvailabilityActivity
    var mAlwaysAvailable : String = "0"
    var mAcceptUrgentRequest : String = "0"

    var mondayChecked: String = "0"
    var tuesdayChecked: String = "0"
    var wednesdayChecked: String = "0"
    var thursdayChecked: String = "0"
    var fridayChecked: String = "0"
    var saturdayChecked: String = "0"
    var sundayChecked: String = "0"

    var mDaysList : ArrayList<BabysitterDays> = ArrayList()

    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mBabysitterModel : BabysitterModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_availability)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setUpValidations()
        getBabysitterAvailablityData()
    }

    private fun getBabysitterAvailablityData() {
        if (isNetworkAvailable(mActivity)){
            executeGetBabySitterData()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetBabySitterData() {
        val mDocumentRefenceBabySitter = mFireStoreDB?.collection(BABYSITTERS_COLLECTION)?.document(getUserID(mActivity))!!.get(Source.SERVER)
        //Babysitter Model
        mDocumentRefenceBabySitter?.addOnSuccessListener { babyDocumentSnapshot ->
            if (babyDocumentSnapshot.exists()){
                mBabysitterModel = babyDocumentSnapshot.toObject(BabysitterModel::class.java)
                setDataOnWidgets()
            }else{
                showToast(mActivity,getString(R.string.some_thing_went_wrong))
            }
        }.addOnFailureListener {
            Log.e(TAG,"**ERROR**")
        }

    }

    private fun setDataOnWidgets() {
        editHourlyRateET.setText(mBabysitterModel?.rate)
        acceptUrgentRequestSB.isChecked = mBabysitterModel?.isAcceptUrgentRequest.equals("1")
        alwaysAvailableSB.isChecked = mBabysitterModel?.isAlwaysAvailable.equals("1")
        mAlwaysAvailable = mBabysitterModel?.isAlwaysAvailable!!
        mAcceptUrgentRequest = mBabysitterModel?.isAlwaysAvailable!!

        if (mBabysitterModel?.isAlwaysAvailable.equals("1")){
            mondaySB.visibility = View.GONE
            tusedaySB.visibility = View.GONE
            wednesdaySB.visibility = View.GONE
            thursdaySB.visibility = View.GONE
            fridaySB.visibility = View.GONE
            saturdaySB.visibility = View.GONE
            sundaySB.visibility = View.GONE
            mondayToFromLL.visibility = View.GONE
            tusedayToFromLL.visibility = View.GONE
            wednesdayToFromLL.visibility = View.GONE
            thursdayToFromLL.visibility = View.GONE
            fridayToFromLL.visibility = View.GONE
            saturdayToFromLL.visibility = View.GONE
            sundayToFromLL.visibility = View.GONE
        }else{
            mondaySB.visibility = View.VISIBLE
            tusedaySB.visibility = View.VISIBLE
            wednesdaySB.visibility = View.VISIBLE
            thursdaySB.visibility = View.VISIBLE
            fridaySB.visibility = View.VISIBLE
            saturdaySB.visibility = View.VISIBLE
            sundaySB.visibility = View.VISIBLE
            for (i in 0 until mBabysitterModel?.days!!.size){
                var mDayModel = mBabysitterModel?.days!!.get(i)
                if (i == 0){
                    mondayChecked = mDayModel.isAvailable!!
                    if (mDayModel.isAvailable.equals("1")){
                        mondaySB.isChecked = mDayModel.isAvailable.equals("1")
                        mondayToFromLL.visibility = View.VISIBLE
                        txtMondayFromTV.setText(getOnlyTimeFromFullDate(mDayModel.from!!))
                        txtMondayToTV.setText(getOnlyTimeFromFullDate(mDayModel.to!!))
                    }else{
                        mondayToFromLL.visibility = View.GONE
                    }
                }
                if (i == 1){
                    tuesdayChecked = mDayModel.isAvailable!!
                    if (mDayModel.isAvailable.equals("1")){
                        tusedaySB.isChecked = mDayModel.isAvailable.equals("1")
                        tusedayToFromLL.visibility = View.VISIBLE
                        txtTusedayFromTV.setText(getOnlyTimeFromFullDate(mDayModel.from!!))
                        txtTusedayToTV.setText(getOnlyTimeFromFullDate(mDayModel.to!!))
                    }else{
                        tusedayToFromLL.visibility = View.GONE
                    }
                }
                if (i == 2){
                    wednesdayChecked = mDayModel.isAvailable!!
                    if (mDayModel.isAvailable.equals("1")){
                        wednesdaySB.isChecked = mDayModel.isAvailable.equals("1")
                        wednesdayToFromLL.visibility = View.VISIBLE
                        txtWednesdayFromTV.setText(getOnlyTimeFromFullDate(mDayModel.from!!))
                        txtWednesdayToTV.setText(getOnlyTimeFromFullDate(mDayModel.to!!))
                    }else{
                        wednesdayToFromLL.visibility = View.GONE
                    }
                }
                if (i == 3){
                    thursdayChecked = mDayModel.isAvailable!!
                    if (mDayModel.isAvailable.equals("1")){
                        thursdaySB.isChecked = mDayModel.isAvailable.equals("1")
                        thursdayToFromLL.visibility = View.VISIBLE
                        txtThursdayFromTV.setText(getOnlyTimeFromFullDate(mDayModel.from!!))
                        txtThursdayToTV.setText(getOnlyTimeFromFullDate(mDayModel.to!!))
                    }else{
                        thursdayToFromLL.visibility = View.GONE
                    }
                }
                if (i == 4){
                    fridayChecked = mDayModel.isAvailable!!
                    if (mDayModel.isAvailable.equals("1")){
                        fridaySB.isChecked = mDayModel.isAvailable.equals("1")
                        fridayToFromLL.visibility = View.VISIBLE
                        txtFridayFromTV.setText(getOnlyTimeFromFullDate(mDayModel.from!!))
                        txtFridayToTV.setText(getOnlyTimeFromFullDate(mDayModel.to!!))
                    }else{
                        fridayToFromLL.visibility = View.GONE
                    }
                }
                if (i == 5){
                    saturdayChecked = mDayModel.isAvailable!!
                    if (mDayModel.isAvailable.equals("1")){
                        saturdaySB.isChecked = mDayModel.isAvailable.equals("1")
                        saturdayToFromLL.visibility = View.VISIBLE
                        txtSaturdayFromTV.setText(getOnlyTimeFromFullDate(mDayModel.from!!))
                        txtSaturdayToTV.setText(getOnlyTimeFromFullDate(mDayModel.to!!))
                    }else{
                        saturdayToFromLL.visibility = View.GONE
                    }
                }
                if (i == 6){
                    sundayChecked = mDayModel.isAvailable!!
                    if (mDayModel.isAvailable.equals("1")){
                        sundaySB.isChecked = mDayModel.isAvailable.equals("1")
                        sundayToFromLL.visibility = View.VISIBLE
                        txtSundayFromTV.setText(getOnlyTimeFromFullDate(mDayModel.from!!))
                        txtSundayToTV.setText(getOnlyTimeFromFullDate(mDayModel.to!!))
                    }else{
                        sundayToFromLL.visibility = View.GONE
                    }
                }
            }

        }

    }

    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.setup_profile))
        val mTitleText = "<font color=#0b0f35>Your</font> <font color=#10B502>availability</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.btnNextB,
        R.id.txtMondayFromTV,
        R.id.txtMondayToTV,
        R.id.txtTusedayFromTV,
        R.id.txtTusedayToTV,
        R.id.txtWednesdayFromTV,
        R.id.txtWednesdayToTV,
        R.id.txtThursdayFromTV,
        R.id.txtThursdayToTV,
        R.id.txtFridayFromTV,
        R.id.txtFridayToTV,
        R.id.txtSaturdayFromTV,
        R.id.txtSaturdayToTV,
        R.id.txtSundayFromTV,
        R.id.txtSundayToTV
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnNextB -> performNextClick()
            R.id.txtMondayFromTV -> createFromToTimeDialog(mActivity,txtMondayFromTV)
            R.id.txtMondayToTV -> createFromToTimeDialog(mActivity,txtMondayToTV)
            R.id.txtTusedayFromTV -> createFromToTimeDialog(mActivity,txtTusedayFromTV)
            R.id.txtTusedayToTV -> createFromToTimeDialog(mActivity,txtTusedayToTV)
            R.id.txtWednesdayFromTV -> createFromToTimeDialog(mActivity,txtWednesdayFromTV)
            R.id.txtWednesdayToTV -> createFromToTimeDialog(mActivity,txtWednesdayToTV)
            R.id.txtThursdayFromTV -> createFromToTimeDialog(mActivity,txtThursdayFromTV)
            R.id.txtThursdayToTV -> createFromToTimeDialog(mActivity,txtThursdayToTV)
            R.id.txtFridayFromTV -> createFromToTimeDialog(mActivity,txtFridayFromTV)
            R.id.txtFridayToTV -> createFromToTimeDialog(mActivity,txtFridayToTV)
            R.id.txtSaturdayFromTV -> createFromToTimeDialog(mActivity,txtSaturdayFromTV)
            R.id.txtSaturdayToTV -> createFromToTimeDialog(mActivity,txtSaturdayToTV)
            R.id.txtSundayFromTV -> createFromToTimeDialog(mActivity,txtSundayFromTV)
            R.id.txtSundayToTV -> createFromToTimeDialog(mActivity,txtSundayToTV)
        }
    }

    private fun performNextClick() {
        if (isValidate()){
            if (isNetworkAvailable(mActivity)){
                executeSubmitDaysRequest()
            }else{
                showToast(mActivity,getString(R.string.internet_connection_error))
            }
        }
    }

    private fun executeSubmitDaysRequest() {
        if (mAlwaysAvailable.equals("0")){
            var mDay1 : BabysitterDays = BabysitterDays("1",mondayChecked,getCurrentDateInFormat()+ "T"+txtMondayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtMondayToTV.text.toString().trim())
            var mDay2 : BabysitterDays = BabysitterDays("2",tuesdayChecked,getCurrentDateInFormat()+ "T"+txtTusedayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtTusedayToTV.text.toString().trim())
            var mDay3 : BabysitterDays = BabysitterDays("3",wednesdayChecked,getCurrentDateInFormat()+ "T"+txtWednesdayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtWednesdayToTV.text.toString().trim())
            var mDay4 : BabysitterDays = BabysitterDays("4",thursdayChecked,getCurrentDateInFormat()+ "T"+txtThursdayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtThursdayToTV.text.toString().trim())
            var mDay5 : BabysitterDays = BabysitterDays("5",fridayChecked,getCurrentDateInFormat()+ "T"+txtFridayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtFridayToTV.text.toString().trim())
            var mDay6 : BabysitterDays = BabysitterDays("6",saturdayChecked,getCurrentDateInFormat()+ "T"+txtSaturdayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtSaturdayToTV.text.toString().trim())
            var mDay7 : BabysitterDays = BabysitterDays("7",sundayChecked,getCurrentDateInFormat()+ "T"+txtSundayFromTV.text.toString().trim(),getCurrentDateInFormat()+ "T"+txtSundayToTV.text.toString().trim())

            mDaysList.add(mDay1)
            mDaysList.add(mDay2)
            mDaysList.add(mDay3)
            mDaysList.add(mDay4)
            mDaysList.add(mDay5)
            mDaysList.add(mDay6)
            mDaysList.add(mDay7)
        }

        showProgressDialog(mActivity)
        val mDaysData = hashMapOf(
            "isAlwaysAvailable" to mAlwaysAvailable,
            "isAcceptUrgentRequest" to mAcceptUrgentRequest,
            "updatedAt" to updatedAt(),
            "days" to mDaysList
        )

        mFireStoreDB?.collection(BABYSITTERS_COLLECTION)?.document(getUserID(mActivity))
            ?.update(mDaysData as Map<String, Any>)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                showToast(mActivity,getString(R.string.updated_successfully))
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                Log.e(TAG, "Error: ${e.toString()}")
            }
    }


    // - - Validations...
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editHourlyRateET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_hourly_rate_information))
                flag = false
            }

            mondaySB.isChecked && txtMondayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_monday_availability_from_time))
                flag = false
            }
            mondaySB.isChecked && txtMondayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_monday_availability_to_time))
                flag = false
            }


            tusedaySB.isChecked && txtTusedayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_tuseday_availability_from_time))
                flag = false
            }
            tusedaySB.isChecked && txtTusedayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_tuseday_availability_to_time))
                flag = false
            }


            wednesdaySB.isChecked && txtWednesdayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_wednesday_availability_from_time))
                flag = false
            }
            wednesdaySB.isChecked && txtWednesdayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_wednesday_availability_to_time))
                flag = false
            }

            thursdaySB.isChecked && txtThursdayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_thursday_availability_from_time))
                flag = false
            }
            thursdaySB.isChecked && txtThursdayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_thursday_availability_to_time))
                flag = false
            }


            fridaySB.isChecked && txtFridayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_friday_availability_from_time))
                flag = false
            }
            fridaySB.isChecked && txtFridayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_friday_availability_to_time))
                flag = false
            }


            saturdaySB.isChecked && txtSaturdayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_saturday_availability_from_time))
                flag = false
            }
            saturdaySB.isChecked && txtSaturdayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_saturday_availability_to_time))
                flag = false
            }

            sundaySB.isChecked && txtSundayFromTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_sunday_availability_from_time))
                flag = false
            }
            sundaySB.isChecked && txtSundayToTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_the_sunday_availability_to_time))
                flag = false
            }



        }
        return flag
    }



    private fun setUpValidations() {
        acceptUrgentRequestSB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                txtacceptUrgentRequestTV.visibility = View.GONE
                mAcceptUrgentRequest = "1"
            } else {
                mAcceptUrgentRequest = "0"
                txtacceptUrgentRequestTV.visibility = View.VISIBLE
            }
        }

        alwaysAvailableSB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                mAlwaysAvailable = "1"
                mondaySB.visibility = View.GONE
                tusedaySB.visibility = View.GONE
                wednesdaySB.visibility = View.GONE
                thursdaySB.visibility = View.GONE
                fridaySB.visibility = View.GONE
                saturdaySB.visibility = View.GONE
                sundaySB.visibility = View.GONE
                mondayToFromLL.visibility = View.GONE
                tusedayToFromLL.visibility = View.GONE
                wednesdayToFromLL.visibility = View.GONE
                thursdayToFromLL.visibility = View.GONE
                fridayToFromLL.visibility = View.GONE
                saturdayToFromLL.visibility = View.GONE
                sundayToFromLL.visibility = View.GONE
            } else {
                mAlwaysAvailable = "0"
                mondaySB.visibility = View.VISIBLE
                tusedaySB.visibility = View.VISIBLE
                wednesdaySB.visibility = View.VISIBLE
                thursdaySB.visibility = View.VISIBLE
                fridaySB.visibility = View.VISIBLE
                saturdaySB.visibility = View.VISIBLE
                sundaySB.visibility = View.VISIBLE
                mondayToFromLL.visibility = View.GONE
                tusedayToFromLL.visibility = View.GONE
                wednesdayToFromLL.visibility = View.GONE
                thursdayToFromLL.visibility = View.GONE
                fridayToFromLL.visibility = View.GONE
                saturdayToFromLL.visibility = View.GONE
                sundayToFromLL.visibility = View.GONE
            }
        }

        mondaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                mondayChecked = "0"
                mondayToFromLL.visibility = View.GONE
            }else{
                mondayChecked = "1"
                mondayToFromLL.visibility = View.VISIBLE
            }
        }

        tusedaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                tuesdayChecked = "0"
                tusedayToFromLL.visibility = View.GONE
            }else{
                tuesdayChecked = "1"
                tusedayToFromLL.visibility = View.VISIBLE
            }
        }

        wednesdaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                wednesdayChecked = "0"
                wednesdayToFromLL.visibility = View.GONE
            }else{
                wednesdayChecked = "1"
                wednesdayToFromLL.visibility = View.VISIBLE
            }
        }

        thursdaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                thursdayChecked = "0"
                thursdayToFromLL.visibility = View.GONE
            }else{
                thursdayChecked = "1"
                thursdayToFromLL.visibility = View.VISIBLE
            }
        }

        fridaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                fridayChecked = "0"
                fridayToFromLL.visibility = View.GONE
            }else{
                fridayChecked = "1"
                fridayToFromLL.visibility = View.VISIBLE
            }
        }

        saturdaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                saturdayChecked = "0"
                saturdayToFromLL.visibility = View.GONE
            }else{
                saturdayChecked = "1"
                saturdayToFromLL.visibility = View.VISIBLE
            }
        }

        sundaySB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked == false){
                sundayChecked = "0"
                sundayToFromLL.visibility = View.GONE
            }else{
                sundayChecked = "1"
                sundayToFromLL.visibility = View.VISIBLE
            }
        }
    }
}