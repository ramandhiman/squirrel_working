package com.squirrel.app.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squirrel.app.activities.babysitter.GroupMembersActivity
import com.squirrel.app.adapters.parentsAdapter.ParentBabySitterChatListingAdapter
import com.squirrel.app.models.*
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_parent_baby_sitter_chat.*
import kotlinx.android.synthetic.main.activity_parent_playdate_chat.*
import kotlinx.android.synthetic.main.activity_parent_playdate_chat.chatListingRV
import kotlinx.android.synthetic.main.activity_parent_playdate_chat.mProgressGIV
import kotlinx.android.synthetic.main.activity_parent_playdate_chat.senMessageIV
import kotlinx.android.synthetic.main.activity_parent_playdate_chat.sendMessageEt
import kotlinx.android.synthetic.main.layout_chat_toolbar.*
import kotlin.math.log


class ParentPlayDateChatActivity : AppCompatActivity() {
    var TAG = this@ParentPlayDateChatActivity.javaClass.simpleName
    var mActivity: Activity = this@ParentPlayDateChatActivity
    var mConversationalModel : ConversationalModel = ConversationalModel()
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mKidsList : ArrayList<MyKidsModel>  = ArrayList()
    var mMessagesList : ArrayList<MessageModel>  = ArrayList()
    var mActualMessagesList : ArrayList<MessageModel>  = ArrayList()
    var mOtherUserId = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_playdate_chat)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        getIntentData()
        setupSendMessageIcon()

    }

    private fun getIntentData() {
        mConversationalModel = intent.getParcelableExtra<ConversationalModel>(MODEL)!!
        getOtherUserData()
    }

    private fun getOtherUserData() {
        for (i in 0 until mConversationalModel.access!!.size){
            if (getUserID(mActivity) != mConversationalModel.access!!.get(i)){
                mOtherUserId = mConversationalModel.access!!.get(i)
                Log.e("TAG","**UserID**"+mOtherUserId)
            }
        }


        //Getting Other  Details
            if (mOtherUserId.isNotEmpty()) {
                mFireStoreDB!!.collection(PARENTS_COLLECTION).document(mOtherUserId)
                    .get().addOnSuccessListener { documentSnapshot ->
                        if (documentSnapshot.exists()) {
                            val mParentModel: ParentModel = documentSnapshot.toObject(
                                ParentModel::class.java)!!
                            Glide.with(mActivity!!).load(mParentModel.imgsource)
                                .placeholder(R.drawable.ic_placeholder)
                                .error(R.drawable.ic_placeholder).into(chatProfileIv)

                            if (mParentModel.name!!.isNotEmpty()) {
                                txtChildNamesTV.setText(mParentModel.name)
                            }
                        }
                    }
            }


        //Getting Kids Details
        gettingKidsDetails()


        //Getting All Messages:
        mFireStoreDB!!.collection(CONVERSATIONS_COLLECTION).document(mConversationalModel.conversationId).collection(MESSAGES)
            .orderBy("date",Query.Direction.ASCENDING)
            .limit(2000)
            .addSnapshotListener { documentSnapshot, e ->
                if (e != null) {
                    Log.e(TAG, "**ERROR**"+e.localizedMessage)
                    return@addSnapshotListener
                }
                dismissProgressDialog()
                mMessagesList.clear()
                if (!documentSnapshot!!.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mMessagesList.add(document.toObject(MessageModel::class.java))
                    }
                    for (i in 0..mMessagesList!!.size - 1){
                        if (mMessagesList!![i]!!.messageuser?.id == getUserID(mActivity)){
                            mMessagesList!![i]!!.viewType = RIGHT_VIEW_HOLDER
                        }else{
                            mMessagesList!![i]!!.viewType = LEFT_VIEW_HOLDER
                        }
                    }

                    Log.e(TAG,"**MessageList Size**${mMessagesList.size}")

                    setupChatListingRV()
                }
            }


    }

    private fun gettingKidsDetails() {
        //Getting Kids:
        showProgressDialog(mActivity)
        mFireStoreDB!!.collection(KIDS_COLLECTION)
            .whereIn("firebaseID", mConversationalModel.kidsList!!)
            .get().addOnSuccessListener {documentSnapshot ->
                dismissProgressDialog()
                mKidsList.clear()
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mKidsList.add(document.toObject(MyKidsModel::class.java))
                    }
                    Log.e(TAG,"**KidsList Size**${mKidsList.size}")
                    var mKidsName = ""
                    if (mKidsList.isNotEmpty()) {
                        for (i in 0 until mKidsList.size) {
                            if (i == 0){
                                mKidsName = mKidsList.get(i).name!!
                            }else{
                                mKidsName = mKidsName + ", " + mKidsList.get(i).name!!
                            }
                        }
                    }

                        txtUserChatTV.text = mKidsName
                }
            }
            .addOnFailureListener { e ->
                dismissProgressDialog()
            }
    }


    fun setupSendMessageIcon() {
        sendMessageEt.doAfterTextChanged { count: Editable? ->
            scrollToBottom()
            senMessageIV.isEnabled = count!!.isNotEmpty()
        }
    }

    @OnClick(
        R.id.imgBackChatIV,
        R.id.profileRL,
        R.id.sendRL)

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackChatIV -> onBackPressed()
            R.id.profileRL -> performProfileClick()
            R.id.sendRL -> sendMessageRequest()

        }
    }

    private fun performProfileClick() {
        val intent = Intent(mActivity, GroupMembersActivity::class.java)
        startActivity(intent)
    }

    private fun setupChatListingRV() {
        val chatListingRV = findViewById(R.id.chatListingRV) as RecyclerView
        var mAdapter: ParentBabySitterChatListingAdapter = ParentBabySitterChatListingAdapter(mActivity, mMessagesList)
        chatListingRV.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        chatListingRV.setHasFixedSize(true)
        chatListingRV.adapter = mAdapter
        scrollToBottom()
    }


    private fun scrollToBottom() {
          chatListingRV!!.scrollToPosition(mMessagesList.size - 1)
    }



    private fun sendMessageRequest() {
        //Make Enable send button is there is text in edittext:
        if (sendMessageEt.text.toString().isNotEmpty()) {
            if (isNetworkAvailable(mActivity)) {
                executeSendMessage()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }

    private fun executeSendMessage() {
        //Update Conversation Document:
        val mConversationMap = hashMapOf("updatedAt" to updatedAt(), "last_msg" to sendMessageEt.text.toString().trim())
        mFireStoreDB!!.collection(CONVERSATIONS_COLLECTION)
            .document(mConversationalModel.conversationId)
            ?.update(mConversationMap as Map<String, Any>)
            ?.addOnSuccessListener {
            }?.addOnFailureListener { e ->
                Log.e(TAG, "**ERROR**" + e.localizedMessage)
            }


        mProgressGIV.visibility = View.VISIBLE
        senMessageIV.visibility = View.GONE
        var mMessageId = mFireStoreDB!!.collection(CONVERSATIONS_COLLECTION).document(mConversationalModel.conversationId).collection(MESSAGES).document().id
        var mMessageUser: MessageUser = MessageUser(getProfilePic(mActivity), getUserID(mActivity), getName(mActivity))
        val mMessageDetails = hashMapOf("date" to updatedAt(), "isread" to false, "message" to sendMessageEt.text.toString().trim(), "messageid" to mMessageId, "messageuser" to mMessageUser, "owner" to mConversationalModel.conversationId)
        mFireStoreDB!!.collection(CONVERSATIONS_COLLECTION)
            .document(mConversationalModel.conversationId).collection(MESSAGES).document(mMessageId)
            ?.set(mMessageDetails as Map<String, Any>)
            ?.addOnSuccessListener {
                sendMessageEt.setText("")
                mProgressGIV.visibility = View.GONE
                senMessageIV.visibility = View.VISIBLE
            }?.addOnFailureListener { e ->
                sendMessageEt.setText("")
                mProgressGIV.visibility = View.GONE
                senMessageIV.visibility = View.VISIBLE
                showAlertDialog(mActivity, e.localizedMessage)
            }
    }
}


