package com.squirrel.app.activities.babysitter
import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.adapters.GroupMemberAdapter
import com.squirrel.app.utils.statusBarColor
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class GroupMembersActivity : AppCompatActivity() {
    var TAG = this@GroupMembersActivity.javaClass.simpleName
    var mActivity: Activity = this@GroupMembersActivity
    var mAdapter: GroupMemberAdapter? = null
    var myKidsRV: RecyclerView? = null
    val namesChild= arrayOf("Mary,5","Mary,5  Sam,5")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_members)
        ButterKnife.bind(this)
        myKidsRV = findViewById(R.id.myKidssRV)
        statusBarColor(mActivity)
        setUpToolBar()
        setupChatDetailsRV()

    }
    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.chat_details_title))
    }


    @OnClick(
        R.id.imgBackIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()

        }
    }
    private fun setupChatDetailsRV() {
        mAdapter = GroupMemberAdapter(mActivity, namesChild)
        myKidsRV!!.layoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        myKidsRV!!.adapter = mAdapter
    }
}