package com.squirrel.app.activities.parent

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.R.string.please_select_your_relation
import kotlinx.android.synthetic.main.activity_parent_profile.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import android.graphics.Typeface
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.squirrel.app.utils.*
import java.util.*


class ParentProfileActivity : AppCompatActivity() {
    var TAG = this@ParentProfileActivity.javaClass.simpleName
    var mActivity: Activity = this@ParentProfileActivity

    var mParentType = ""
    var mBitmap : Bitmap? = null

    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = Manifest.permission.CAMERA

    private var mProfilePicURI: Uri? = null

    var mFirebaseStorage: FirebaseStorage = FirebaseStorage.getInstance()
    var mStorageRefrence: StorageReference = mFirebaseStorage!!.reference
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mDeviceToken : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_profile)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        getDeviceToken()
    }


    private fun getDeviceToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            mDeviceToken = task.result.toString()
            Log.e(TAG,"**TOKEN**$mDeviceToken")
        })
    }


    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.setup_profile))
        val mTitleText = "<font color=#0b0f35>Parent</font> <font color=#f16c21>profile</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.imgProfileIV,
        R.id.txtMotherTV,
        R.id.txtFatherTV,
        R.id.txtOthersTV,
        R.id.btnNextB
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.imgProfileIV -> performImageClick()
            R.id.txtMotherTV -> tabSelection(0)
            R.id.txtFatherTV -> tabSelection(1)
            R.id.txtOthersTV -> tabSelection(2)
            R.id.btnNextB -> performNextClick()
        }
    }

    private fun performImageClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    private fun performNextClick() {
        if (isValidate()){
            if (isNetworkAvailable(mActivity)){
                executeUploadProfileRequest()
            }else{
                showToast(mActivity,getString(R.string.internet_connection_error))
            }
        }
    }

    /*
    * Upload Image and Getting url of Image
    * */
    private fun executeUploadProfileRequest() {
        showProgressDialog(mActivity)
        val ref = mStorageRefrence?.child(USER_IMAGES +getUserID(mActivity)+"/"+getUserID(mActivity)+".jpg".toString())
        val uploadTask = ref?.putFile(mProfilePicURI!!)

        val urlTask = uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>>
        { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation ref.downloadUrl
        })?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                //Insert data in DB
                val mParentProfile = hashMapOf(
                    "badge" to "0",
                    "gender" to mParentType,
                    "deviceToken" to mDeviceToken,
                    "genderText" to editSpecificET.text.toString().trim(),
                    "imgsource" to downloadUri.toString(),
                    "isLocationEnable" to "0",
                    "isNotificationEnabled" to "0",
                    "lattitude" to "",
                    "location" to "",
                    "longitude" to "",
                    "name" to editNameET.text.toString().trim(),
                    "role" to "1",
                    "updatedAt" to updatedAt(),
                    "userid" to getUserID(mActivity)
                )

                mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
                    ?.set(mParentProfile)
                    ?.addOnSuccessListener {
                        dismissProgressDialog()
                        showToast(mActivity,getString(R.string.parent_profile_create_sucessfully))
                        val intent=Intent(mActivity,AddKidActivity::class.java)
                        startActivity(intent)
                    }?.addOnFailureListener { e ->
                        dismissProgressDialog()
                        Log.e(TAG, "Error: ${e.toString()}")
                        showAlertDialog(mActivity,e.localizedMessage)
                    }

            } else {
                // Handle failures
            }
        }?.addOnFailureListener{
            dismissProgressDialog()
            showAlertDialog(mActivity,it.message)
        }
    }



    // - - Validations for Sign Up fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            mBitmap == null -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_profile_picture))
                flag = false
            }
            editNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_name))
                flag = false
            }
            mParentType.trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(please_select_your_relation))
                flag = false
            }
            editSpecificET.text.toString().trim { it <= ' ' } == ""  && mParentType.equals("2") -> {
                showAlertDialog(mActivity, getString(R.string.please_specify_relationship))
                flag = false
            }

        }
        return flag
    }

    // - - To Change the color of Icon and Text When Tab is selected
    private fun tabSelection(mPos: Int) {
        val mTypeface = Typeface.createFromAsset(assets, "poppins_regular.ttf")
        val mTypefaceBold = Typeface.createFromAsset(assets, "poppins_bold.ttf")
        txtMotherTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtMotherTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtMotherTV.setTypeface(mTypeface)

        txtFatherTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtFatherTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtFatherTV.setTypeface(mTypeface)

        txtOthersTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtOthersTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtOthersTV.setTypeface(mTypeface)

        when (mPos) {
            0 -> {
                mParentType = "0"
                editSpecificET.visibility = View.GONE
                txtMotherTV.getBackground().setColorFilter(getResources().getColor(R.color.box_color_highlight), PorterDuff.Mode.SRC_ATOP)
                txtMotherTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtMotherTV.setTypeface(mTypefaceBold)
            }
            1 -> {
                mParentType = "1"
                editSpecificET.visibility = View.GONE
                txtFatherTV.getBackground().setColorFilter(getResources().getColor(R.color.box_color_highlight), PorterDuff.Mode.SRC_ATOP)
                txtFatherTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtFatherTV.setTypeface(mTypefaceBold)
            }
            2 -> {
                mParentType = "2"
                editSpecificET.visibility = View.VISIBLE
                txtOthersTV.getBackground().setColorFilter(getResources().getColor(R.color.box_color_highlight), PorterDuff.Mode.SRC_ATOP)
                txtOthersTV.setTextColor(mActivity.resources.getColor(R.color.text_color_highlight))
                txtOthersTV.setTypeface(mTypefaceBold)
            }
        }
    }


    // - - To Check permissions
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    // - - To Request permissions
    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    // - - After Request permissions
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    onSelectImageClick()
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
        }
    }

    // - - Start Image Picker Activity
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(480)     //Final image size will be less than 1 MB(Optional)
            .maxResultSize(512, 512)                          //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
    }






    // - - SetUp Image from Camera/Gallery on View
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!
                    mProfilePicURI = uri
                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_cam)
                        .into(imgProfileIV)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }

    }



}