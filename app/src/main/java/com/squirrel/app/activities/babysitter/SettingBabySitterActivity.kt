package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Source
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_setting_baby_sitter.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class SettingBabySitterActivity : AppCompatActivity() {
    var TAG = this@SettingBabySitterActivity.javaClass.simpleName
    var mActivity: Activity = this@SettingBabySitterActivity
    var isLocationEnable="0"
    var isNotificationEnable="0"

    /*
     * Initalize Location
     * */
    var easyLocationProvider: EasyLocationProvider? = null
    var mAccessFineLocation = android.Manifest.permission.ACCESS_FINE_LOCATION
    var mAccessCourseLocation = android.Manifest.permission.ACCESS_COARSE_LOCATION
    val REQUEST_PERMISSION_CODE = 325
    var latitude = 0.0
    var longitude = 0.0

    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mBabysitterModel : BabysitterModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_baby_sitter)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        getBabysitterAvailablityData()
        setSwitchListner()
    }
    private fun setSwitchListner() {
        locationSwitchSB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                txtSelectYourCityTV.visibility = View.GONE
                isLocationEnable = "1"
                performLocationClick()
            } else {
                txtSelectYourCityTV.visibility = View.VISIBLE
                isLocationEnable = "0"
            }
        }
        notificationSwitchSB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                isNotificationEnable = "1"
            } else {
                isNotificationEnable = "0"
            }
        }

    }


    private fun getBabysitterAvailablityData() {
        if (isNetworkAvailable(mActivity)){
            executeGetBabySitterData()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetBabySitterData() {
        val mDocumentRefenceBabySitter = mFireStoreDB?.collection(BABYSITTERS_COLLECTION)?.document(getUserID(mActivity))!!.get(Source.SERVER)
        //Babysitter Model
        mDocumentRefenceBabySitter?.addOnSuccessListener { babyDocumentSnapshot ->
            if (babyDocumentSnapshot.exists()){
                mBabysitterModel = babyDocumentSnapshot.toObject(BabysitterModel::class.java)
                setDataOnWidgets()
            }else{
                showToast(mActivity,getString(R.string.some_thing_went_wrong))
            }
        }.addOnFailureListener {
            Log.e(TAG,"**ERROR**")
        }

    }

    private fun setDataOnWidgets() {
        isLocationEnable = mBabysitterModel?.isLocationEnable!!
        isNotificationEnable = mBabysitterModel?.isNotificationEnabled!!

        locationSwitchSB.isChecked = mBabysitterModel?.isLocationEnable.equals("1")
        notificationSwitchSB.isChecked = mBabysitterModel?.isNotificationEnabled.equals("1")
        if (mBabysitterModel?.isLocationEnable.equals("1")){
            txtLocationHindTV.visibility = View.VISIBLE
            txtSelectYourCityTV.setText(mBabysitterModel?.location)
        }else{
            txtLocationHindTV.visibility = View.GONE
        }
    }

    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.settings))
        val mTitleText = "<font color=#0b0f35>General</font> <font color=#10B502>settings</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.txtSelectYourCityTV,
        R.id.btnSaveB
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.txtSelectYourCityTV -> showToast(mActivity,"Coming soon...")
            R.id.btnSaveB -> performSaveClick()
        }
    }

    private fun performSaveClick() {
        if (isNetworkAvailable(mActivity)){
            updateSettingRequest()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun updateSettingRequest() {
        showProgressDialog(mActivity)
        val mDaysData = hashMapOf(
            "isLocationEnable" to isLocationEnable,
            "isNotificationEnabled" to isNotificationEnable,
            "lattitude" to ""+latitude,
            "longitude" to ""+longitude,
            "location" to txtSelectYourCityTV.text.toString().trim(),
            "updatedAt" to updatedAt()
        )

        mFireStoreDB?.collection(BABYSITTERS_COLLECTION)?.document(getUserID(mActivity))
            ?.update(mDaysData as Map<String, Any>)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                showToast(mActivity,getString(R.string.setting_updated_successfully))
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                Log.e(TAG, "Error: ${e.toString()}")
            }
    }


    private fun performLocationClick() {
        if (checkLocationPermission()) {
            getLocationLatLong()
        } else {
            requestLocationPermission()
        }
    }


    /*
    * Location Permissions::::
    * */
    private fun checkLocationPermission(): Boolean {
        val mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation)
        val mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation)
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
    }


    private fun requestLocationPermission() {
        ActivityCompat.requestPermissions(mActivity, arrayOf(mAccessFineLocation, mAccessCourseLocation), REQUEST_PERMISSION_CODE)
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    //Perform Location Click Code
                    getLocationLatLong()
                }
            }
        }
    }

    fun getLocationLatLong() {
        easyLocationProvider = EasyLocationProvider.Builder(mActivity)
            .setInterval(5000)
            .setFastestInterval(3000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setListener(object : EasyLocationProvider.EasyLocationCallback {
                override fun onGoogleAPIClient(googleApiClient: GoogleApiClient?, message: String) {
                    Log.e("EasyLocationProvider", "onGoogleAPIClient: $message")
                }

                override fun onLocationUpdated(mlatitude: Double, mlongitude: Double) {
                    Log.e(TAG, "onLocationUpdated:: Latitude: $mlatitude Longitude: $mlongitude")
                    latitude = mlatitude
                    longitude = mlongitude
                }

                override fun onLocationUpdateRemoved() {
                    Log.e("EasyLocationProvider", "onLocationUpdateRemoved")
                }
            }).build()
        lifecycle.addObserver(easyLocationProvider!!)
    }

}