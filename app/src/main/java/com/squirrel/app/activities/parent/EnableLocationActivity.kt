package com.squirrel.app.activities.parent

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.firebase.firestore.FirebaseFirestore
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.activities.SearchLocationActivity
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_enable_location.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*
import java.util.*
import kotlin.collections.HashMap



class EnableLocationActivity : AppCompatActivity() {
    var TAG = this@EnableLocationActivity.javaClass.simpleName
    var mActivity: Activity = this@EnableLocationActivity
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var isLocationEnable="1"
    /*
     * Initalize Location
     * */
    var easyLocationProvider: EasyLocationProvider? = null
    var mAccessFineLocation = android.Manifest.permission.ACCESS_FINE_LOCATION
    var mAccessCourseLocation = android.Manifest.permission.ACCESS_COARSE_LOCATION
    val REQUEST_PERMISSION_CODE = 325
    var latitude = 0.0
    var longitude = 0.0
    var searchLatitude = 0.0
    var searchLongitude = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enable_location)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setSwitchListner()
    }

    private fun setSwitchListner() {
        locationSwitchSB.isChecked = true
        selectYourCityLL.visibility = View.GONE
        performLocationClick()

        locationSwitchSB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                selectYourCityLL.visibility = View.GONE
                isLocationEnable = "1"
                performLocationClick()
            } else {
                selectYourCityLL.visibility = View.VISIBLE
                isLocationEnable = "0"
            }
        }
    }


    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.setup_profile))
        val mTitleText = "<font color=#0b0f35>Enable</font> <font color=#F16C21>location</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.txtSelectYourCityTV,
        R.id.imgCurrentLocationIV,
        R.id.btnNextB
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.txtSelectYourCityTV -> performSelectCityClick()
            R.id.imgCurrentLocationIV -> performCurrentLocationClick()
            R.id.btnNextB -> performNextClick()
        }
    }
    private fun performCurrentLocationClick() {
        latitude = getUserLatitude(mActivity).toDouble()
        longitude = getUserLongitude(mActivity).toDouble()

        txtSelectYourCityTV.setText(getAddressFromLatLong(getUserLatitude(mActivity).toDouble(),getUserLongitude(mActivity).toDouble()))
    }

    // User Location Launcher
    var selectLocationLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == 666) {
                // There are no request codes
                val mIntentData: Intent? = result.data
                txtSelectYourCityTV.text = mIntentData?.getStringExtra(FULL_ADDRESS)
                searchLatitude = mIntentData?.getDoubleExtra(LATITUDE,0.0)!!
                searchLongitude = mIntentData?.getDoubleExtra(LONGITUDE,0.0)!!
            }
        }
    private fun performSelectCityClick() {
        val intent = Intent(this, SearchLocationActivity::class.java)
        intent.putExtra(LOCATION_ROLE, FROM_PARENT)
        selectLocationLauncher.launch(intent)
    }

    private fun performNextClick() {
        if (isLocationEnable == "0" && txtSelectYourCityTV.text.toString().trim() == "") {
            showAlertDialog(mActivity,getString(R.string.please_enable_your_current_location_or_seardh))
        }else{
            if(isNetworkAvailable(mActivity)) {
               exeucteUdpateLocation()
            } else {
                showToast(mActivity,getString(R.string.internet_connection_error))
            }
        }
    }


    private fun exeucteUdpateLocation() {
        showProgressDialog(mActivity)

        var mParentUpdate = HashMap<String, Any>()

        if (isLocationEnable == "1"){
            mParentUpdate = hashMapOf(
                "lattitude" to ""+latitude,
                "longitude" to ""+longitude,
                "location" to txtSelectYourCityTV.text.toString().trim(),
                "updatedAt" to updatedAt())

        }else{
            mParentUpdate = hashMapOf(
                "lattitude" to ""+searchLatitude,
                "longitude" to ""+searchLongitude,
                "location" to txtSelectYourCityTV.text.toString().trim(),
                "updatedAt" to updatedAt())
        }

        mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
            ?.update(mParentUpdate as Map<String, Any>)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                showToast(mActivity,getString(R.string.profile_completed_successfully))
                val intent=Intent(mActivity,ParentHomeActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                Log.e(TAG, "Error: ${e.toString()}")
            }

    }


    private fun performLocationClick() {
        if (checkLocationPermission()) {
            getLocationLatLong()
        } else {
            requestLocationPermission()
        }
    }


    /*
    * Location Permissions::::
    * */
    private fun checkLocationPermission(): Boolean {
        val mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation)
        val mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation)
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
    }


    private fun requestLocationPermission() {
        ActivityCompat.requestPermissions(mActivity, arrayOf(mAccessFineLocation, mAccessCourseLocation), REQUEST_PERMISSION_CODE)
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    //Perform Location Click Code
                    getLocationLatLong()
                }
            }
        }
    }

    fun getLocationLatLong() {
        easyLocationProvider = EasyLocationProvider.Builder(mActivity)
            .setInterval(5000)
            .setFastestInterval(3000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setListener(object : EasyLocationProvider.EasyLocationCallback {
                override fun onGoogleAPIClient(googleApiClient: GoogleApiClient?, message: String) {
                    Log.e("EasyLocationProvider", "onGoogleAPIClient: $message")
                }

                override fun onLocationUpdated(mlatitude: Double, mlongitude: Double) {
                        Log.e(TAG, "onLocationUpdated:: Latitude: $mlatitude Longitude: $mlongitude")
                        latitude = mlatitude
                        longitude = mlongitude
                    AppPrefrences().writeString(mActivity, USER_LATITUDE, ""+latitude)
                    AppPrefrences().writeString(mActivity, USER_LONGITUDE, ""+longitude)
                }

                override fun onLocationUpdateRemoved() {
                    Log.e("EasyLocationProvider", "onLocationUpdateRemoved")
                }
            }).build()
        lifecycle.addObserver(easyLocationProvider!!)
    }

}