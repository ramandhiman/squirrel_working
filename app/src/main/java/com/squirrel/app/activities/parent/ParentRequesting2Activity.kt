package com.squirrel.app.activities.parent

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import com.squirrel.app.utils.showToast
import com.squirrel.app.utils.statusRequestColor
import kotlinx.android.synthetic.main.activity_baby_sitter_chat.*
import kotlinx.android.synthetic.main.layout_request_toolbar.*

class ParentRequesting2Activity : AppCompatActivity() {
    var TAG = this@ParentRequesting2Activity.javaClass.simpleName
    var mActivity: Activity = this@ParentRequesting2Activity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_requesting2)
        ButterKnife.bind(this)
        statusRequestColor(mActivity,getString(R.string.parentTag))
        setUpToolBar()
        setupSendMessageIcon()
    }

    private fun setUpToolBar() {
        txtRequestTV.setText(getString(R.string.sendRequestTitle))
        txtDismissTv.visibility = View.VISIBLE

    }


    @OnClick(
        R.id.imgCrossIV,
        R.id.txtDismissTv,
        R.id.senMessageIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {

            R.id.imgCrossIV -> onBackPressed()
            R.id.txtDismissTv -> onBackPressed()
            R.id.senMessageIV -> performSendMessagetClick()

        }
    }

    private fun performSendMessagetClick() {
        showToast(mActivity,"Message sent!!!")
        sendMessageEt.setText("")

    }

    fun setupSendMessageIcon() {
        sendMessageEt.doAfterTextChanged { count: Editable? ->

            if (count!!.isNotEmpty()) {
                senMessageIV.visibility = View.VISIBLE
            } else {
                senMessageIV.visibility = View.GONE

            }


        }
    }
}
