package com.squirrel.app.activities.parent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import com.squirrel.app.utils.PREVIOUS_DATA
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.getName
import com.squirrel.app.utils.statusBarColor
import kotlinx.android.synthetic.main.activity_parent_edit_name.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class ParentEditNameActivity : AppCompatActivity() {
    var TAG = this@ParentEditNameActivity.javaClass.simpleName
    var mActivity: Activity = this@ParentEditNameActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_edit_name)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setDataOnWidget()
    }

    private fun setDataOnWidget() {
        editNameET.setText(getName(mActivity))
        editNameET.setSelection(getName(mActivity).length)
    }

    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.edit_name_title))
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.btnSaveB
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnSaveB -> performSaveNameAction()

        }
    }

    private fun performSaveNameAction() {
        val intent = Intent()
        intent.putExtra(PREVIOUS_DATA,editNameET.text.toString().trim())
        setResult(666, intent)
        finish()
    }
}
