package com.squirrel.app.activities.parent

import android.app.Activity
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.hideKeyboard
import com.squirrel.app.utils.showToast
import com.squirrel.app.utils.statusRequestColor
import kotlinx.android.synthetic.main.activity_match_request.*
import kotlinx.android.synthetic.main.layout_request_toolbar.*

class MatchRequestActivity : AppCompatActivity() {
    var TAG = this@MatchRequestActivity.javaClass.simpleName
    var mActivity: Activity = this@MatchRequestActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_request)
        ButterKnife.bind(this)
        statusRequestColor(mActivity,getString(R.string.babySitterTag))
        setUpToolBar()
    }

    private fun setUpToolBar() {
        txtRequestTV.setText(getString(R.string.new_request))
        txtDismissTv.visibility = View.VISIBLE

    }


    @OnClick(
        R.id.imgCrossIV,
        R.id.txtDismissTv,
        R.id.senMessageIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {

            R.id.imgCrossIV -> onBackPressed()
            R.id.txtDismissTv -> onBackPressed()
            R.id.senMessageIV -> sendMessageRequest()

        }
    }

    fun sendMessageRequest()
    {
        hideKeyboard(mActivity)
        showToast(mActivity,"Message sent!!!")
        sendMessageEt.setText(" ")

    }

}