package com.squirrel.app.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.squirrel.app.R
import com.squirrel.app.models.FlagModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_enter_phone_no.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class EnterPhoneNoActivity : AppCompatActivity() {
    var TAG = this@EnterPhoneNoActivity.javaClass.simpleName
    var mActivity: Activity = this@EnterPhoneNoActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_phone_no)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpHeading()
    }
    fun setUpHeading(){
        val mTitleText = "<font color=#0b0f35>Enter with</font> <font color=#F16C21>phone</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }



    @OnClick(
        R.id.imgBackIV,
        R.id.btnContinueB,
        R.id.countryCodeLL,
        R.id.txtTermsConditionsTV,
        R.id.txtPrivacyPolicyTV,
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnContinueB -> performPhoneClick()
            R.id.countryCodeLL -> performCountryCodeClick()
            R.id.txtTermsConditionsTV -> performTermConditionClick()
            R.id.txtPrivacyPolicyTV -> performPrivacyPolicyClick()
        }
    }

    private fun performPrivacyPolicyClick() {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.addCategory(Intent.CATEGORY_BROWSABLE)
        intent.data = Uri.parse(LINK_PP)
        startActivity(intent)
    }

    private fun performTermConditionClick() {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.addCategory(Intent.CATEGORY_BROWSABLE)
        intent.data = Uri.parse(LINK_TERMS)
        startActivity(intent)
    }


    private fun performCountryCodeClick() {
        val intent = Intent(mActivity, CountriesActivity::class.java)
        startActivityForResult(intent, 555)
    }


    private fun performPhoneClick() {
        if (isValidate()) {
            startActivity(Intent(mActivity, OTPActivity::class.java).putExtra(PHONE_NO, txtCountryCodeTV.text.toString().trim() +""+ editPhoneNoET.text.toString().trim()))
        }
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, mIntent: Intent?) {
        super.onActivityResult(requestCode, resultCode, mIntent)

        if (requestCode == 555) {
            if (mIntent != null) {
                var mModel: FlagModel? = mIntent.getParcelableExtra(MODEL)
                txtCountryCodeTV.setText(mModel!!.countryPhoneCode)
                mActivity?.let {
                    Glide.with(it).load(mModel!!.countryImage)
                        .placeholder(R.drawable.ic_flag_ph)
                        .error(R.drawable.ic_flag_ph)
                        .into(imgFlagImageIV)
                }
            }
        }
    }


    // - - Validations for Sign Up fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editPhoneNoET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_phone_no))
                flag = false
            }
            editPhoneNoET.text.toString().trim().length!! != 10 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_valid_phone_no))
                flag = false
            }
        }
        return flag
    }
}