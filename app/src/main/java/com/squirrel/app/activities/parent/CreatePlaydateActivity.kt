package com.squirrel.app.activities.parent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squirrel.app.R
import com.squirrel.app.activities.SearchLocationActivity
import com.squirrel.app.adapters.parentsAdapter.PlayDateKidsAvtarAdapter
import com.squirrel.app.interfaces.PlayDateKidsListnerInterface
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_create_playdate.*
import kotlinx.android.synthetic.main.activity_create_playdate.txtTitleTV
import kotlinx.android.synthetic.main.activity_setting_baby_sitter.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class CreatePlaydateActivity : AppCompatActivity() {
    var TAG = this@CreatePlaydateActivity.javaClass.simpleName
    var mActivity: Activity = this@CreatePlaydateActivity
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var latitude = 0.0
    var longitude = 0.0
    var isUnlimitedGroupEvent = true
    var mKidsList : ArrayList<MyKidsModel>  = ArrayList()
    var mSelectedKidsList : ArrayList<MyKidsModel>  = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_playdate)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setUpGroup()
        gettingParentChild()
    }

    private fun gettingParentChild() {
        showProgressDialog(mActivity)
        mFireStoreDB!!.collection(KIDS_COLLECTION).orderBy("updatedAt", Query.Direction.ASCENDING)
            .whereEqualTo("parentID",getUserID(mActivity))
            .get().addOnSuccessListener {documentSnapshot ->
                dismissProgressDialog()
                mKidsList.clear()
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mKidsList.add(document.toObject(MyKidsModel::class.java))
                    }
                    Log.e(TAG,"**KidsList Size**${mKidsList.size}")
                    setupKidsAvatarRV()
                } else {
                    showToast(mActivity, getString(R.string.no_kids))
                }
            }
            .addOnFailureListener { e ->
                dismissProgressDialog()
                showAlertDialog(mActivity,getString(R.string.some_thing_went_wrong))
            }

    }

    private fun setUpGroup() {
        unlimitedGroupSizeSB.setOnCheckedChangeListener { view, isChecked ->
            isUnlimitedGroupEvent = isChecked
        }
    }

    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.create_playdate))
        val mTitleText = "<font color=#0b0f35>Create</font> <font color=#f16c21>playdate</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }



    @OnClick(
        R.id.imgBackIV,
        R.id.btnPost,
        R.id.dateLL,
        R.id.timeLL,
        R.id.imgCurrentLocationIV,
        R.id.locationLL)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnPost -> performPostClickAction()
            R.id.dateLL -> showFutureDatePicker(dateTv)
            R.id.timeLL -> createFromToTimeDialog(mActivity,timeTv)
            R.id.imgCurrentLocationIV -> performCurrentLocationClick()
            R.id.locationLL -> performLocationClick()
        }
    }

    private fun performCurrentLocationClick() {
        latitude = getUserLatitude(mActivity).toDouble()
        longitude = getUserLongitude(mActivity).toDouble()
        locationTV.setText(getAddressFromLatLong(getUserLatitude(mActivity).toDouble(),getUserLongitude(mActivity).toDouble()))
    }


    // Playdates Location Launcher
    var selectLocationLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == 888) {
            // There are no request codes
            val mIntentData: Intent? = result.data
            locationTV.text = mIntentData?.getStringExtra(FULL_ADDRESS)
            latitude = mIntentData?.getDoubleExtra(LATITUDE,0.0)!!
            longitude = mIntentData?.getDoubleExtra(LONGITUDE,0.0)!!
        }
    }
    private fun performLocationClick() {
        val intent = Intent(this, SearchLocationActivity::class.java)
        intent.putExtra(LOCATION_ROLE, FROM_PLAYDATES)
        selectLocationLauncher.launch(intent)
    }

    fun performPostClickAction() {
        if (isNetworkAvailable(mActivity)){
            if (isValidate()) {
                executeAddPlayDateRequest()
            }
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeAddPlayDateRequest() {
       val mKidsArrayList: ArrayList<String> = ArrayList()
        for (i in 0 until mSelectedKidsList.size){
            mKidsArrayList.add(mSelectedKidsList.get(i).firebaseID!!)
        }


        //Insert data in DB
        var mFirebaseID = mFireStoreDB?.collection(EVENTS_COLLECTION)?.document()?.id
        val mPlayDateData = hashMapOf(
            "title" to editTitleET.text.toString().trim(),
            "date" to convertPlayDateDobToDbFormat(dateTv.text.toString().trim() + " " + convert12To24HoursFormat(timeTv.text.toString())) ,
            "groupEvent" to isUnlimitedGroupEvent,
            "id" to mFirebaseID,
            "lattitude" to ""+latitude,
            "longitude" to ""+longitude,
            "location" to locationTV.text.toString().trim(),
            "parentID" to getUserID(mActivity),
            "postalCode" to getZipcodeFromLong(latitude,longitude),
            "childList" to mKidsArrayList,
            "createdAt" to updatedAt())

        showProgressDialog(mActivity)
        mFireStoreDB?.collection(EVENTS_COLLECTION)?.document(mFirebaseID!!)
            ?.set(mPlayDateData)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                showToast(mActivity,getString(R.string.playdate_added_successfully))
                finish()
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                Log.e(TAG, "Error: ${e.toString()}")
                showAlertDialog(mActivity,e.localizedMessage)
            }
    }


    fun isValidate(): Boolean {
        var flag = true
        when {
            editTitleET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_playdate_title))
                flag = false
            }
            locationTV.text.toString().trim{ it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_the_playdate_location))
                flag = false
            }
            dateTv.text.toString().trim{ it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_the_playdate_date))
                flag = false
            }
            timeTv.text.toString().trim{ it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_the_playdate_time))
                flag = false
            }
            mSelectedKidsList.isEmpty() -> {
                showAlertDialog(mActivity, getString(R.string.please_select_who_will_join_to_continue_to_post_playdate))
                flag = false
            }

        }
        return flag
    }


    fun setupKidsAvatarRV() {
        var mAdapter : PlayDateKidsAvtarAdapter = PlayDateKidsAvtarAdapter(mActivity, mKidsList,mPlayDateKidsListnerInterface)
        whoWillJoinRV.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL,false)
        whoWillJoinRV.setHasFixedSize(true)
        whoWillJoinRV.adapter = mAdapter
    }

    var mPlayDateKidsListnerInterface : PlayDateKidsListnerInterface = object  : PlayDateKidsListnerInterface{
        override fun onSelectedKidListner(mArrayList: ArrayList<MyKidsModel>) {
            mSelectedKidsList = mArrayList
            Log.e(TAG,"**SIZE**${mSelectedKidsList.size}")
        }
    }


}