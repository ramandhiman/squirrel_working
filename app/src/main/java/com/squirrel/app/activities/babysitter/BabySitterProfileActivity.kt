package com.squirrel.app.activities.babysitter

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_baby_sitter_profile.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*
import java.io.ByteArrayOutputStream
import java.io.IOException

class BabySitterProfileActivity : AppCompatActivity() {
    var TAG = this@BabySitterProfileActivity.javaClass.simpleName
    var mActivity: Activity = this@BabySitterProfileActivity
    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = Manifest.permission.CAMERA

    private var mProfilePicURI: Uri? = null
    private var mIDPicURI: Uri? = null

    private var mSourceImageUrl: String? = ""
    private var mIDImageUrl: String? = ""


    var mBitmap : Bitmap? = null
    var mBitmapUploadId : Bitmap? = null
    var isProfilePick = false
    var isUploadId = false


    var isBothImages = false
    var isProfileImages = false
    var isIdImages = false

    var mFirebaseStorage: FirebaseStorage = FirebaseStorage.getInstance()
    var mStorageRefrence: StorageReference = mFirebaseStorage!!.reference
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_baby_sitter_profile)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setDataOnWidgets()
    }

    private fun setDataOnWidgets() {
        txtNameTV.setText(getName(mActivity))
        txtDateOfBirthTV.setText(getDateOfBirth(mActivity))
        txtGenderTV.setText(getGenderString(getUserGender(mActivity)))
        txtIdTV.setText(getString(R.string.id_))
        Glide.with(mActivity).load(getProfilePic(mActivity)).placeholder(R.drawable.ic_cam_green).error(R.drawable.ic_cam_green).into(profileIV)
    }

    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.edit_profile))
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.editNameLL,
        R.id.editDobLL,
        R.id.genderLL,
        R.id.uploadDocumentLL,
        R.id.profilePicEditLL,
        R.id.btnSaveB

        )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.editNameLL -> editNameAction()
            R.id.editDobLL -> editDobAction()
            R.id.genderLL -> editGenderAction()
            R.id.profilePicEditLL -> performImageClick()
            R.id.uploadDocumentLL -> performUploadIdClick()
            R.id.btnSaveB -> performSaveClick()
        }
    }



    private fun performSaveClick() {
        if (isNetworkAvailable(mActivity)){
            updateProfileRequest()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }

    }


    // Date Of Birth Launcher
    var dobLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == 333) {
            // There are no request codes
            val mIntentData: Intent? = result.data
            //Do Some Operation Here
            txtDateOfBirthTV.setText(mIntentData?.getStringExtra(PREVIOUS_DATA))
        }
    }

    private fun editDobAction() {
        val intent = Intent(this, EditBabySittersDOBActivity::class.java)
        dobLauncher.launch(intent)
    }


    // User Name Launcher
    var nameLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == 666) {
            // There are no request codes
            val mIntentData: Intent? = result.data
            //Do Some Operation Here
            txtNameTV.setText(mIntentData?.getStringExtra(PREVIOUS_DATA))
        }
    }
    private fun editNameAction() {
        val intent = Intent(this, EditBabySitterNameActivity::class.java)
        nameLauncher.launch(intent)
    }


    // User Gender Launcher
    var genderLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == 777) {
            // There are no request codes
            val mIntentData: Intent? = result.data
            //Do Some Operation Here
            txtGenderTV.setText(mIntentData?.getStringExtra(PREVIOUS_DATA))
            AppPrefrences().writeString(mActivity, USER_GENDER_SPECIFY,mIntentData?.getStringExtra(OTHER_SPECIFY))
        }
    }

    private fun editGenderAction() {
        val intent = Intent(this, EditBabySitterGenderActivity::class.java)
        genderLauncher.launch(intent)
    }

    private fun performUploadIdClick() {
        isProfilePick = false
        isUploadId = true
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    private fun performImageClick() {
        isProfilePick = true
        isUploadId = false
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }


    // - - To Check permissions
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    // - - To Request permissions
    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    // - - After Request permissions
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    onSelectImageClick()
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
        }
    }

    // - - Start Image Picker Activity
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(480)     //Final image size will be less than 1 MB(Optional)
            .maxResultSize(512, 512)                          //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
    }


    // - - SetUp Image from Camera/Gallery on View
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!
                    // Use Uri object instead of File to avoid storage permissions
                    if (isProfilePick){
                        mProfilePicURI = uri
                        Glide
                            .with(this)
                            .load(uri)
                            .centerCrop()
                            .placeholder(R.drawable.ic_cam)
                            .into(profileIV)
                        val imageStream = contentResolver.openInputStream(uri)
                        val selectedImage = BitmapFactory.decodeStream(imageStream)
                        val out = ByteArrayOutputStream()
                        selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                        mBitmap = selectedImage

                    }else if (isUploadId){
                        mIDPicURI = uri
                        val imageStream = contentResolver.openInputStream(uri)
                        val selectedImage = BitmapFactory.decodeStream(imageStream)
                        val out = ByteArrayOutputStream()
                        selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                        mBitmapUploadId = selectedImage
                    }
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }



    private fun updateProfileRequest() {
        AppPrefrences().writeString(mActivity, USER_DOB, txtDateOfBirthTV.text.toString().trim())
        AppPrefrences().writeString(mActivity, NAME, txtNameTV.text.toString().trim())
        AppPrefrences().writeString(mActivity, USER_GENDER, getGenderNumber(txtGenderTV.text.toString().trim()))
        if (mProfilePicURI != null && mProfilePicURI.toString().isNotEmpty() && mIDPicURI != null && mIDPicURI.toString().isNotEmpty()){
             isBothImages = true
             isProfileImages = false
             isIdImages = false
            executeUploadProfileProfileRequest()
        }else if (mProfilePicURI != null && mProfilePicURI.toString().isNotEmpty() && mIDPicURI != null){
            isBothImages = false
            isProfileImages = true
            isIdImages = false
            executeUploadProfileProfileRequest()
        }else if (mProfilePicURI != null && mIDPicURI != null && mIDPicURI.toString().isNotEmpty()){
            isBothImages = false
            isProfileImages = false
            isIdImages = true
            executeUploadIDImageRequest()
        }else if (mProfilePicURI == null && mIDPicURI == null){
            insertDataInDB()
        }
    }




    /*
  * Upload Image and Getting url of Image
  * */
    private fun executeUploadProfileProfileRequest() {
        showProgressDialog(mActivity)
        val ref = mStorageRefrence?.child(USER_IMAGES +getUserID(mActivity)+"/"+getUserID(mActivity)+".jpg".toString())
        val uploadTask = ref?.putFile(mProfilePicURI!!)

        val urlTask = uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>>
        { task ->
            if (!task.isSuccessful) {
                dismissProgressDialog()
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation ref.downloadUrl
        })?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                mSourceImageUrl = downloadUri.toString()
                AppPrefrences().writeString(mActivity, PROFILEPIC, mSourceImageUrl)
                if (isBothImages){
                    executeUploadIDImageRequest()
                }else if (isProfileImages){
                    insertDataInDB()
                }
            } else {
                dismissProgressDialog()
                showAlertDialog(mActivity,getString(R.string.some_thing_went_wrong))
            }
        }?.addOnFailureListener{
            dismissProgressDialog()
            showAlertDialog(mActivity,it.message)
        }
    }


    private fun executeUploadIDImageRequest() {
        if (isIdImages){
            showProgressDialog(mActivity)
        }
        val ref = mStorageRefrence?.child(USER_IMAGES_ID +getUserID(mActivity)+"/"+getUserID(mActivity)+".jpg".toString())
        val uploadTask = ref?.putFile(mIDPicURI!!)

        val urlTask = uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>>
        { task ->
            if (!task.isSuccessful) {
                dismissProgressDialog()
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation ref.downloadUrl
        })?.addOnCompleteListener { task ->
            dismissProgressDialog()
            if (task.isSuccessful) {
                val downloadUri = task.result
                mIDImageUrl = downloadUri.toString()
                if (isBothImages){
                    AppPrefrences().writeString(mActivity, PROFILEPIC, mSourceImageUrl)
                    AppPrefrences().writeString(mActivity, USER_IC_EPIC, mIDImageUrl)
                    insertDataInDB()
                }else if (isIdImages){
                     AppPrefrences().writeString(mActivity, USER_IC_EPIC, mIDImageUrl)
                     insertDataInDB()
                }
            } else {
                dismissProgressDialog()
                showAlertDialog(mActivity,getString(R.string.some_thing_went_wrong))
            }
        }?.addOnFailureListener{
            dismissProgressDialog()
            showAlertDialog(mActivity,it.message)
        }
    }


    private fun insertDataInDB(){
        if (!isBothImages && !isProfileImages && !isIdImages){
            showProgressDialog(mActivity)
        }
        //Insert data in DB
        val mBabysitterDetails = hashMapOf(
            "gender" to getUserGender(mActivity),
            "genderText" to getUserGenderSpecify(mActivity),
            "imageId" to getIdImage(mActivity),
            "imgsource" to getProfilePic(mActivity),
            "name" to getName(mActivity),
            "dob" to convertDobToDbFormat(getDateOfBirth(mActivity)),
            "updatedAt" to updatedAt(),
            "userid" to getUserID(mActivity))

        mFireStoreDB?.collection(BABYSITTERS_COLLECTION)?.document(getUserID(mActivity))
            ?.update(mBabysitterDetails)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                 isBothImages = false
                 isProfileImages = false
                 isIdImages = false
                 mProfilePicURI = null
                 mIDPicURI = null
                 mSourceImageUrl = ""
                 mIDImageUrl = ""
                showToast(mActivity,getString(R.string.profile_updated_sucessfully))
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                Log.e(TAG, "Error: ${e.toString()}")
                showAlertDialog(mActivity,e.localizedMessage)
            }
    }


}