package com.squirrel.app.activities.parent

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.LINK_PP
import com.squirrel.app.utils.LINK_SUPPORT
import com.squirrel.app.utils.showLogoutAlertDialog
import com.squirrel.app.utils.statusBarColor

class LeftMenuActivity : AppCompatActivity() {
    var TAG = this@LeftMenuActivity.javaClass.simpleName
    var mActivity: Activity = this@LeftMenuActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_left_menu)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
    }



    @OnClick(
        R.id.imgCloseIV,
        R.id.homeLL,
        R.id.settingsLL,
        R.id.myKidsLL,
        R.id.parentProfileLL,
        R.id.myPlaydatesLL,
        R.id.supportLL,
        R.id.privacyPolicytLL,
        R.id.txtLogoutTV
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgCloseIV -> onBackPressed()
            R.id.homeLL ->  performHomeClick()
            R.id.settingsLL -> performSettingsClick()
            R.id.myKidsLL -> performMyKidsClick()
            R.id.parentProfileLL -> performParentProfileClick()
            R.id.myPlaydatesLL -> performMyPlayDatesClick()
            R.id.supportLL -> performSupportClick()
            R.id.privacyPolicytLL -> performPrivacyPolicyClick()
            R.id.txtLogoutTV -> performLogoutClick()
        }
    }

    private fun performHomeClick() {
        finish()
    }

    private fun performLogoutClick() {
        showLogoutAlertDialog(mActivity)
    }

    private fun performParentProfileClick() {
        startActivity(Intent(mActivity, KidParentProfileActivity::class.java))
        finish()
    }

    private fun performMyPlayDatesClick() {
        startActivity(Intent(mActivity, PlayDatesActivity::class.java))
        finish()
    }

    private fun performSupportClick() {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.addCategory(Intent.CATEGORY_BROWSABLE)
        intent.data = Uri.parse(LINK_SUPPORT)
        startActivity(intent)
    }

    private fun performPrivacyPolicyClick() {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.addCategory(Intent.CATEGORY_BROWSABLE)
        intent.data = Uri.parse(LINK_PP)
        startActivity(intent)
    }

    private fun performMyKidsClick() {
        startActivity(Intent(mActivity,MyKidsActivity::class.java))
        finish()
    }

    private fun performSettingsClick() {
        startActivity(Intent(mActivity,SettingsActivity::class.java))
        finish()
    }
}