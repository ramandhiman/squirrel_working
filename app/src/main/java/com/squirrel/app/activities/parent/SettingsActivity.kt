package com.squirrel.app.activities.parent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Source
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.activities.SearchLocationActivity
import com.squirrel.app.models.ParentModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.activity_settings.locationLL
import kotlinx.android.synthetic.main.activity_settings.locationTV
import kotlinx.android.synthetic.main.activity_settings.txtTitleTV
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class SettingsActivity : AppCompatActivity() {
    var TAG = this@SettingsActivity.javaClass.simpleName
    var mActivity: Activity = this@SettingsActivity
    var isLocationEnable="0"
    var isNotificationEnable="0"
    var latitude = 0.0
    var longitude = 0.0
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mParentModel : ParentModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        getParentAvailablityData()
        setSwitchListner()
    }


    private fun setSwitchListner() {
        locationSwitchSB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                locationLL.visibility = View.GONE
                txtLocationHindTV.visibility = View.VISIBLE
                isLocationEnable = "1"
            } else {
                locationLL.visibility = View.VISIBLE
                txtLocationHindTV.visibility = View.GONE
                isLocationEnable = "0"
            }
        }
        notificationSwitchSB.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                isNotificationEnable = "1"
            } else {
                isNotificationEnable = "0"
            }
        }

    }


    private fun getParentAvailablityData() {
        if (isNetworkAvailable(mActivity)){
            executeGetParentData()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetParentData() {
        val mDocumentRefenceParent = mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))!!.get(Source.SERVER)
        //Babysitter Model
        mDocumentRefenceParent?.addOnSuccessListener { parentDocumentSnapshot ->
            if (parentDocumentSnapshot.exists()){
                mParentModel = parentDocumentSnapshot.toObject(ParentModel::class.java)
                setDataOnWidgets()
            }else{
                showToast(mActivity,getString(R.string.some_thing_went_wrong))
            }
        }?.addOnFailureListener {
            Log.e(TAG,"**ERROR**")
        }

    }

    private fun setDataOnWidgets() {
        isLocationEnable = mParentModel?.isLocationEnable!!
        isNotificationEnable = mParentModel?.isNotificationEnabled!!

        locationSwitchSB.isChecked = mParentModel?.isLocationEnable.equals("1")
        notificationSwitchSB.isChecked = mParentModel?.isNotificationEnabled.equals("1")
        locationTV.setText(mParentModel?.location)

        if (mParentModel?.isLocationEnable.equals("1")){
            txtLocationHindTV.visibility = View.VISIBLE
            locationLL.visibility = View.GONE
            isLocationEnable = "1"
        }else{
            txtLocationHindTV.visibility = View.GONE
            locationLL.visibility = View.VISIBLE
        }
    }


    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.settings))
        val mTitleText = "<font color=#0b0f35>General</font> <font color=#F16C21>settings</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }



    @OnClick(
        R.id.imgBackIV,
        R.id.imgCurrentLocationIV,
        R.id.locationLL,
        R.id.btnSaveB
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.imgCurrentLocationIV -> performCurrentLocationClick()
            R.id.locationLL -> performLocationClick()
            R.id.btnSaveB -> performSaveClick()
        }
    }

    private fun performCurrentLocationClick() {
        latitude = getUserLatitude(mActivity).toDouble()
        longitude = getUserLongitude(mActivity).toDouble()
        locationTV.setText(getAddressFromLatLong(getUserLatitude(mActivity).toDouble(),getUserLongitude(mActivity).toDouble()))
    }

    private fun performSaveClick() {
        if (isNetworkAvailable(mActivity)){
            updateSettingRequest()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun updateSettingRequest() {
        showProgressDialog(mActivity)
        val mDaysData = hashMapOf(
            "isLocationEnable" to isLocationEnable,
            "isNotificationEnabled" to isNotificationEnable,
            "lattitude" to ""+latitude,
            "longitude" to ""+longitude,
            "location" to locationTV.text.toString().trim(),
            "updatedAt" to updatedAt()
        )

        mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
            ?.update(mDaysData as Map<String, Any>)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                showToast(mActivity,getString(R.string.setting_updated_successfully))
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                Log.e(TAG, "Error: ${e.toString()}")
            }
    }


    // Playdates Location Launcher
    var selectLocationLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == 888) {
            // There are no request codes
            val mIntentData: Intent? = result.data
            locationTV.text = mIntentData?.getStringExtra(FULL_ADDRESS)
            latitude = mIntentData?.getDoubleExtra(LATITUDE,0.0)!!
            longitude = mIntentData?.getDoubleExtra(LONGITUDE,0.0)!!
        }
    }
    private fun performLocationClick() {
        val intent = Intent(this, SearchLocationActivity::class.java)
        intent.putExtra(LOCATION_ROLE, FROM_PLAYDATES)
        selectLocationLauncher.launch(intent)
    }

}