package com.squirrel.app.activities

import android.app.Activity
import android.app.TaskStackBuilder
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.squirrel.app.R
import com.squirrel.app.activities.babysitter.*
import com.squirrel.app.activities.parent.AddKidActivity
import com.squirrel.app.activities.parent.EnableLocationActivity
import com.squirrel.app.activities.parent.ParentHomeActivity
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.models.ParentModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_otpactivity.*
import java.util.concurrent.TimeUnit

class OTPActivity : AppCompatActivity() {
    var TAG = this@OTPActivity.javaClass.simpleName
    var mActivity: Activity = this@OTPActivity
    var mPhoneNo: String = ""
    private var mVerificationId: String? = null
    private var mAuth: FirebaseAuth? = null
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    var mResendingToken: PhoneAuthProvider.ForceResendingToken? = null
    var isQuotaExceeded = false
    var currentUserID: FirebaseUser? = null
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mDeviceToken : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otpactivity)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        getIntentData()
        initFirebaseOTP()
        getDeviceToken()
    }




    private fun getDeviceToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            mDeviceToken = task.result.toString()
            Log.e(TAG,"**TOKEN**$mDeviceToken")
        })
    }
    private fun getIntentData() {
        if (intent != null) {
            mPhoneNo = intent.getStringExtra(PHONE_NO)!!
            Log.e(TAG, "getIntentData: " + mPhoneNo)
            setUpTextDynamically()
        }
    }

    private fun setUpTextDynamically() {
        val desc = "We sent a code via SMS on your number <b>$mPhoneNo</b> Please enter it below"
        val labelTxt = "<font color=#0b0f35>Verify</font> <font color=#f16c21>phone number</font>"
        txtTitleTV.text = Html.fromHtml(labelTxt)
        val resendTxt =
            "<font color=#0b0f35>You didn't receive it? </font> <font color=#10b502>  Resend code</font>"
        txtDeciriptionTV.text = Html.fromHtml(desc)
        txtResendTV.text = Html.fromHtml(resendTxt)
    }

    fun initFirebaseOTP() {
        showProgressDialog(mActivity)
        mAuth = FirebaseAuth.getInstance()
        mOTPCallbacks()

        val options = PhoneAuthOptions.newBuilder(mAuth!!)
            .setPhoneNumber(mPhoneNo)       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    fun mOTPCallbacks() {
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                //showProgressDialog(mActivity)
                Log.d(TAG, "onVerificationCompleted:$credential")
               // editPinPV.setText(credential.smsCode.toString())
            }

            override fun onVerificationFailed(e: FirebaseException) {
                dismissProgressDialog()
                isQuotaExceeded = true
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e)
                if (e is FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    showFinishAlertDialog(mActivity, getString(R.string.invalid_phone_no))
                } else if (e is FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    showFinishAlertDialog(mActivity, getString(R.string.we_have_blocked_all))
                }
            }

            override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
                dismissProgressDialog()
                isQuotaExceeded = false
                Log.d(TAG, "onCodeSent:$verificationId")
                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId
                mResendingToken = token
            }
        }
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        showProgressDialog(mActivity)
        mAuth!!.signInWithCredential(credential).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                //verification successful we will start the profile activity
                currentUserID = FirebaseAuth.getInstance().currentUser
                AppPrefrences().writeString(mActivity, USERID, currentUserID!!.uid.toString())
                AppPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                //Redirect User According to Status
                getUserDetailsFromDB()
            } else {
                showAlertDialog(mActivity,task.exception?.localizedMessage)
                dismissProgressDialog()
            }
        }
    }

    private fun verifyVerificationCode(code: String) {
        //creating the credential
        val credential = PhoneAuthProvider.getCredential(mVerificationId!!, code)
        //signing the user
        signInWithPhoneAuthCredential(credential)
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.btnConfirmB,
        R.id.txtResendTV
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnConfirmB -> performConfirmClick()
            R.id.txtResendTV -> performResendOTPClick(mPhoneNo, mResendingToken!!)
        }
    }

    private fun performResendOTPClick(phoneNumber: String, token: PhoneAuthProvider.ForceResendingToken) {
        editPinPV.setText("")

        showProgressDialog(mActivity)
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phoneNumber,  // Phone number to verify
            60,  // Timeout duration
            TimeUnit.SECONDS,  // Unit of timeout
            this,  // Activity (for callback binding)
            callbacks,  // OnVerificationStateChangedCallbacks
            token
        ) // ForceResendingToken from callbacks
//        showToast(mActivity,getString(R.string.enter_number))
//        finish()
    }

    private fun performConfirmClick() {
        if (isValidate()) {
            Log.e(TAG, "performConfirmClickCODE: " + editPinPV.text.toString().trim())
            if (isQuotaExceeded) {
                showAlertDialog(mActivity, "Please add another number!!")
            } else {
                verifyVerificationCode(editPinPV.text.toString().trim())
            }
        }

    }

    // - - Validations for Sign Up fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editPinPV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.enter_otp))
                flag = false
            }
            editPinPV.text.toString().trim().length <= 5 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_valid_otp))
                flag = false
            }
        }
        return flag
    }


    private fun getUserDetailsFromDB() {
        val mDocumentRefenceParent = mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
        val mDocumentRefenceBabySitter = mFireStoreDB?.collection(BABYSITTERS_COLLECTION)?.document(getUserID(mActivity))
        mDocumentRefenceParent?.get()?.addOnSuccessListener { documentSnapshot ->
            Log.e(TAG,"***UserID***"+getUserID(mActivity))
            dismissProgressDialog()
            //Parent Model Role
            if (documentSnapshot.exists()){
                val mParentModel=documentSnapshot.toObject(ParentModel::class.java)

                if (mParentModel?.kidsList!!.size > 0 && mParentModel?.lattitude?.length!! > 0){
                    showToast(mActivity,getString(R.string.login_successfully))
                    AppPrefrences().writeString(mActivity, USER_ROLE, mParentModel.role)
                    AppPrefrences().writeString(mActivity, NAME, mParentModel.name)
                    AppPrefrences().writeString(mActivity, PROFILEPIC, mParentModel.imgsource)
                    AppPrefrences().writeString(mActivity, USER_GENDER, mParentModel.gender)
                    AppPrefrences().writeString(mActivity, USER_LATITUDE, mParentModel.lattitude)
                    AppPrefrences().writeString(mActivity, USER_LONGITUDE, mParentModel.longitude)

                    if (getUserRole(mActivity) == "1"){
                        val mParentDoc = hashMapOf("deviceToken" to mDeviceToken)
                        mFireStoreDB?.collection(PARENTS_COLLECTION)?.document(getUserID(mActivity))
                            ?.update(mParentDoc as Map<String, Any>)
                            ?.addOnSuccessListener {
                                Log.e(TAG,"**Token Updated Successfully**")
                            }?.addOnFailureListener { e ->
                            }
                    }else if (getUserRole(mActivity) == "2"){
                        val mBabySitterDoc = hashMapOf("deviceToken" to mDeviceToken)
                        mFireStoreDB?.collection(BABYSITTERS_COLLECTION)?.document(getUserID(mActivity))
                            ?.update(mBabySitterDoc as Map<String, Any>)
                            ?.addOnSuccessListener {
                                Log.e(TAG,"**Token Updated Successfully**")
                            }?.addOnFailureListener { e ->
                            }
                    }

                    val i = Intent(mActivity, ParentHomeActivity::class.java)
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(i)
                    finish()
                }else if (mParentModel?.kidsList!!.size > 0  && mParentModel?.lattitude?.length!! == 0){
                    val i = Intent(mActivity, EnableLocationActivity::class.java)
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(i)
                    finish()
                }else if (mParentModel?.kidsList!!.size == 0){
                    val i = Intent(mActivity, AddKidActivity::class.java)
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(i)
                    finish()
                }else{
                    val i = Intent(mActivity, SelectRoleActivity::class.java)
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(i)
                    finish()
                }
            }else{
                //Babysitter Model Role
                mDocumentRefenceBabySitter?.get()?.addOnSuccessListener { babyDocumentSnapshot ->
                    if (babyDocumentSnapshot.exists()){

                        val mBabysitterModel = babyDocumentSnapshot.toObject(BabysitterModel::class.java)

                        if (mBabysitterModel?.gender != null && mBabysitterModel?.gender!!.isEmpty()){
                            val intent=Intent(mActivity, WelcomeTeamActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else if (mBabysitterModel?.days != null && mBabysitterModel?.days!!.size == 0){
                            val intent=Intent(mActivity, YourAvalablityActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else if (mBabysitterModel?.lattitude != null && mBabysitterModel?.lattitude!!.isEmpty()){
                            val intent=Intent(mActivity, EnableLocationBabySitterActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else if (mBabysitterModel?.isAdminApproved != null && mBabysitterModel?.isAdminApproved.equals("0")) {
                            val intent = Intent(mActivity, ProfilePendingActivity::class.java)
                            startActivity(intent)
                            finish()

                        }else if (mBabysitterModel?.isAdminApproved != null && mBabysitterModel?.isAdminApproved.equals("2")) {
                            val preferences: SharedPreferences = mActivity?.let { AppPrefrences().getPreferences(it) }!!
                            val editor = preferences.edit()
                            editor.clear()
                            editor.apply()
                            Firebase.auth.signOut()
                            val mIntent = Intent(mActivity, SelectionActivity::class.java)
                            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent).startActivities()
                            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
                            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            mActivity?.startActivity(mIntent)
                            mActivity?.finish()
                            val intent = Intent(mActivity, SelectionActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else if(mBabysitterModel?.isAdminApproved != null && mBabysitterModel?.isAdminApproved.equals("1")){
                            AppPrefrences().writeString(mActivity, USER_ROLE, mBabysitterModel.role)
                            AppPrefrences().writeString(mActivity, USER_DOB, convertDBToDobFormat(mBabysitterModel.dob))
                            AppPrefrences().writeString(mActivity, NAME, mBabysitterModel.name)
                            AppPrefrences().writeString(mActivity, PROFILEPIC, mBabysitterModel.imgsource)
                            AppPrefrences().writeString(mActivity, USER_IC_EPIC, mBabysitterModel.imageId)
                            AppPrefrences().writeString(mActivity, USER_GENDER, mBabysitterModel.gender)
                            AppPrefrences().writeString(mActivity, USER_LATITUDE, mBabysitterModel.lattitude)
                            AppPrefrences().writeString(mActivity, USER_LONGITUDE, mBabysitterModel.longitude)
                            val i = Intent(mActivity, BabySItterNewHomeActivity::class.java)
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(i)
                            finish()
                        }else {
                            val i = Intent(mActivity, ProfilePendingActivity::class.java)
                            startActivity(i)
                            finish()
                        }
                    }else{
                        val i = Intent(mActivity, SelectRoleActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(i)
                        finish()
                    }
                }
            }
        }
    }




}