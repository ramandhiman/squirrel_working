package com.squirrel.app.activities.parent

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import com.squirrel.app.activities.babysitter.YourAvalablityActivity
import com.squirrel.app.fragments.babysetter.BabySitterHomeFragment
import com.squirrel.app.fragments.parent.ParentChatFragment
import com.squirrel.app.fragments.parent.ParentHomeFragment
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_welcome_team.*
import kotlinx.android.synthetic.main.layout_bottom_main.*
import kotlinx.android.synthetic.main.layout_home_toolbar.*

class ParentHomeActivity : AppCompatActivity() {
    var TAG = this@ParentHomeActivity.javaClass.simpleName
    var mActivity: Activity = this@ParentHomeActivity
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

     /*
     * Initalize Location
     * */
    var easyLocationProvider: EasyLocationProvider? = null
    var mAccessFineLocation = android.Manifest.permission.ACCESS_FINE_LOCATION
    var mAccessCourseLocation = android.Manifest.permission.ACCESS_COARSE_LOCATION
    val REQUEST_PERMISSION_CODE = 325
    var latitude = 0.0
    var longitude = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_home)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        performHomeClick()
        performLocationClick()
    }



    @OnClick(
        R.id.imgMenuIV,
        R.id.switchToBabySitterLL,
        R.id.homeLL,
        R.id.addLL,
        R.id.chatLL
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgMenuIV -> performMenuClick()
            R.id.switchToBabySitterLL -> performSwitchToMenuClick()
            R.id.homeLL -> performHomeClick()
            R.id.addLL -> performAddClick()
            R.id.chatLL -> performChatClick()
        }
    }

    private fun performHomeClick() {
        txtHeadingTV.visibility = View.GONE
        switchToBabySitterLL.visibility = View.VISIBLE
        tabSelection(0)
        if(moduleTypeTv.text==getString(R.string.playdate_title)) {
            switchFragment(BabySitterHomeFragment(), PARENT_CHAT_TAG, false, null)
        } else {
            switchFragment(ParentHomeFragment(), PARENT_HOME_TAG, false, null)
        }
    }



    /*
    * Perform Add Button Click...
    * */
    private fun performAddClick() {
        startActivity(Intent(mActivity, CreatePlaydateActivity::class.java))
    }

    private fun performChatClick() {
        txtHeadingTV.visibility = View.VISIBLE
        switchToBabySitterLL.visibility = View.GONE
        tabSelection(2)
        switchFragment(ParentChatFragment(), PARENT_CHAT_TAG, false, null)
    }

    private fun performSwitchToMenuClick() {
        if (moduleTypeTv.text == "Babysitters") {
            moduleTypeTv.text = getString(R.string.playdate_title)
            moduleTypeTv.setTextColor(resources.getColor(R.color.colorOrange))
            moduleTypeTv.background = getDrawable(R.drawable.ic_button_orange)
            switchFragment(BabySitterHomeFragment(), PARENT_CHAT_TAG, false, null)
        } else if (moduleTypeTv.text == "Playdates") {
            moduleTypeTv.text = getString(R.string.babysitters)
            moduleTypeTv.setTextColor(resources.getColor(R.color.colorGreen))
            moduleTypeTv.background = getDrawable(R.drawable.ic_button)
            switchFragment(ParentHomeFragment(), PARENT_CHAT_TAG, false, null)
        }
    }

    private fun performMenuClick() {
        startActivity(Intent(mActivity, LeftMenuActivity::class.java))
    }


    // - - To Change the color of Icon and Text When Tab is selected
    private fun tabSelection(mPos: Int) {
        imgHomeIV.setImageResource(R.drawable.ic_category_empty)
        imgAddIV.setImageResource(R.drawable.ic_add_menu)
        imgChatIV.setImageResource(R.drawable.ic_chat)

        when (mPos) {
            0 -> {
                imgHomeIV.setImageResource(R.drawable.ic_category)
            }
            1 -> {
                imgAddIV.setImageResource(R.drawable.ic_add_menu_active)
            }
            2 -> {
                imgChatIV.setImageResource(R.drawable.ic_tab3_select)
            }
        }
    }


    /*Switch between fragments*/
    fun switchFragment(fragment: Fragment?, Tag: String?, addToStack: Boolean, bundle: Bundle?) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.container, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }


    private fun performLocationClick() {
            if (checkLocationPermission()) {
                getLocationLatLong()
            } else {
                requestLocationPermission()
            }
    }


    /*
    * Location Permissions::::
    * */
    private fun checkLocationPermission(): Boolean {
        val mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation)
        val mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation)
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
    }


    private fun requestLocationPermission() {
        ActivityCompat.requestPermissions(mActivity, arrayOf(mAccessFineLocation, mAccessCourseLocation), REQUEST_PERMISSION_CODE)
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    //Perform Location Click Code
                    getLocationLatLong()
                }
            }
        }
    }

    fun getLocationLatLong() {
        easyLocationProvider = EasyLocationProvider.Builder(mActivity)
            .setInterval(5000)
            .setFastestInterval(3000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setListener(object : EasyLocationProvider.EasyLocationCallback {
                override fun onGoogleAPIClient(googleApiClient: GoogleApiClient?, message: String) {
                    Log.e("EasyLocationProvider", "onGoogleAPIClient: $message")
                }

                override fun onLocationUpdated(mlatitude: Double, mlongitude: Double) {
                    Log.e(TAG, "onLocationUpdated:: Latitude: $mlatitude Longitude: $mlongitude")
                    latitude = mlatitude
                    longitude = mlongitude
                    AppPrefrences().writeString(mActivity, USER_LATITUDE, ""+latitude)!!
                    AppPrefrences().writeString(mActivity, USER_LONGITUDE, ""+longitude)!!
                }

                override fun onLocationUpdateRemoved() {
                    Log.e("EasyLocationProvider", "onLocationUpdateRemoved")
                }
            }).build()
        lifecycle.addObserver(easyLocationProvider!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(easyLocationProvider!!)
    }

}