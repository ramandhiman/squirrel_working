package com.squirrel.app.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import com.squirrel.app.adapters.CountriesAdapter
import com.squirrel.app.interfaces.ItemClickListner
import com.squirrel.app.models.FlagModel
import com.squirrel.app.utils.MODEL
import com.squirrel.app.utils.getAllCountriesData
import com.squirrel.app.utils.statusBarColor
import kotlinx.android.synthetic.main.activity_countries.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*


class CountriesActivity : AppCompatActivity() {
    var TAG = this@CountriesActivity.javaClass.simpleName
    var mActivity: Activity = this@CountriesActivity
    lateinit var mAdapter : CountriesAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_countries)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setUpSearch()
        setCountriesAdapter()
    }
    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.select_country))
    }

    private fun setUpSearch() {
        editSearchET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().length > 0){
                    imgCancelIV.visibility = View.VISIBLE
                }else{
                    imgCancelIV.visibility = View.GONE
                }
                //after the change calling the method and passing the search input
                filter(s.toString());
            }
        })
    }

    private fun filter(text: String) {
        //new array list that will hold the filtered data
        val filterdList = ArrayList<FlagModel>()
        //looping through existing elements
        for (s in getAllCountriesData(mActivity)!!) {
            //if the existing elements contains the search input
            if (s!!.countryName.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdList.add(s)
            }
        }
        //calling a method of the adapter class and passing the filtered list
        mAdapter.filterList(filterdList)
    }

    @OnClick(
        R.id.imgBackIV)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun setCountriesAdapter() {
        mAdapter = CountriesAdapter(mActivity, getAllCountriesData(mActivity),mItemClickListner)
        countriesRV.layoutManager = LinearLayoutManager(this)
        countriesRV.setHasFixedSize(true)
        countriesRV.adapter = mAdapter
    }


    var mItemClickListner : ItemClickListner = object : ItemClickListner{
        override fun onItemClickListner(mModel: FlagModel) {
            super.onItemClickListner(mModel)
            val intent = Intent()
            intent.putExtra(MODEL, mModel) //value should be your string from the edittext
            setResult(555, intent) //The data you want to send back
            finish()
        }
    }

}