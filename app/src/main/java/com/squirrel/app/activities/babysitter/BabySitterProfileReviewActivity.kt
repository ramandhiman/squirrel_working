package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.showToast
import com.squirrel.app.utils.statusBarColor
import kotlinx.android.synthetic.main.activity_baby_sitter_profile_review.*
import kotlinx.android.synthetic.main.activity_create_playdate.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class BabySitterProfileReviewActivity : AppCompatActivity() {
    var TAG = this@BabySitterProfileReviewActivity.javaClass.simpleName
    var mActivity: Activity = this@BabySitterProfileReviewActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_baby_sitter_profile_review)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
    }
    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.review_title))
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.btnPostReview

    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.btnPostReview -> performPostReviewClick()
        }
    }

    private fun performPostReviewClick() {
        showToast(mActivity,"Review sent successfully!!")
        editReviewET.setText(" ")
    }

}