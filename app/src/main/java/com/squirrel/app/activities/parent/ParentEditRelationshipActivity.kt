package com.squirrel.app.activities.parent

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.aigestudio.wheelpicker.WheelPicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_parent_edit_relationship.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class ParentEditRelationshipActivity : AppCompatActivity() {
    var TAG = this@ParentEditRelationshipActivity.javaClass.simpleName
    var mActivity: Activity = this@ParentEditRelationshipActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_edit_relationship)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        setDataOnWidget()
    }

    private fun setDataOnWidget() {
        txtRelationET.setText(getRelationShipString(getUserGender(mActivity)))
        if (getUserGender(mActivity).equals("2")){
            editRelationTextET.visibility = View.VISIBLE
            editRelationTextET.setText(getUserGenderSpecify(mActivity))
            editRelationTextET.setSelection(getUserGenderSpecify(mActivity).length)
        }else{
            editRelationTextET.visibility = View.GONE
        }
    }


    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.edit_relation_ship))
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.txtRelationET,
        R.id.btnSaveB)

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.txtRelationET -> performRelationshipClick()
            R.id.btnSaveB -> performSaveNameAction()

        }
    }

    private fun performRelationshipClick() {
        createRelationshipDialog(mActivity)
    }

    private fun performSaveNameAction() {
        val intent = Intent()
        intent.putExtra(PREVIOUS_DATA, txtRelationET.text.toString().trim())
        intent.putExtra(OTHER_SPECIFY, editRelationTextET.text.toString().trim())
        setResult(777, intent)
        finish()
    }


    fun createRelationshipDialog(mActivity: Activity) {
        var mDataList : ArrayList<String> = ArrayList<String>()
        for (i in 0..RELATIONSHIP?.size!! - 1){
            mDataList.add(RELATIONSHIP?.get(i))
        }

        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_edit_relationship, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "poppins_regular.ttf")
        val relationshipWP: WheelPicker = sheetView.findViewById(R.id.relationshipWP)
        relationshipWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)

        relationshipWP.setAtmospheric(true)
        relationshipWP.isCyclic = false
        relationshipWP.isCurved = true
        //Set Data
        relationshipWP.data = mDataList

        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }

        txtSaveTV.setOnClickListener {
            var mRelationShip  = mDataList.get(relationshipWP.currentItemPosition)
            txtRelationET.setText(mRelationShip)
            if (mRelationShip.equals(getString(R.string.other))){
                editRelationTextET.visibility = View.VISIBLE
            }else{
                editRelationTextET.visibility = View.GONE
            }

            mBottomSheetDialog.dismiss()
        }
    }

}