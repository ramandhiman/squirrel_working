package com.squirrel.app.activities.babysitter

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squirrel.app.models.BabysitterModel
import com.squirrel.app.models.MyKidsModel
import com.squirrel.app.models.ParentModel
import com.squirrel.app.models.RequestsModel
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_admit_child2.*
import kotlinx.android.synthetic.main.fragment_playdates_chat.*
import kotlinx.android.synthetic.main.layout_request_toolbar.*

class AdmitChild2Activity : AppCompatActivity() {
    var TAG = this@AdmitChild2Activity.javaClass.simpleName
    var mActivity: Activity = this@AdmitChild2Activity
    var mRequestsModel : RequestsModel? = null
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()
    var mBabySitterList: ArrayList<BabysitterModel> = ArrayList<BabysitterModel>()
    var mParentList: ArrayList<ParentModel> = ArrayList<ParentModel>()
    var mKidsArrayList: ArrayList<String> = ArrayList<String>()
    var mBabySitterToken = ""
    var mParentToken = ""

    // 0. Initiated, 1. Approved 2. Rejected 3. Own cancel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admit_child2)
        ButterKnife.bind(this)
        statusRequestColor(mActivity,getString(R.string.babySitterTag))
        setUpToolBar()
        getIntentData()
        getBabySitterToken()
    }

    private fun getBabySitterToken() {
        showProgressDialog(mActivity)
        //Getting Token Array
        mFireStoreDB!!.collection(BABYSITTERS_COLLECTION)
            .whereEqualTo("userid",  getUserID(mActivity)).limit(1)
            .get().addOnSuccessListener { documentSnapshot ->
                if (!documentSnapshot.isEmpty) {
                    for (document in documentSnapshot) {
                        Log.e(TAG, "${document.id} => ${document.data}")
                        mBabySitterList.add(document.toObject(BabysitterModel::class.java))
                    }
                    val mBabysitterModel: BabysitterModel = mBabySitterList[0]
                    mBabySitterToken = mBabysitterModel.deviceToken
                    Log.e(TAG, "**BabySitterToken**$mBabySitterToken")
                }

                //Getting Parent Device Token
                mFireStoreDB!!.collection(PARENTS_COLLECTION)
                    .whereEqualTo("userid",  mRequestsModel?.request_parent_id)
                    .get().addOnSuccessListener { documentSnapshot ->
                        dismissProgressDialog()
                        if (!documentSnapshot.isEmpty) {
                            for (document in documentSnapshot) {
                                Log.e(TAG, "${document.id} => ${document.data}")
                                mParentList.add(document.toObject(ParentModel::class.java))
                            }
                            val mParentModel: ParentModel = mParentList[0]
                            mParentToken = mParentModel.deviceToken!!
                            Log.e(TAG, "**ParentToken**$mParentToken")
                            mKidsArrayList = mParentModel.kidsList
                        }
                    }.addOnFailureListener { e ->
                        dismissProgressDialog()
                    }
            }.addOnFailureListener { e ->
                dismissProgressDialog()
            }
    }

    private fun setUpToolBar() {
        txtRequestTV.setText(getString(R.string.sendRequestTitle))
        txtDismissTv.visibility = View.VISIBLE
    }


    private fun getIntentData() {
        mRequestsModel = intent.getParcelableExtra(MODEL)
        //Getting  Details
        mFireStoreDB!!.collection(KIDS_COLLECTION).document(mRequestsModel!!.kids!![0])
            .get().addOnSuccessListener { documentSnapshot ->
                if (documentSnapshot.exists()) {
                    val mMyKidsModel: MyKidsModel = documentSnapshot.toObject(MyKidsModel::class.java)!!
                    txtNameAgeTV.text=mMyKidsModel.name + ", " + calculateDateOfBirthWithCurrentTime(mMyKidsModel.dob!!) + "y"

                    Glide.with(mActivity!!).load(AVTAR_ARRAY[Integer.parseInt(mMyKidsModel.avatar)])
                        .placeholder(R.drawable.ic_placeholder)
                        .error(R.drawable.ic_placeholder)
                        .into(imgProfileIV)
                }
            }
    }



    @OnClick(
        R.id.imgCrossIV,
        R.id.txtDismissTv,
        R.id.btnAdmit
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> onBackPressed()
            R.id.txtDismissTv -> performDismissClick()
            R.id.btnAdmit -> performAdmitClick()

        }
    }


    private fun performDismissClick() {
        showProgressDialog(mActivity)
        var   mRequestsDocument = hashMapOf(
            "status" to "2",
            "createdAt" to updatedAt())
        mFireStoreDB?.collection(REQUESTS_COLLECTION)?.document(mRequestsModel?.id!!)
            ?.update(mRequestsDocument as Map<String, Any>)
            ?.addOnSuccessListener {
                dismissProgressDialog()
                showFinishAlertDialog(mActivity,getString(R.string.request_rejected_successfully))
            }?.addOnFailureListener { e ->
                dismissProgressDialog()
            }
    }

    private fun performAdmitClick() {
        //Update Conversation Model
        showProgressDialog(mActivity)

        //Conversational ID
        var mConversationID = mFireStoreDB?.collection(CONVERSATIONS_COLLECTION)?.document()?.id

        //Access Array
        var mAccessList = ArrayList<String>()
        mAccessList.add(mRequestsModel?.request_parent_id!!)
        mAccessList.add(mRequestsModel?.playdate_parent_id!!)

        //DeviceTokens Array
        var deviceTokensList = ArrayList<String>()
        deviceTokensList.add(mBabySitterToken)
        deviceTokensList.add(mParentToken)


        var mConversationDocument = hashMapOf(
            "access" to mAccessList,
            "conversationId" to mConversationID,
            "createdAt" to updatedAt(),
            "deviceTokens" to deviceTokensList,
            "isForBabysitter" to "1",
            "kidsList" to mKidsArrayList,
            "last_msg" to "Welcome!!",
            "owner" to mRequestsModel?.request_parent_id!!,
            "updatedAt" to updatedAt())

        Log.e(TAG,"**AdmitRequestHashMap**"+mConversationDocument)

        mFireStoreDB?.collection(CONVERSATIONS_COLLECTION)?.document(mConversationID!!)
            ?.set(mConversationDocument as Map<String, Any>)
            ?.addOnSuccessListener {
                //Update Request Model:
                var   mRequestsDocument = hashMapOf(
                    "status" to "1",
                    "createdAt" to updatedAt())

                mFireStoreDB?.collection(REQUESTS_COLLECTION)?.document(mRequestsModel?.id!!)
                    ?.update(mRequestsDocument as Map<String, Any>)
                    ?.addOnSuccessListener {
                        dismissProgressDialog()
                        showFinishAlertDialog(mActivity,getString(R.string.admited_successfully))
                    }?.addOnFailureListener { e ->
                        dismissProgressDialog()
                    }

            }?.addOnFailureListener { e ->
                dismissProgressDialog()
                showToast(mActivity,e.message)
            }
    }

}
