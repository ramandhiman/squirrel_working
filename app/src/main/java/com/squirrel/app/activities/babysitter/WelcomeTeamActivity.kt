package com.squirrel.app.activities.babysitter

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.squirrel.app.R
import androidx.appcompat.app.AppCompatActivity
import com.squirrel.app.utils.*
import kotlinx.android.synthetic.main.activity_welcome_team.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*
import java.io.ByteArrayOutputStream
import java.io.IOException

class WelcomeTeamActivity : AppCompatActivity() {
    var TAG = this@WelcomeTeamActivity.javaClass.simpleName
    var mActivity: Activity = this@WelcomeTeamActivity
    var mGenderType = ""
    var mDeviceToken = ""

    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = Manifest.permission.CAMERA

    private var mProfilePicURI: Uri? = null
    private var mIDPicURI: Uri? = null

    var mBitmap : Bitmap? = null
    var mBitmapUploadId : Bitmap? = null
    var isProfilePick = false
    var isUploadId = false

    var mFirebaseStorage: FirebaseStorage = FirebaseStorage.getInstance()
    var mStorageRefrence: StorageReference = mFirebaseStorage!!.reference
    var mFireStoreDB: FirebaseFirestore? = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_team)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
        getDeviceToken()
    }


    private fun getDeviceToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            mDeviceToken = task.result.toString()
            Log.e(TAG,"**TOKEN**$mDeviceToken")
        })
    }


    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.setup_profile))
        val mTitleText = "<font color=#0b0f35>Welcome</font> <font color=#10B502>to the team</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }



    @OnClick(
        R.id.imgBackIV,
        R.id.imgProfileIV,
        R.id.txtMaleTV,
        R.id.txtFemaleTV,
        R.id.txtOthersTV,
        R.id.txtDateOfBithET,
        R.id.txtUploadIdTV,
        R.id.imgCloseIV,
        R.id.btnNextB
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.imgProfileIV -> performImageClick()
            R.id.txtMaleTV -> tabSelection(0)
            R.id.txtFemaleTV -> tabSelection(1)
            R.id.txtOthersTV -> tabSelection(2)
            R.id.txtDateOfBithET -> showDatePicker(txtDateOfBithET)
            R.id.txtUploadIdTV -> performUploadIdClick()
            R.id.imgCloseIV -> performCloseClick()
            R.id.btnNextB -> performNextClick()
        }
    }

    private fun performCloseClick() {
        uploadIdLL.visibility = View.GONE
        txtUploadIdTV.visibility = View.VISIBLE
        mBitmapUploadId = null
    }

    private fun performUploadIdClick() {
        isProfilePick = false
        isUploadId = true
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    private fun performImageClick() {
        isProfilePick = true
        isUploadId = false
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    private fun performNextClick() {
        if (isValidate()){
            if (isNetworkAvailable(mActivity)){
                executeUploadProfileProfileRequest()
            }else{
                showToast(mActivity,getString(R.string.internet_connection_error))
            }
        }
    }


     /*
     * Upload Image and Getting url of Image
     * */
    private fun executeUploadProfileProfileRequest() {
        showProgressDialog(mActivity)
        val ref = mStorageRefrence?.child(USER_IMAGES +getUserID(mActivity)+"/"+getUserID(mActivity)+".jpg".toString())
        val uploadTask = ref?.putFile(mProfilePicURI!!)

        val urlTask = uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>>
        { task ->
            if (!task.isSuccessful) {
                dismissProgressDialog()
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation ref.downloadUrl

        })?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                executeUploadIDImageRequest(downloadUri.toString())
            } else {
                dismissProgressDialog()
                showAlertDialog(mActivity,getString(R.string.some_thing_went_wrong))
            }
        }?.addOnFailureListener{
            dismissProgressDialog()
            showAlertDialog(mActivity,it.message)
        }
    }

    private fun executeUploadIDImageRequest(mImageSource: String) {
        val ref = mStorageRefrence?.child(USER_IMAGES_ID +getUserID(mActivity)+"/"+getUserID(mActivity)+".jpg".toString())
        val uploadTask = ref?.putFile(mIDPicURI!!)

        val urlTask = uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>>
        { task ->
            if (!task.isSuccessful) {
                dismissProgressDialog()
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation ref.downloadUrl
        })?.addOnCompleteListener { task ->
            dismissProgressDialog()
            if (task.isSuccessful) {
                val downloadUri = task.result
                //Insert data in DB

                val mBabysitterDetails = hashMapOf(
                    "badge" to "0",
                    "deviceToken" to mDeviceToken,
                    "dob" to convertDobToDbFormat(txtDateOfBithET.text.toString().trim()),
                    "gender" to mGenderType,
                    "genderText" to editSpecificTV.text.toString(),
                    "imageId" to downloadUri.toString(),
                    "imgsource" to mImageSource,
                    "isAdminApproved" to "0",
                    "isLocationEnable" to "0",
                    "isNotificationEnabled" to "0",
                    "name" to editNameET.text.toString().trim(),
                    "role" to "2",
                    "updatedAt" to updatedAt(),
                    "lattitude" to "0.0",
                    "longitude" to "0.0",
                    "userRating" to "0",
                    "userid" to getUserID(mActivity))

                mFireStoreDB?.collection(BABYSITTERS_COLLECTION)?.document(getUserID(mActivity))
                    ?.set(mBabysitterDetails)
                    ?.addOnSuccessListener {
                        dismissProgressDialog()
                        val intent=Intent(mActivity, YourAvalablityActivity::class.java)
                        startActivity(intent)
                    }?.addOnFailureListener { e ->
                        dismissProgressDialog()
                        Log.e(TAG, "Error: ${e.toString()}")
                        showAlertDialog(mActivity,e.localizedMessage)
                    }
            } else {
                dismissProgressDialog()
                showAlertDialog(mActivity,getString(R.string.some_thing_went_wrong))
            }
        }?.addOnFailureListener{
            dismissProgressDialog()
            showAlertDialog(mActivity,it.message)
        }
    }




    // - - Validations for Sign Up fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            mBitmap == null -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_profile_picture))
                flag = false
            }
            editNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_name))
                flag = false
            }
            txtDateOfBithET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_date_of_birth))
                flag = false
            }
            mGenderType.trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_gender))
                flag = false
            }
            editSpecificTV.text.toString().trim { it <= ' ' } == ""  && mGenderType.equals("2") -> {
                showAlertDialog(mActivity, getString(R.string.please_specify_gender))
                flag = false
            }
            mBitmapUploadId == null -> {
                showAlertDialog(mActivity, getString(R.string.please_select_id_image))
                flag = false
            }

        }
        return flag
    }

    // - - To Change the color of Icon and Text When Tab is selected
    private fun tabSelection(mPos: Int) {
        val mTypeface = Typeface.createFromAsset(assets, "poppins_regular.ttf")
        val mTypefaceBold = Typeface.createFromAsset(assets, "poppins_bold.ttf")
        txtMaleTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtMaleTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtMaleTV.setTypeface(mTypeface)

        txtFemaleTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtFemaleTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtFemaleTV.setTypeface(mTypeface)

        txtOthersTV.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        txtOthersTV.setTextColor(mActivity.resources.getColor(R.color.menu_color_default))
        txtOthersTV.setTypeface(mTypeface)

        when (mPos) {
            0 -> {
                mGenderType = "0"
                editSpecificTV.visibility = View.GONE
                txtMaleTV.getBackground().setColorFilter(getResources().getColor(R.color.colorUnselct), PorterDuff.Mode.SRC_ATOP)
                txtMaleTV.setTextColor(mActivity.resources.getColor(R.color.colorGreen))
                txtMaleTV.setTypeface(mTypefaceBold)
            }
            1 -> {
                mGenderType = "1"
                editSpecificTV.visibility = View.GONE
                txtFemaleTV.getBackground().setColorFilter(getResources().getColor(R.color.colorUnselct), PorterDuff.Mode.SRC_ATOP)
                txtFemaleTV.setTextColor(mActivity.resources.getColor(R.color.colorGreen))
                txtFemaleTV.setTypeface(mTypefaceBold)
            }
            2 -> {
                mGenderType = "2"
                editSpecificTV.visibility = View.VISIBLE
                txtOthersTV.getBackground().setColorFilter(getResources().getColor(R.color.colorUnselct), PorterDuff.Mode.SRC_ATOP)
                txtOthersTV.setTextColor(mActivity.resources.getColor(R.color.colorGreen))
                txtOthersTV.setTypeface(mTypefaceBold)

            }
        }
    }


    // - - To Check permissions
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    // - - To Request permissions
    private fun requestPermission() {
        ActivityCompat.requestPermissions(mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369)
    }

    // - - After Request permissions
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    onSelectImageClick()
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
        }
    }

    // - - Start Image Picker Activity
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                              //Crop image(Optional), Check Customization for more option
            .compress(480)                //Final image size will be less than 1 MB(Optional)
            .maxResultSize(512, 512)  //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
    }


    // - - SetUp Image from Camera/Gallery on View
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!
                    // Use Uri object instead of File to avoid storage permissions
                    if (isProfilePick){
                        mProfilePicURI = uri
                        Glide
                            .with(this)
                            .load(uri)
                            .centerCrop()
                            .placeholder(R.drawable.ic_cam)
                            .into(imgProfileIV)
                        val imageStream = contentResolver.openInputStream(uri)
                        val selectedImage = BitmapFactory.decodeStream(imageStream)
                        val out = ByteArrayOutputStream()
                        selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                        mBitmap = selectedImage
                    }else if (isUploadId){
                        mIDPicURI = uri
                        val imageStream = contentResolver.openInputStream(uri)
                        val selectedImage = BitmapFactory.decodeStream(imageStream)
                        val out = ByteArrayOutputStream()
                        selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                        mBitmapUploadId = selectedImage
                        uploadIdLL.visibility = View.VISIBLE
                        txtUploadIdTV.visibility = View.GONE
                    }
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }

    }





}