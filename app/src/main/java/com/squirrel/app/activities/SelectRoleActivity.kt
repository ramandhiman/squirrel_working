package com.squirrel.app.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.OnClick
import com.squirrel.app.R
import com.squirrel.app.activities.babysitter.WelcomeTeamActivity
import com.squirrel.app.activities.parent.ParentProfileActivity
import com.squirrel.app.utils.statusBarColor
import kotlinx.android.synthetic.main.activity_select_role.*
import kotlinx.android.synthetic.main.layout_backbtn_toolbar.*

class SelectRoleActivity : AppCompatActivity() {
    var TAG = this@SelectRoleActivity.javaClass.simpleName
    var mActivity: Activity = this@SelectRoleActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_role)
        ButterKnife.bind(this)
        statusBarColor(mActivity)
        setUpToolBar()
    }

    private fun setUpToolBar() {
        txtHeadingTV.setText(getString(R.string.select_role))
        val mTitleText = "<font color=#0b0f35>Select</font> <font color=#F16C21>profile</font>"
        txtTitleTV.setText(Html.fromHtml(mTitleText))
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.cardParentCV,
        R.id.cardBabysitterCV
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
            R.id.cardParentCV -> performParentClick()
            R.id.cardBabysitterCV -> performBabySitterClick()
        }
    }

    private fun performParentClick() {
        startActivity(Intent(mActivity, ParentProfileActivity::class.java))
    }

    private fun performBabySitterClick() {
        startActivity(Intent(mActivity, WelcomeTeamActivity::class.java))
    }
}