package com.squirrel.app.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddKidModel (
    var kidCount: String,
    var position: Int,
    var name: String,
    var dateOfBirth: String,
    var gender: String,
    var specifyGender: String,
    var avatar: Int,
    var kidDescription: String,
    var kidAvtarList: ArrayList<KidAvtarModel> = ArrayList()
) : Parcelable