package com.squirrel.app.models

import com.google.firebase.Timestamp

data class ParentModel(
    var badge: String?="",
    var gender: String?="",
    var deviceToken: String?="",
    var genderText: String?="",
    var imgsource: String?="",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    var isLocationEnable: String?="",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    var isNotificationEnabled: String?="",
    var lattitude: String?="",
    var location: String?="",
    var longitude: String?="",
    var name: String?="",
    var role: String?="",
    var updatedAt: Timestamp?= null,
    var userid: String?="",
    val kidsList: ArrayList<String> = ArrayList()
)

