package com.squirrel.app.models

import android.os.Parcelable
import com.google.firebase.Timestamp
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlin.reflect.KClass

@Parcelize
class BabysitterModel(
    var badge: String="",
    var days: ArrayList<BabysitterDays> = ArrayList(),
    var deviceToken: String="",
    var dob: String="",
    var gender: String="",
    var genderText: String="",
    var imageId: String="",
    var imgsource: String="",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    val isAdminApproved: String = "",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    val isLocationEnable: String = "",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    val isNotificationEnabled: String = "",
    var name: String="",
    var role: String="",
    var updatedAt: Timestamp?= null,
    var userRating: String="",
    var lattitude: String="",
    var longitude: String="",
    var location: String="",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    var isAcceptUrgentRequest: String="",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    var isAlwaysAvailable: String="",
    var rate: String="",
    var userid: String="",
    var distance: Double? = 0.0,
) : Parcelable

@Parcelize
class BabysitterDays(
    var day: String? = "",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    var isAvailable: String? = "",
    var from: String? = "",
    var to: String? = "",
): Parcelable
