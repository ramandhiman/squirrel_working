package com.squirrel.app.models

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize



@Parcelize
data class ConversationalModel (
    var conversationId: String = "",
    var last_msg: String = "",
    var name: String = "",
    var owner: String = "",
    var kidsList: ArrayList<String>?=null,
    var createdAt: Timestamp?=null,
    var updatedAt: Timestamp?=null,
    var avatar: String = "",
    var access: ArrayList<String>?=null,
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    var isForBabysitter: String = "",
    var deviceTokens: ArrayList<String>?=null,
) : Parcelable



@Parcelize
data class MessageModel(
    var date: Timestamp?=null,
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    var isread: Boolean?=false,
    var message: String?="",
    var messageid: String?="",
    var owner: String?="",
    var messageuser: MessageUser?=null,
    var viewType : Int = -1,
    var dayDate : String = ""
): Parcelable

@Parcelize
data class MessageUser(
    var avatar: String?="",
    var id: String?="",
    var name: String?="",
) : Parcelable