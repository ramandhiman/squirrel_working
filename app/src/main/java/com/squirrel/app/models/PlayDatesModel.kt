package com.squirrel.app.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import com.google.firebase.Timestamp

@Parcelize
data class PlayDatesModel (
    var createdAt: Timestamp? = null,
    var date: String? = "",
    var groupEvent: Boolean? = false,
    var id: String? = "",
    var lattitude: String? = "",
    var location: String? = "",
    var longitude: String? = "",
    var parentID: String? = "",
    var postalCode: String? = "",
    var childList: ArrayList<String> = ArrayList(),
    var title: String? = "",
    var distance: Double? = 0.0,
) : Parcelable