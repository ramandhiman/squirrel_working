package com.squirrel.app.models

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize
import retrofit2.http.Field


@Parcelize
data class RequestsModel (
    var createdAt: Timestamp?=null,
    var id: String = "",
    @field:JvmField // use this annotation if your Boolean field is prefixed with 'is'
    var isForBabysitter: String = "",
    var kids: ArrayList<String>?=null,
    var playdate_id: String = "",
    var playdate_name: String = "",
    var playdate_parent_id: String = "",
    var request_parent_id: String = "",
    var status: String = ""
) : Parcelable

