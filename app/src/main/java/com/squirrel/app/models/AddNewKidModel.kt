package com.squirrel.app.models

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddNewKidModel(
    var name: String,
    var dob: String,
    var gender: String,
    var genderText: String,
    var avatar: String,
    var hobbies: String,
    var firebaseID: String,
    var parentID: String,
    var updatedAt: Timestamp? = null,

    ) : Parcelable