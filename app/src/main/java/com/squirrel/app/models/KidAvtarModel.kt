package com.squirrel.app.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class KidAvtarModel (
    var avtarImage: Int = 0,
    var isSelected: Boolean = false
) : Parcelable